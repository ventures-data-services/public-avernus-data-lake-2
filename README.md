# Avernus Data Lake

**Ventures**

The public repository for the Avernus Data Lake code.

## Description

The Avernus Data Lake project is a combined data lake structure and ETL (extract transform load) process built within the AWS ecosystem. To start building the project now check out the README in the CloudFormation Templates directory. Other directories have additional READMEs for their specific scopes.

## Repository Contents

- [CloudFormation](CloudFormation_Templates/)
	- This contains all the resources and automated processes to create the Data Lake on the AWS and have it running daily, using AWS Cloudformation templates.
- [DynamoTables](DynamoTables/)
	- Contains the DynamoDB tables and their usage and requirements. Also contains some examples for adding more documents.
- [ETL](ETL/)
	- Contains the code used by AWS's EMR to process the files, and the raw code for the data crawler.
- [HelperNotebooks](HelperNotebooks/)
	- Assorted notebooks that might assist in working with the Data Lake.
- [Lambdas](Lambdas/)
	- The raw code for all the lambdas used by the Data lake.

## AWS Services Used

The Avernus Data Lake uses many AWS services to store and process the data, these include:

* Lambda
* Step Functions
* Elastic Map Reduce (EMR)
* Elastic Compute Cloud (EC2)
* Simple Storage Service (S3)
* Dynamo Data Base
* Glue
* Identity and Access Management (IAM)
* Cloudwatch
* Cloudtrail
* Simple Notification Service (SNS)
* Simple Queue Service (SQS)