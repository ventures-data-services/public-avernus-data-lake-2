"""
Ventures Pinnacle Incorporated - Pedro Mroninski - ??/??/2020
Contains Functions that are used by multiple files.
"""
import csv
import datetime
import glob
import json
import os


def FilesByExtention(extensionList, path="tempFiles"):
    """
    Find all the files in the TempFiles directory with the extensions
    - Input:
	-- List of extensions as List of Strings
    - Returns:
	-- List of file names as List of String
    """
    allFiles = []
    for extension in extensionList:
        listOfFiles = glob.glob("{0}/*.{1}".format(path, extension))
        allFiles += [file for file in listOfFiles]
    return allFiles


def RemoveExtension(filename, count=-2):
    """
    Removes the extension from a filename
    filename: Filename with extension as String (string)
    
    :returns:Filename without extension (String)
    """
    if "." in filename:
        filename = filename.split(".")[count]
    return filename


def RemovePath(filename):
    """
    Removes the path from a filename leaving only the file
    Input:
        Filename with full path as String
    Returns:
        Filename without path as String
    """
    if "/" in filename:
        filename = filename.split("/")[-1]
    if "\\" in filename:
        filename = filename.split("\\")[-1]

    return filename


def GetExtension(filename):
    """
    Gets a file's extention
    filename - name of the file to be checked (string)
    """
    return filename.split(".")[-1].lower()

def RemoveFile(filename):
    os.remove(filename)
    return


def CheckDirectory(directory):
    """
    Checks if directory exists, if not, creates it
    directory - path to be checked (string)
    """
    if not os.path.isdir(directory):
        os.makedirs(directory)
    return


def ClearFileName(file):
    """
    Removes the extension and path from file.
    file - path to the file (string)
    """
    return file.split(".")[0].split("/")[-1]

def LowerAndClearFileName(file):
    return ClearFileName(file).lower()