import os
import time
from datetime import datetime, timedelta

import json
import boto3

from Helpers import Dynamo_Helper as d
from Helpers import SimpleStorageService_Helper as sss


def chunks(lst, n):
    """
    Yield successive n-sized chunks from lst.
    """
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def older_documents_lambda_handler(event="", context=""):
    """
    Get all old documents and send them to be processed, ensure it stop before 
    """

    dynamo_helper = d.DynamoDB_Helper(os.environ["log_table"])

    lambda_helper = boto3.client('lambda')

    days_to_subtract = 15
    subtracted_date = datetime.today() - timedelta(days=days_to_subtract)
    unix_timestamp = int(
        (subtracted_date - datetime(1970, 1, 1)).total_seconds())

    full_scan = dynamo_helper.GetAllItems()

    filtered_scan = [d for d in full_scan if 'timestamp' in d]
    filtered_scan = [
        d for d in filtered_scan if d['timestamp'] <= unix_timestamp]

    obj_chunks = chunks(filtered_scan, 3)
    iterable_chunks = list(obj_chunks)

    base = {"dynamo_table": os.environ["log_table"],
            "destination_bucket": os.environ["log_bucket"]}

    for c in iterable_chunks:
        base["documents"] = c
        lambda_helper.invoke(FunctionName=os.environ['lambda_name'],InvocationType='Event', Payload=json.dumps(base))
    return {"number_processed_documents": len(filtered_scan)}