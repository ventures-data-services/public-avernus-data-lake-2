import os
import time
from datetime import datetime, timedelta

import json

from Helpers import Dynamo_Helper as d
from Helpers import SimpleStorageService_Helper as sss

def clean_key(k):
    """
    Whatever cleaning rules needed for the "dynamo key" -> "s3 path" conversion.
    """
    return k.replace('/', '.')


def upload_file(dynamo_class, bucket_helper, document):
    """
    Uploads a dictionary object into S3 + Remove Dynamo
    """
    key = 'DynamoTables/{}/{}'.format(dynamo_class.tableName,
                                      clean_key(document[dynamo_class.indexKey]))
    if dynamo_class.sortKey is not None:
        key = "{}_{}".format(key, clean_key(document[dynamo_class.sortKey]))
    bucket_helper.upload_JSON(document, key)
    dynamo_class.RemoveDocument(document)


def iterate_though_list(event, context=""):
    """
    Iterates through a list of documents and uploads them to s3 and removes them from the Dynamo Dictionary
    """

    dynamo_helper = d.DynamoDB_Helper(event["dynamo_table"])
    bucket_helper = sss.s3Basic(event["destination_bucket"])

    objects_list = event["documents"]

    completed_docs = []

    while objects_list != completed_docs:
        for doc in objects_list:
            try:
                upload_file(dynamo_helper, bucket_helper, doc)
                completed_docs.append(doc)
            except Exception as e:
                print('Error {} with document {}'.format(e, doc))
                continue
        time.sleep(10)