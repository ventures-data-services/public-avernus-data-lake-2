# Ventures
# Dylan Byrne
import boto3
from Helpers import avernus_sqs_helper
from boto3.dynamodb.conditions import Key, Attr
from datetime import datetime


def find_unprocessed_data_sets(in_log_table, in_sqs_helper):
    """
    Find unprocessed data set entries from the log table. These are created by transformed bucket create events
    """
    PRIMARY_KEY = "log_key"
    UNPROCESSED_VALUE = "unprocessed_multi_object"
    SORT_KEY = "log_type"

    try:
        response = in_log_table.query(
            KeyConditionExpression=Key(PRIMARY_KEY).eq(UNPROCESSED_VALUE)
        )
        print("[INFO] Scan for process doc response: {}".format(str(response)))

        # get data sets with unprocessed logs
        data_sets = []
        for doc in response['Items']:
            data_sets.append(doc[SORT_KEY])
            print("deleting data set {}".format(doc[SORT_KEY]))
            del_resp = in_log_table.delete_item(Key={PRIMARY_KEY: doc[PRIMARY_KEY], SORT_KEY: doc[SORT_KEY]})

        data_sets = list(set(data_sets))

        print("[INFO] Final Data Sets: {}".format(str(data_sets)))

        return data_sets
    except Exception as e:
        error = "ERROR in avernus_multi_obj_finder find_unprocessed_data_sets {}".format(str(e))
        print(error)
        in_sqs_helper.sqs_put([error])
        return []


def get_dags(in_settings_table, in_data_sets, in_sqs_helper):
    """
    Find all applicable DAGs based on which data sets need to be processed
    """
    try:
        APPLIED_SET_KEYS = "multi_object_process_applied_data_sets"
        PRIMARY_KEY = "setting_name"

        dag_list = []

        response = in_settings_table.scan(FilterExpression=Attr(APPLIED_SET_KEYS).exists())['Items']

        for entry in response:
            for data_set in entry[APPLIED_SET_KEYS]:
                if data_set in in_data_sets:
                    dag_list.append(entry[PRIMARY_KEY])

        print("[INFO] Duplicated DAGs: {}".format(dag_list))

        dag_list = list(set(dag_list))

        print("[INFO] Final DAGs: {}".format(str(dag_list)))

        return dag_list
    except Exception as e:
        error = "ERROR in avernus_multi_obj_finder get_dags {}".format(str(e))
        print(error)
        in_sqs_helper.sqs_put([error])
        return []


def get_current_time() -> int:
    """
    Get current UNIX timestamp
    :return:  unix timestamp
    """
    # Calculate UTC Now Timestamp
    d = datetime.utcnow()
    epoch = datetime(1970, 1, 1)
    time_epoch = (d - epoch).total_seconds()
    return round(time_epoch)


def create_prefix_document(in_prefixes, in_log_table, in_exec_id, in_sqs_helper):
    prefix_document = {
        "log_key": "prefixes_{}".format(in_exec_id),
        "log_type": "crawl_prefixes",
        "prefix_list": in_prefixes,
        "timestamp": get_current_time()
    }

    try:
        response = in_log_table.put_item(Item=prefix_document)
        print(response)
    except Exception as e:
        print(e)
        error = "ERROR in avernus_multi_obj_finder create_prefix_document {}".format(str(e))
        print(error)
        in_sqs_helper.sqs_put([error])


def create_process_document(in_function_input, in_exec_id, in_log_table, in_sqs_helper):
    """
    Create a multi object process document for EMR and put it in the log table
    """
    unique_function_input = list(set(in_function_input))

    etl_jobtype = "function_specific_process"
    meta_type = "transformed"

    new_process_log = {
        "log_key": "{0}_{1}".format(meta_type, in_exec_id),
        "log_type": meta_type,
        "etl_jobtype": etl_jobtype,
        "job_input": unique_function_input,
        "timestamp": get_current_time()
    }

    print(new_process_log)

    try:
        response = in_log_table.put_item(Item=new_process_log)
        print(response)
    except Exception as e:
        print(e)
        print(new_process_log)
        error = "ERROR in avernus_multi_obj_finder create_process_document {}".format(str(e))
        print(error)
        in_sqs_helper.sqs_put([error])


def lambda_handler(event, context):
    # init
    region = event['config']['region']
    ddb_res = boto3.resource('dynamodb', region_name=region)
    log_table = ddb_res.Table(event['config']['ddb_name_log'])
    setting_table = ddb_res.Table(event['config']['ddb_name_setting'])
    sqs_url = event['config']['sqs_url']
    sqs_helper = avernus_sqs_helper.sqs_helper(sqs_url)

    try:
        # find data sets
        data_sets = find_unprocessed_data_sets(log_table, sqs_helper)
        if len(data_sets) == 0:
            return {"start_job": False}

        # get dags
        dags = get_dags(setting_table, data_sets, sqs_helper)
        if len(dags) == 0:
            return {"start_job": False}

        # create process document
        create_process_document(dags, event['exec_arn']['exec_arn'], log_table, sqs_helper)

        # create prefix document
        create_prefix_document(data_sets, log_table, event['exec_arn']['exec_arn'], sqs_helper)

        # format args to start job
        log_index_key = "{0}_{1}".format("transformed", event['exec_arn']['exec_arn'])

        args = [
            "spark-submit",
            "--deploy-mode",
            "client",
            "--py-files",
            event['config']['EmrSparkZipPath'],
            event['config']['EmrSparkMainPath'],
            "EMR",  # environment
            event['config']['ddb_name_setting'],  # settings table
            log_index_key,  # index key for the main log
            # "function_specific_process",  # process type
            "DOCUMENT",  # input type for the step (eg document, SQS)
            "{0}_{1}".format("transformed", event['exec_arn']['exec_arn']),  # more input info for the step (eg SQS URL)
            "transformed"  # secondary input, eg sort key
        ]

        # return
        return {"args": args, "start_job": True, "num_prefixes_updated": len(data_sets)}
    except Exception as e:
        print(e)
        error = "ERROR in avernus_multi_obj_finder lambda_handler {}".format(str(e))
        print(error)
        sqs_helper.sqs_put([error])
        return {"start_job": False}
