# Pinnacle Ventures
# Dylan Byrne

def lambda_handler(event, context):
    """
    Create the args to run the EMR step. These have to be in a specific order. Check the main EMR Spark code
    to see the inputs fully.
    """
    log_index_key = event['exec_arn']

    args = [
        "spark-submit",
        "--deploy-mode",
        "client",
        "--py-files",
        event['EmrSparkZipPath'],
        event['EmrSparkMainPath'],
        "EMR",  # environment
        event['SettingsTable'],  # settings table
        log_index_key,  # index key for the main log
        # str(event['item']), # sort key for the main log
        event['input_type'],  # input type for the step (eg document, SQS)
        event['primary_input']  # more input info for the step (eg SQS URL)
    ]

    return args
