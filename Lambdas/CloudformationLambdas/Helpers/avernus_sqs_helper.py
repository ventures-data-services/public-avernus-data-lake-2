# Pinnacle Ventures
# Dylan Byrne
import boto3


class sqs_helper:

    def __init__(self, in_sqsUrl: str):
        self.sqs_url = in_sqsUrl

        self.sqsRes = boto3.resource('sqs')
        self.queue = self.sqsRes.Queue(self.sqs_url)

    @staticmethod
    def divide_chunks(in_list, in_size):
        """
        Divide a list into chunks of in_size
        """
        for i in range(0, len(in_list), in_size):
            yield in_list[i:i + in_size]

    @staticmethod
    def list_remove_dupes(in_list) -> list:
        """
        Remove all duplicates from a list
        """
        return list(dict.fromkeys(in_list))

    def sqs_put_batch(self, in_message_list):
        """
        Put a list of messages into an SQS queue as a batch

        in_messageList : a list of strings to send to SQS
        """
        sqs_batch_max = 10
        chunked_list = self.divide_chunks(in_message_list, sqs_batch_max)

        for chunk in chunked_list:
            message_batch = []
            id_int = 0
            for message in chunk:
                message_batch.append({"Id": str(id_int), "MessageBody": message})
                id_int = id_int + 1

            try:
                response = self.queue.send_messages(Entries=message_batch)
            except Exception as e:
                print("ERROR sending message batch, error is {}".format(str(e)))
                print("Message batch:")
                print(message_batch)

    def sqs_put(self, in_messageList):
        """
        Put a list of messages into an SQS queue

        in_messageList : a list of strings to send to SQS
        """
        for message in in_messageList:
            try:
                response = self.queue.send_message(MessageBody=message)
            except Exception as e:
                # print("ERROR")
                # print(e)
                print("MESSAGE")
                print(message)
                error = "Error encountered when trying to send SQS message, error: {0}".format(str(e))
                print(error)

                # try:
                #     response = self.queue.send_message(MessageBody=error)
                # except Exception as e:
                #     print("ERROR SENDING ERROR")
                #     print(e)

    def get_messages_from_queue(self, in_deleteMessages=True, in_removeDuplicateMessages=True):
        """
        Get and possibly delete all the messages from the SQS URL
        """
        concat = []
        still_messages = True

        # receive all messages
        while still_messages:
            message_list = self.queue.receive_messages(AttributeNames=['All'], MaxNumberOfMessages=10)

            if len(message_list) == 0:
                still_messages = False
                break

            for message in message_list:

                concat.append(message.body)

                if in_deleteMessages == True:
                    # Let the queue know that the message is processed
                    message.delete()
                    # print("[SPAM] message deleted")

        if in_removeDuplicateMessages == True:
            # remove message duplicates
            concat = self.list_remove_dupes(concat)

        return concat

    def get_message_batch_from_queue(self, in_batch_size=10, in_deleteMessages=True, in_removeDuplicateMessages=True):
        """
        Get and possibly delete a batch of messages from the SQS URL
        """
        concat = []
        still_messages = True

        # receive all messages
        while still_messages:
            message_list = self.queue.receive_messages(AttributeNames=['All'], MaxNumberOfMessages=10)

            if len(message_list) == 0:
                still_messages = False
                break

            for message in message_list:

                concat.append(message.body)

                if in_deleteMessages:
                    # Let the queue know that the message is processed
                    message.delete()
                    # print("[SPAM] message deleted")

            if len(concat) >= in_batch_size:
                still_messages = False
                break

        if in_removeDuplicateMessages:
            # remove message duplicates
            concat = self.list_remove_dupes(concat)

        return concat

    @property
    def check_objects_in_sqs(self) -> bool:
        """
        See if there's at least one message in the SQS
        """
        concat = []
        still_messages = True

        # receive all messages
        while still_messages:
            message_list = self.queue.receive_messages(AttributeNames=['All'], MaxNumberOfMessages=1)
            if len(message_list) == 0:
                return False
            else:
                return True
