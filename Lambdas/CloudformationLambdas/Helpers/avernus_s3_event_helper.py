# Ventures
# Dylan Byrne

def get_obj_key(in_s3Event):
    return in_s3Event["Records"][0]["s3"]["object"]["key"]


def get_obj_bucket(in_s3Event):
    return in_s3Event["Records"][0]["s3"]["bucket"]["name"]
