# Ventures Pinnacle Incorporated
# Anna Mulligan
# Dylan Byrne
import boto3
import decimal
from datetime import datetime, timezone
from Helpers import avernus_obj_meta_helper as metaHelper
from Helpers import bucketLister

'''
________          __       ___________
\______ \ _____ _/  |______\__    ___/__.__.______   ____
 |    |  \\__  \\   __\__  \ |    | <   |  |\____ \_/ __ \
 |    `   \/ __ \|  |  / __ \|    |  \___  ||  |_> >  ___/
/_______  (____  /__| (____  /____|  / ____||   __/ \___  >
        \/     \/          \/        \/     |__|        \/
  /$$$$$$                        /$$
 /$$__  $$                      | $$
| $$  \__/  /$$$$$$   /$$$$$$  /$$$$$$    /$$$$$$   /$$$$$$
|  $$$$$$  /$$__  $$ /$$__  $$|_  $$_/   /$$__  $$ /$$__  $$
 \____  $$| $$  \ $$| $$  \__/  | $$    | $$$$$$$$| $$  \__/
 /$$  \ $$| $$  | $$| $$        | $$ /$$| $$_____/| $$
|  $$$$$$/|  $$$$$$/| $$        |  $$$$/|  $$$$$$$| $$
 \______/  \______/ |__/         \___/   \_______/|__/
'''

'''
    Inputs: -config: dict with following attributes, nominally sent as part of a lambda trigger event
                - ddb_name: The table where your sorting rules are
                        - bucket_name: name of the bucket to find and sort the files
                        - data_dict: dictionary with descriptions of the different data types
                        - data_prefixes: dictionary with rules for mapping file names to
                          data types
                        - dict_access_rule: dictionary with rule for splitting filename to
                          obtain substring map to the data_prefixes dict
                - keyName: name of the id column in above database
                - keyValue: value of the id corresponding to the rules
                - ddb_name_log: name of database where events will be logged
                - sqs_url: url of the SQS queue where error messages will be sent
'''


class dataSorter:
    landing_prefix = "landing"

    def __init__(self, config):
        self.config = config
        self.ddb_name = config['ddb_name_setting']
        self.keyName = config['keyName']
        self.keyValueSort = config['keyValueSort']
        self.keyValueDataDict = config['keyValueDataDict']

        self.ddb_name_log = config['ddb_name_meta']
        self.sqs_url = config['sqs_url']
        self.current_unix = int(datetime.timestamp(datetime.now(timezone.utc)))
        self.state_arn = config["state_machine_arn"]

        self.region = config["region"]

        response = self.get_dynamo_data()
        if not response:
            print("ddb error")
        else:
            self.state_info = config['state_info']
            self.state_exec_arn = self.state_info['exec_arn']
            self.messages = self.state_info['new_sqs']
            self.bucket_name = config["bucket_name_raw"]
            self.keys = bucketLister.get_matching_s3_keys(self.bucket_name, prefix=self.landing_prefix)
            self.msg_id = 1

            self.metaHelper = metaHelper.meta_helper(self.ddb_name_log, self.ddb_name, self.region)

    def get_dynamo_data(self):
        """
            Retrieve rules and definitions for data type sorting
            from the dyanmo table
        """
        print("start get ddb")
        ddbRes = boto3.resource('dynamodb', region_name=self.region)
        ddbTable = ddbRes.Table(self.ddb_name)
        try:
            item = ddbTable.get_item(Key={self.keyName: self.keyValueDataDict})['Item']

            self.data_dict = item['value']

        except Exception as e:
            print("data dict ddb error")
            print(e)
            return False

        try:
            item = ddbTable.get_item(Key={self.keyName: self.keyValueSort})['Item']

            self.data_prefixes = item['value']
            self.dict_access_rule = item['dict_access_rule']

        except Exception as e:
            print("data prefixes ddb error")
            print(e)
            return False

        return True

    def sort_keys(self, full_key):
        """
            Given the filename, determine the datatype and move the file
            into the appropriate sub directory in the bucket
        """

        key = full_key.split("/")[-1]
        data_type = self.get_data_type(key)

        data_type = self.get_sub_type(key,
                                      data_type) if type(data_type) is dict else data_type

        print("[INFO] save object: key {0} data type {1} full key {2}".format(key, data_type, full_key))
        self.save_object(key, data_type, full_key)

        if "quarantine" in data_type:
            self.sqs_messages('{0} sent to quarantine prefix {1}'.format(key, str(self.current_unix)))

        return ({"DataType": data_type, "Object": key})

    def sqs_messages(self, message):
        """
            When a file has been quarantined, send a message to the SQS queue
        """
        sqs = boto3.resource('sqs')
        queue = sqs.Queue(self.sqs_url)
        response = queue.send_message(MessageBody=message)
        print('SQS queue message sent status : {0}'.format(response))

    def save_object(self, obj, obj_data_type, obj_path):
        """
            move the file to the sorted location
        """
        print("SAVE Object")
        print("Object: {}".format(obj))
        print("Obj data type: {}".format(obj_data_type))
        print("Obj Path: {}".format(obj_path))

        if obj_data_type == "quarantine":
            extra_prefix = str(self.current_unix) + "/"
        else:
            extra_prefix = ""

        s3Res = boto3.resource('s3')
        try:
            s3Res.Object(
                self.bucket_name,
                self.data_dict[obj_data_type]["Path"] + extra_prefix + obj).copy_from(
                CopySource=self.bucket_name + '/' + obj_path
            )
            s3Res.Object(self.bucket_name, obj_path).delete()
        except Exception as e:
            print("AVERNUS COMMENT: " + obj)
            print("AVERNUS ERROR " + str(e))

    def update_ddb_log(self, process_action, sorted_key, data_type):
        """
            Update the log database with the sort events for each of the data files.
        """
        # response = self.metaHelper.get_obj_meta(sorted_key.lower())
        response = self.metaHelper.get_obj_meta(sorted_key)

        if response == {}:
            self.metaHelper.create_obj_meta(
                sorted_key,
                # sorted_key.lower(),
                '{0}/'.format(data_type),
                data_type,
                self.bucket_name,
                in_optional={
                    "deleted": False,
                    "processed": False,
                    'sorter_action': {self.state_exec_arn: {'action': process_action,
                                                            'timestamp': decimal.Decimal(self.current_unix)}}},
                in_type='raw'
            )
        else:
            self.metaHelper.update_obj_meta(sorted_key, {
                'prefix': '{0}/'.format(data_type),
                'data_type': data_type,
                'deleted': False,
                'sorter_action': {
                    self.state_exec_arn: {'action': process_action, 'timestamp': decimal.Decimal(self.current_unix)}},
                'origin_bucket': self.bucket_name
            }, in_entry=response)

    def get_data_type(self, name):
        """
            Given the name of the file, find the prefix from the prefix dictionary
        """

        dt = None
        prefix = name.split(self.dict_access_rule['separator'])[int(self.dict_access_rule['index'])].lower()
        print("Prefix: {}".format(prefix))

        for key in self.data_prefixes:
            # print("Data Prefix key: {}".format(key))
            if key in prefix or prefix in key:
                print("data prefix in prefix dict")
                dt = self.data_prefixes[key]
                print("Data Type: {}".format(dt))
                break

        if dt is not None:
            if len(dt) == 1 and type(dt) == list:
                return dt[0]
            elif len(dt) > 1 or type(dt) == dict:
                dtype = list(dt.keys())
                dtype.sort(key=len, reverse=True)
                print("Data Type: {}".format(dtype))
                for prefix in dtype:
                    if prefix in name.lower():
                        return dt[prefix]
        print("quarantine")
        return "quarantine"

    def get_sub_type(self, name, data_type):
        """
        If the discovered data_type has sub-types, determine their values, for 1+ indexes
        """
        print("in get_sub_type")
        if data_type.get('sub_types'):
            if type(data_type['index']) == list:
                for index in data_type['index']:
                    result = self.get_single_subtype(name.lower(), data_type, index)
                    if result != "quarantine":
                        return result

                if "default" in data_type:
                    return data_type["default"]
                else:
                    return "quarantine"
            else:
                return self.get_single_subtype(name.lower(), data_type, data_type['index'])
        return data_type['prefix']

    def get_single_subtype(self, name, data_type, index):
        """
        Get the sub type info for a single index
        """
        if data_type.get('sub_types'):
            try:
                sub_type = name.lower().split(data_type['separator'])[int(index)]
                sub_type = data_type['sub_types'][sub_type]
                if 'seperator_sub_type' in data_type:
                    return data_type['prefix'] + data_type['seperator_sub_type'] + sub_type
                return data_type['prefix'] + "_" + sub_type
            except:
                return "quarantine"
        return data_type['prefix']
