# Ventures Pinnacle Incorporated
# Dylan Byrne
import boto3
import os

"""
Create environment variables ddb entry from the data lake cloudformation
"""


def lambda_handler(event, context):
    var_table = os.environ['variableTable']

    var_list = os.environ['cloudformationVarList'].split(',')
    print(var_list)

    config = create_avernus_config(var_list)

    response = write_to_ddb(config, var_table)
    print("[DDB] DDB response: {}".format(response))

    return "success"


def create_avernus_config(in_var_list):
    """
    Loop through environment variables to create a dict of data lake config
    """

    config_dict = {"setting_name": "data_lake_config"}

    for var_name in in_var_list:
        print(var_name)
        config_dict[var_name] = os.environ[var_name]

    print("[DDB] Variable dict:")
    print(config_dict)

    return config_dict


def write_to_ddb(in_dict, in_table_name):
    """
    Write a dict to a ddb table
    """
    ddbRes = boto3.resource('dynamodb')
    ddbTable = ddbRes.Table(in_table_name)
    response = ddbTable.put_item(Item=in_dict)

    return response
