# Ventures Pinnacle Incorporated
# Anna Mulligan
# Dylan Byrne

"""
________          __       ___________
\______ \ _____ _/  |______\__    ___/__.__.______   ____
 |    |  \\__  \\   __\__  \ |    | <   |  |\____ \_/ __ \
 |    `   \/ __ \|  |  / __ \|    |  \___  ||  |_> >  ___/
/_______  (____  /__| (____  /____|  / ____||   __/ \___  >
        \/     \/          \/        \/     |__|        \/
  /$$$$$$                        /$$
 /$$__  $$                      | $$
| $$  \__/  /$$$$$$   /$$$$$$  /$$$$$$    /$$$$$$   /$$$$$$
|  $$$$$$  /$$__  $$ /$$__  $$|_  $$_/   /$$__  $$ /$$__  $$
 \____  $$| $$  \ $$| $$  \__/  | $$    | $$$$$$$$| $$  \__/
 /$$  \ $$| $$  | $$| $$        | $$ /$$| $$_____/| $$
|  $$$$$$/|  $$$$$$/| $$        |  $$$$/|  $$$$$$$| $$
 \______/  \______/ |__/         \___/   \_______/|__/   
 """

from DataSorter import dataSorter


def lambda_handler(event, context):
    if "skip_sort" in event:
        if event["skip_sort"]:
            return {"process_id": event["exec_arn"]["exec_arn"]}

    event["config"]["state_info"] = {"exec_arn": event["exec_arn"]["exec_arn"], "new_sqs": event['config']['sqs_url']}
    ds = dataSorter(event['config'])
    print(ds.config)
    results = [ds.sort_keys(key) for key in ds.keys]
    print(results)
    return True
