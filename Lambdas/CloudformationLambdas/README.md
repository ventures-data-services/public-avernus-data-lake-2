# Avernus Cloudformation Lambda Code

Purpose and further detail on each of Lambdas can found in the READMEs located in the Cloudformation Templates directory.

## Description

This directory contains the code needed by the Avernus Cloudformation template to create it's required Lambda functions. All pieces of code are written in Python 3.6, and many of them import the pieces of code in the Helpers directory.

In order to be used by the Cloudformation templates compress this directory into a zip, then upload to an S3 Bucket your Cloudformation has access to.

## Templates

### Core Template Code

* avernus_variable_table_init.py

### Storage Template Code

* avernus_s3_put_meta.py
* avernus_s3_put_multi_meta.py
* avernus_delete_meta.py

### ETL Template Code

* avernus_dt_sorter.py
  * imports DataSorter.py
* avernus_emr_arg_maker.py
* avernus_emr_error_sqs.py
* avernus_emr_step_range.py
* avernus_log_sqs_checker.py
* avernus_multi_obj_finder.py
* avernus_sqs_to_sns.py
* find_current_sm_exec.py

### Data Pickup Template Code

* avernus_ferryman_log_checker.py

