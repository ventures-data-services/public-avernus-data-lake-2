import os
import boto3
from Helpers import avernus_obj_meta_helper
from Helpers import avernus_sqs_helper


def get_obj_key(in_event):
    return in_event["Records"][0]["s3"]["object"]["key"]


def get_obj_bucket(in_event):
    return in_event["Records"][0]["s3"]["bucket"]["name"]


def get_data_type_partitioned(in_objKey):
    ignore_prefixes = ["source_key", "_SUCCESS"]

    split_key = in_objKey.split("/")[0:2]

    if len(split_key) == 1 or 'source_key' in split_key[1]:
        return "{}/".format(split_key[0])

    for ignore in ignore_prefixes:
        if ignore in split_key[1]:
            return "{}/".format(split_key[0])

    return "{}/".format("/".join(in_objKey.split("/")[0:2]))


def force_multi_obj_meta(in_objKey, in_bucket, in_metaHelper):
    """
        Update/create the object meta ddb with the sort events for each of the data files.
    """

    ddb_response = in_metaHelper.get_obj_meta(in_objKey)

    if ddb_response == {}:
        print("creating meta")
        in_metaHelper.create_multi_obj_meta(
            in_objKey,
            in_bucket,
            in_objKey,
            in_optional={
                "deleted": False,
                "processed": False,
            }
        )


def create_unprocessed_log(in_dataSet, in_logTableName, region):
    ddbRes = boto3.resource('dynamodb', region_name=region)
    ddbTableLog = ddbRes.Table(in_logTableName)

    response = ddbTableLog.put_item(Item={
        "log_key": "unprocessed_multi_object",
        "log_type": in_dataSet
    })

    return response


def lambda_handler(event, context):
    try:
        ignore_words = ['_temporary', '$folder$', "_SUCCESS"]
        # Get Object info
        obj_key = get_obj_key(event)

        for ignore in ignore_words:
            if ignore in obj_key:
                print("ignored for {}".format(obj_key))
                return "process ignored"

        if '/' not in obj_key:
            print("ignored for {}".format(obj_key))
            return "process ignored"

        bucket = get_obj_bucket(event)

        # Set up AWS resources
        objMetaDDB = os.environ['metaTable']
        settingsDDB = os.environ['settingsTable']
        logDDB = os.environ['logTable']
        region = os.environ['region']

        metaHelper = avernus_obj_meta_helper.meta_helper(objMetaDDB, settingsDDB, in_region=region)

        data_set = get_data_type_partitioned(obj_key)

        # add/update dynamo with obj info
        force_multi_obj_meta(data_set, bucket, metaHelper)

        # create unprocessed log for data set for use in the T2N finder
        create_unprocessed_log(data_set, logDDB, region)

        print("process complete for {0}".format(obj_key))
        return "process complete for {0}".format(obj_key)
    except Exception as e:
        print(e)
        sqsHelper = avernus_sqs_helper(os.environ['sqs_url'])
        sqsHelper.put_sqs(["ERROR in avernus_s3_put_multi_meta {}".format(str(e))])
