# Pinnacle Ventures
# Dylan Byrne
import boto3
import os
from Helpers import bucketLister
from Helpers import avernus_obj_meta_helper as metaHelper
from Helpers import avernus_sqs_helper


def get_del_obj_key(in_delEvent):
    obj_key = in_delEvent['Records'][0]['s3']['object']['key'].split('/')[-1]
    return obj_key


def get_obj_type(in_delEvent):
    """
    Get the obj type from the bucket the obj is in
    """
    obj_bucket = in_delEvent['Records'][0]['s3']['bucket']['name']

    if 'raw' in obj_bucket:
        return 'raw'
    elif 'transformed' in obj_bucket:
        return 'transformed'
    else:
        print("[ERR] can not find type for bucket {0}".format(obj_bucket))
        return False


def ignore_sorting_event(event):
    """
    Check if the delete event should be ignored based on criteria
    """
    # this name is taken from the cloudformation
    sorter_lambda_name = "ObjectSorterLambda"
    try:
        user_identity = event['Records'][0]['userIdentity']['principalId']
        print(user_identity)
        if sorter_lambda_name in user_identity:
            return True
    except Exception as e:
        print(e)

    return False


def process_function_document(in_document):
    try:
        save_function_name = "save_df"

        if in_document["function_name"] == save_function_name:
            return in_document["function_input"]["save_path"]

        return None
    except Exception as e:
        print("[ERROR] in process_function_document avernus_delete_meta {0} with document {1}".format(str(e),
                                                                                                      in_document))
        sqs_helper = avernus_sqs_helper(os.environ['sqs_url'])
        sqs_helper.put_sqs([error])
        return None


def process_dag_document(in_document, in_settingsTable):
    save_list = []
    for function in in_document['function_list']:
        res = process_document(function, in_settingsTable)
        if res != None:
            save_list.append(res)

    return save_list


def process_document(in_documentKey, in_settingsTable):
    """
    Recursively process DAG document
    """
    doc = in_settingsTable.get_item(Key={"setting_name": in_documentKey})['Item']

    if 'function_input' in doc:
        return process_function_document(doc)
    elif 'function_list' in doc:
        return process_dag_document(doc, in_settingsTable)


def clean_save_df(in_string, in_bucket):
    remove_list = ["s3:", "{objName}", None, "", in_bucket, "s3a:"]
    split = in_string.split('/')
    split.remove("")

    for remove in remove_list:
        if remove in split:
            split.remove(remove)

    return '{}/'.format('/'.join(split))


def remove_suffix(in_objKey):
    return in_objKey.split(".")[0]


def delete_transformed_object_dags(in_objKey, in_targetBucket, in_metaHelper, in_settingsTableName, region):
    """
    Object in source bucket has been deleted, delete that obj also from target bucket
    obj_meta --> DAG name --> DAG document --> save_df --> location
    """
    s3Res = boto3.resource('s3')

    response = in_metaHelper.get_obj_meta(in_objKey, in_remove_extensions=True)
    print(in_objKey)
    print("dynamo response: {}".format(str(response)))

    if 'functions' in response:
        dag_list = response['functions']
    else:
        print("[ERROR] not functions in object meta for {}".format(in_objKey))
        return []

    saved_prefix_list = []
    deleted_list = []
    partition_format = "source_key={}/"

    ddbRes = boto3.resource('dynamodb', region_name=region)
    ddbTableSettings = ddbRes.Table(in_settingsTableName)

    for dag in dag_list:
        saved_prefix_list = saved_prefix_list + process_document(dag, ddbTableSettings)

    for prefix in saved_prefix_list:
        s3_prefix = clean_save_df(prefix, in_targetBucket) + partition_format.format(remove_suffix(in_objKey)).format(
            transformed_bucket_name=in_targetBucket)
        print(s3_prefix)

        for obj in bucketLister.get_matching_s3_keys(in_targetBucket, prefix=s3_prefix):
            s3Res.Object(in_targetBucket, obj).delete()
            deleted_list.append(obj)

    return deleted_list


def convert_obj_key_to_partition_name(in_objKey):
    return in_objKey.split('/')[-1].split('.')[0]


def delete_transformed_object_s3_list(in_objKey, in_bucket):
    s3Res = boto3.resource('s3')
    delete_list = []

    for obj in bucketLister.get_matching_s3_keys(in_bucket):
        if in_objKey in obj:
            delete_list.append(obj)
            s3Res.Object(in_bucket, obj).delete()

    return delete_list


def lambda_handler(event, context):
    try:
        if ignore_sorting_event(event) == True:
            print("Ignoring sorter delete event")
            return True

        obj_key = get_del_obj_key(event)
        obj_type = get_obj_type(event)

        if obj_type == False:
            # Send SNS here
            return False

        objMetaTable = os.environ['metaTable']
        settingsTable = os.environ['settingsTable']
        transformedBucket = os.environ['transformedBucket']
        region = os.environ['region']

        # update object meta
        meta_helper = metaHelper.meta_helper(objMetaTable, settingsTable, in_region=region)

        resp = meta_helper.update_obj_meta(obj_key, {"deleted": True}, in_meta_type=obj_type)

        if resp == False:
            print("[OBJDEL] No obj meta to update for {0} {1}, ignoring".format(obj_key, obj_type))
        else:
            print("[OBJDEL] added delete true to {} {}".format(obj_key, obj_type))

        # delete transformed objects
        deleted_transformed_objects = delete_transformed_object_dags(obj_key, transformedBucket, meta_helper,
                                                                     settingsTable,
                                                                     region)
        print("[INFO] Deleted transformed objects {0}".format(deleted_transformed_objects))

        return True
    except Exception as e:
        print(e)
        error = "ERROR in avernus_s3_put_meta {}".format(str(e))
        sqs_helper = avernus_sqs_helper(os.environ['sqs_url'])
        sqs_helper.put_sqs([error])
        return False
