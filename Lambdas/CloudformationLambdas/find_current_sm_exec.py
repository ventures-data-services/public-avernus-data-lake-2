# Pinnacle Ventures
# Dylan Byrne
import boto3
from datetime import datetime, timezone


def find_current_sm_exec(in_state_arn, in_sqs_messages, in_unix):
    """
    Find the current ARN of the state machine execution
    """
    print("[FCSE] In find_current_sm_exec")
    client = boto3.client('stepfunctions')
    response = client.list_executions(
        stateMachineArn=in_state_arn,
        statusFilter='RUNNING',
        maxResults=2
    )

    if len(response['executions']) != 0:
        exec_arn = response['executions'][0]['executionArn']

        if len(response['executions']) > 1:
            print("[FCSE] WARNING!!! Multiple state machine executions running, defaulting to most recent {0}".format(
                exec_arn))
            if 'admin' not in in_sqs_messages:
                in_sqs_messages = {'admin': [
                    "WARNING!!! Multiple state machine executions running, defaulting to most recent{0}".format(
                        exec_arn)]}
            else:
                in_sqs_messages['admin'].append(
                    "WARNING!!! Multiple state machine executions running, defaulting to most recent{0}".format(
                        exec_arn))

        return {'exec_arn': exec_arn, 'new_sqs': in_sqs_messages}
    else:
        print("[FCSE] WARNING!!! No state machine running, ignore if testing lambda function")
        return {'exec_arn': 'no_exec_{0}'.format(str(in_unix)), 'new_sqs': in_sqs_messages}


def lambda_handler(event, context):
    current_unix = int(datetime.timestamp(datetime.now(timezone.utc)))
    messages = []

    sm_exec_arn = find_current_sm_exec(event["config"]["state_machine_arn"], messages, current_unix)

    print(messages)

    return sm_exec_arn
