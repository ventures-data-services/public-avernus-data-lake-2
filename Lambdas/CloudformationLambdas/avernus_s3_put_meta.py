import os

from Helpers import avernus_obj_meta_helper
from Helpers import avernus_sqs_helper


def get_obj_key(in_event):
    return in_event["Records"][0]["s3"]["object"]["key"]


def get_obj_bucket(in_event):
    return in_event["Records"][0]["s3"]["bucket"]["name"]


def check_key_ignored(in_key):
    """
    check to see if the key should be ignored (in ignored prefix)
    """

    # ignore root level
    if '/' not in in_key:
        return True

    ignore_prefixes = ["quarantine", "landing"]
    check_key = in_key.split('/')[0]

    if check_key in ignore_prefixes:
        return True

    return False


def get_data_type(in_objKey):
    return "/".join(in_objKey.split("/")[:-1])


def update_ddb_log(in_objKey, in_bucket, in_metaHelper, in_metaType):
    """
    Update/create the object meta ddb with the sort events for each of the data files.
    """

    ddb_response = in_metaHelper.get_obj_meta(in_objKey, in_metaType)

    split_key = in_objKey.split("/")
    data_type = get_data_type(in_objKey)
    key = split_key[-1]

    prefixes = "{0}/".format("/".join(split_key[:-1]))

    if ddb_response == {}:
        print("creating meta")
        in_metaHelper.create_obj_meta(
            key,
            prefixes,
            data_type,
            in_bucket,
            in_optional={
                "deleted": False,
                "processed": False,
            },
            in_type=in_metaType
        )
    else:
        print("updating meta")
        result = in_metaHelper.update_obj_meta(key, {
            'prefix': prefixes,
            'data_type': data_type,
            'deleted': False,
            'origin_bucket': in_bucket
        }, in_entry=ddb_response)
        if result == False:
            print("Object meta failed to update: {0}".format(key))


def lambda_handler(event, context):
    try:
        # Get Object info
        obj_key = get_obj_key(event)
        split_obj_key = obj_key.split("/")
        unprefixed_obj_key = split_obj_key[-1]
        # unsuffixed_obj_key = unprefixed_obj_key.split(".")[0]
        bucket = get_obj_bucket(event)

        # check if ignored
        if check_key_ignored(obj_key) == True:
            print("obj {0} ignored prefix".format(obj_key))
            return "obj {0} ignored prefix".format(obj_key)

        # Set up AWS resources
        objMetaDDB = os.environ['metaTable']
        settingsDDB = os.environ['settingsTable']
        single_obj_proc_sqs = os.environ['sopSqsUrl']
        meta_type = "raw"
        region = os.environ['region']

        metaHelper = avernus_obj_meta_helper.meta_helper(objMetaDDB, settingsDDB, in_region=region)

        unsuffixed_obj_key = metaHelper.remove_object_extensions(unprefixed_obj_key)

        # add/update dynamo with obj info
        update_ddb_log(obj_key, bucket, metaHelper, meta_type)

        # add object to SQS
        sop_sqsHelper = avernus_sqs_helper.sqs_helper(single_obj_proc_sqs)
        sop_sqsHelper.sqs_put([unsuffixed_obj_key])

        print("process complete for {}".format(obj_key))
        return "process complete for {}".format(obj_key)
    except Exception as e:
        print(e)
        error = "ERROR in avernus_s3_put_meta {}".format(str(e))
        sqsHelper = avernus_sqs_helper(os.environ['sqs_url'])
        sqsHelper.put_sqs([error])
