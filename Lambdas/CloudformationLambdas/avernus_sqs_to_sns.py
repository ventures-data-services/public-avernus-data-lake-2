# Pinnacle Ventures
# Dylan Byrne
import json
from datetime import datetime

import boto3


def lambda_handler(event, context):
    sqs_url = event["config"]["sqs_url"]
    sns_topic = event["config"]["sns_topic"]

    messages = get_messages_from_queue(sqs_url, True)
    if len(messages) == 0 and get_end_type(event) == "Success":
        return "No need to send message"

    print(messages)

    formatted_message = format_messages_email(messages, event)

    print("VVV FULL MESSAGE VVV")
    print(formatted_message)

    try:
        log_table_name = get_log_table_from_event(event)
        archive_message_ddb(log_table_name, formatted_message, event)
    except Exception as e:
        error = "[ERROR] error archiving message, error is {}".format(str(e))
        print(error)

    if not sns_size_threshold(formatted_message):
        formatted_message = format_too_large_message(event, len(messages))

    response = send_message_to_topic(sns_topic, formatted_message)

    return response


def get_log_table_from_event(in_event) -> str:
    """
    Get the log table Dynamo DB name from the event
    :param in_event: step function event
    :return: name of the Dynamo DB log table
    """
    return in_event["config"]["ddb_name_log"]


def format_too_large_message(in_event, in_message_number) -> str:
    """
    Format a message with basic information if the size of the full message is too large
    :param in_event: the current event from the step function
    :param in_message_number: number of messages received
    :return formatted_message: message with basic information
    """
    formatted_message = "FULL MESSAGE TOO LARGE TO SEND \n " \
                        "{1}" \
                        "Avernus End Type: {0} \n " \
                        "Number of messages: {2}" \
                        "Look in the lambda " \
                        "log for the print of the full message, " \
                        "search for FULL MESSAGE.".format(get_end_type(in_event),
                                                          get_step_function_name(in_event),
                                                          str(in_message_number))

    return formatted_message


def get_current_time() -> int:
    """
    Get current UNIX timestamp
    :return:  unix timestamp
    """
    # Calculate UTC Now Timestamp
    d = datetime.utcnow()
    epoch = datetime(1970, 1, 1)
    time_epoch = (d - epoch).total_seconds()
    return round(time_epoch)


def archive_message_ddb(in_log_table_name, in_messages, in_event):
    region = in_event['config']['region']
    ddb_res = boto3.resource('dynamodb', region_name=region)
    ddb_table_log = ddb_res.Table(in_log_table_name)

    # check if exec arn is in event
    if 'exec_arn' not in in_event:
        log_key = "unknown_{}".format(str(get_current_time()))
    else:
        log_key = in_event['exec_arn']['exec_arn']

    item = {
        "log_key": log_key,
        "log_type": "sns_message",
        "messages": in_messages,
        "exec_output": in_event,
        "timestamp": get_current_time()
    }

    response = ddb_table_log.put_item(Item=item)
    print("[INFO] archiving message ddb response {}".format(str(response)))


def get_end_type(in_event):
    """
    Get the end type (success/fail) of this execution
    """
    try:
        end_type = in_event['endType']
    except Exception as e:
        end_type = 'none'
        print("[ERROR] error as {}".format(str(e)))

    return end_type


def get_messages_from_queue(in_sqs_url, in_delete_messages=True):
    """
    Get and delete all the messages from the SQS URL
    """
    concat = []
    still_messages = True
    # Get the service resource
    sqs = boto3.resource('sqs')

    # Get the queue
    queue = sqs.Queue(in_sqs_url)

    # receive all messages
    while still_messages:
        message_list = queue.receive_messages(AttributeNames=['All'], MaxNumberOfMessages=10)

        if len(message_list) == 0:
            still_messages = False
            break

        for message in message_list:

            concat.append(message.body)

            if in_delete_messages:
                # Let the queue know that the message is processed
                message.delete()

    # remove message duplicates
    concat = list_remove_dupes(concat)

    return concat


def list_remove_dupes(in_list):
    return list(dict.fromkeys(in_list))


def send_message_to_topic(in_sns_topic, in_message):
    """
    Sends an sns message to a topic ARN

    Inputs:
    in_content : the content of the message to send
    in_snsARN : the ARN of the sns topic to send the message to

    """
    sns = boto3.client('sns')
    response = sns.publish(TopicArn=in_sns_topic, Message=in_message, )

    return response


def format_messages_email(in_message_list, in_state_input) -> str:
    """
    Format the list of messages into a pretty human readable format
    :param in_message_list: list of messages to send
    :param in_state_input: step function state input
    :return: message string
    """
    step_function_name = get_step_function_name(in_state_input)

    message = "{2} Avernus end type : {0} \nNumber of messages : {1} \n".format(get_end_type(in_state_input),
                                                                                str(len(in_message_list)),
                                                                                step_function_name)
    message = message + "\nMessages: \n"
    in_message_list.sort()
    for mes in in_message_list:
        message = message + "- {0} \n".format(mes)

    formatted_input = json.dumps(in_state_input, sort_keys=True, indent=4)
    message = message + "\nStep Function Output: \n {0} \n".format(formatted_input)

    return message


def get_step_function_name(in_state_input):
    return in_state_input['config']['state_machine_arn'].split(":")[-1]


def sns_size_threshold(in_str):
    """
    Check if a string is above the byte threshold for SNS

    in_str : the string to check
    """
    size = len(in_str.encode('utf-8'))
    sns_size_threshold_bytes = 256000

    if size > sns_size_threshold_bytes:
        return False
    else:
        return True
