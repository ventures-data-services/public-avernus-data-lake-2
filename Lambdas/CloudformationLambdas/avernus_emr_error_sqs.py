import boto3
from Helpers import avernus_sqs_helper


def lambda_handler(event, context):
    sqs_helper = avernus_sqs_helper.sqs_helper(event['sqs_url'])

    try:
        message = "Error processing EMR step for {0} data set. Error {1}".format(event['item'],
                                                                                 event['error']['emr_step']['Error'])
    except Exception as e:
        message = "Error processing EMR step for {0} data set.".format(event['item'])
        print("Couldn't find error")

    sqs_helper.sqs_put([message])

    return message
