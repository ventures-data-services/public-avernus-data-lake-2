import boto3


def check_objects_in_sqs(in_sqs_url):
    """
    See if there's at least one message in the SQS
    """
    still_messages = True
    # Get the service resource
    sqs = boto3.resource('sqs')

    # Get the queue
    queue = sqs.Queue(in_sqs_url)

    # receive all messages
    while still_messages:
        message_list = queue.receive_messages(AttributeNames=['All'], MaxNumberOfMessages=1)
        if len(message_list) == 0:
            return False
        else:
            return True


def lambda_handler(event, context):
    sop_sqs = event["config"]["single_object_process_sqs_url"]
    start_etl = check_objects_in_sqs(sop_sqs)
    if not start_etl:
        return {"start_job": start_etl}

    range_count = 6
    args = {
        "--ITERATE_LIST": [str(i) for i in range(1, range_count)],
        "--INPUT_TYPE": "QUEUE",
    }

    return {"glue_args": args, "start_job": start_etl}
