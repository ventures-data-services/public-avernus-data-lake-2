# Pinnacle Ventures
# Dylan Byrne
from typing import List

import boto3

from Helpers import avernus_obj_meta_helper
from Helpers import avernus_sqs_helper
from Helpers import dynamo_helper

import datetime


def get_current_time() -> int:
    """
    Get current UNIX timestamp
    """
    # Calculate UTC Now Timestamp
    d = datetime.datetime.utcnow()
    epoch = datetime.datetime(1970, 1, 1)
    time_epoch = (d - epoch).total_seconds()
    return round(time_epoch)


def get_obj_ddb_log(in_ddb_log_res, in_obj_key, in_sort_key) -> dict:
    """
    Get the object's specific log from the log ddb table
    """
    primary_key = "log_key"
    sort_key = "log_type"
    # print('[INFO] Getting error log: primary : {0}  sort : {1}'.format(in_obj_key, in_sort_key))

    try:
        item = in_ddb_log_res.get_item(Key={primary_key: in_obj_key, sort_key: in_sort_key})['Item']
        # print(item)
        return item
    except Exception as e:
        print("[ERR][ETL-LOG-CHECKER] Error getting object log from dynamo: {0}".format(in_obj_key))
        # print("[ERR][ETL-LOG-CHECKER] Error getting object log from dynamo")
        # print(e)
        # print({primary_key : in_objKey, sort_key : in_sortKey})
        return {}


def update_successful_obj(in_obj, in_meta_helper, in_process_time):
    """
    Update a successfully processed obj's meta in the obj meta ddb
    """
    new_info = {"processed": True, "last_process_time": in_process_time, "failed": False}
    in_meta_helper.update_obj_meta(in_obj, new_info)


def update_failed_obj(in_obj, in_exec_arn, in_sqs_helper, in_ddb_log_res, in_meta_helper, in_process_time, in_sort_key):
    """
    Update a failed object's meta in the obj meta ddb
    :param in_obj: object's name in the meta table
    :param in_exec_arn: execution ARN/process name
    :param in_sqs_helper: SQS helper object for error notifications
    :param in_ddb_log_res: log Dynamo DB table boto3 resource
    :param in_meta_helper: avernus object meta helper object
    :param in_process_time: current unix process time
    :param in_sort_key: sort key of the object's log document in Dynamo DB
    :return: None
    """
    error_message_key_process = "process_break_error"
    error_message_key_function = "function_error"
    log_sort_key = in_sort_key

    # update meta
    new_info = {"etl_error": [in_exec_arn], "failed_process_time": [in_process_time], "failed": True, "processed": True}
    in_meta_helper.update_obj_meta(in_obj, new_info)

    # send SQS
    obj_log = get_obj_ddb_log(in_ddb_log_res, in_obj, log_sort_key)
    error_message = ""

    if obj_log != {}:
        if error_message_key_process in obj_log:
            error_message = "Process Error: " + obj_log[error_message_key_process]

        if error_message_key_function in obj_log:
            if error_message == "":
                error_message = "Function Error: " + str(obj_log[error_message_key_function])
            else:
                error_message = ", Function Error: " + str(obj_log[error_message_key_function])

        if error_message != "":
            in_sqs_helper.sqs_put(["{obj} failed to process during the process. Error is {error}".format(
                obj=in_obj,
                error=error_message)
            ])
        else:
            in_sqs_helper.sqs_put([
                "{obj} failed to process during the process, but NO ERROR was found in the ETL log.".format(
                    obj=in_obj)])
    else:
        in_sqs_helper.sqs_put(
            ["{obj} failed to process during the process, but there was NO LOG for the object.".format(obj=in_obj)])


def split_log_messages(in_log_message_list: list) -> dict:
    """
    Split a list of log strings in the form "<object name> <process name> <outcome>"

    :param in_log_message_list: list of log strings to be split
    :return save_prefixes: unique list of all Multi Object data sets that will need to be updated
    """
    # EXAMPLE MESSAGE: BP0136_HISTORY_20200314234542 ReapplyingSchema success
    # obj sort type
    log_message_dict = {}

    for message in in_log_message_list:
        try:
            # fail/success messages split by spaces "<object name> <process name> <success/fail>"
            message_split = message.split(' ')
            log_message_dict[message_split[0]] = {"log_sort_key": message_split[1], "log_type": message_split[2]}
        except Exception as e:
            error_message = "Error processing {0} message, error is {1}".format(message, str(e))

    return log_message_dict


def process_log_dict(in_log_dict, in_log_table_name, in_meta_table_name, in_setting_table_name, in_notification_url,
                     in_exec_arn, in_region="ap-southeast-2"):
    """
    Process all split logs, as either success, fail, or unknown, by updating appropriate obj meta and sending error
    notifications
    :param in_log_dict: list of split log dictionaries to be processed
    :param in_log_table_name: name of the log Dynamo DB table
    :param in_meta_table_name: name of the object meta Dynamo DB table
    :param in_setting_table_name: name of the settings Dynamo DB table
    :param in_notification_url: the SQS URL for notifications
    :param in_exec_arn: current execution ARN/process name
    :return: None
    """
    # init
    meta_helper = avernus_obj_meta_helper.meta_helper(in_meta_table_name, in_setting_table_name, in_region=in_region)
    notification_sqs_helper = avernus_sqs_helper.sqs_helper(in_notification_url)

    ddb_res = boto3.resource('dynamodb', region_name=in_region)
    ddb_table_log = ddb_res.Table(in_log_table_name)

    settings_ddb_helper = dynamo_helper.DynamoDB_Helper(in_setting_table_name, regionName=in_region)
    obj_meta_ddb_helper = dynamo_helper.DynamoDB_Helper(in_meta_table_name, regionName=in_region)

    process_time = get_current_time()

    processed_data_sets = []

    # process
    for log in in_log_dict:
        try:
            # successful log process
            if in_log_dict[log]["log_type"] == "success":
                update_successful_obj(log, meta_helper, process_time)
                data_sets = get_multi_object_data_sets(log, meta_helper, settings_ddb_helper, obj_meta_ddb_helper,
                                                       notification_sqs_helper)
                processed_data_sets = add_to_multi_object_data_sets(data_sets, processed_data_sets)
            # failed log process
            elif in_log_dict[log]["log_type"] == "fail":
                update_failed_obj(
                    in_obj=log,
                    in_exec_arn=in_exec_arn,
                    in_sqs_helper=notification_sqs_helper,
                    in_ddb_log_res=ddb_table_log,
                    in_meta_helper=meta_helper,
                    in_process_time=process_time,
                    in_sort_key=in_log_dict[log]["log_sort_key"])
            # unknown log process
            else:
                process_unknown_log(log, in_log_dict[log], notification_sqs_helper)

        except Exception as e:
            error_message = "Error processing {0} log, error is {1}".format(log, str(e))
            notify_error(notification_sqs_helper, error_message)

    # create ddb process logs
    for ds in processed_data_sets:
        resp = create_unprocessed_log(ds, ddb_table_log)
        print("[INFO] Created process file for {0}, ddb response {1}".format(ds, str(resp)))


def add_to_multi_object_data_sets(in_new_data_sets, in_current_data_sets) -> list:
    """
    Create a list of unique data sets to be updated
    :param in_new_data_sets: list of new data sets
    :param in_current_data_sets: list of the current data sets
    :return: unique list of data sets to be updated
    """
    for ds in in_new_data_sets:
        if ds not in in_current_data_sets:
            in_current_data_sets.append(ds)

    return in_current_data_sets


def get_multi_object_data_sets(in_log, in_meta_helper, in_setting_ddb_helper, in_obj_meta_helper,
                               in_notification_sqs_helper, in_notify_no_functions=False) -> List[str]:
    """
    Get the Multi Object data sets that need to be updated if the inputted log was processed

    :param in_log: the log key (object meta name)
    :param in_meta_helper: object meta helper object
    :param in_setting_ddb_helper: a dynamo helper set up for the settings table
    :param in_obj_meta_helper: a dynamo helper set up for the object meta table
    :param in_notification_sqs_helper: avernus SQS helper object for notification sending
    :param in_notify_no_functions: whether to send a notification message if the function key is missing from the object
    meta
    :return save_prefixes: unique list of all Multi Object data sets that will need to be updated
    """
    # removes multi object logs which won't have the functions key
    if '/' in in_log:
        return []

    # get obj meta for obj
    obj_meta = in_meta_helper.get_obj_meta(in_log)
    # get each DAG for obj
    save_prefix_key = "object_creation_prefixes"
    function_key = "functions"
    save_prefixes: List[str] = []

    if function_key in obj_meta:
        for dag in obj_meta[function_key]:
            dag_document = in_setting_ddb_helper.GetDocumentOnce(dag)

            # get all save df documents for the DAG
            for function in dag_document['function_list']:
                function_document = in_setting_ddb_helper.GetDocumentOnce(function)
                if 'function_name' in function_document:
                    if function_document['function_name'] == "save_df":
                        if save_prefix_key in function_document:
                            for prefix in function_document[save_prefix_key]:
                                # add save prefix to list if not there
                                if prefix not in save_prefixes:
                                    save_prefixes.append(prefix)
                                    force_multi_obj_meta(
                                        prefix,
                                        get_bucket_from_save_document(function_document),
                                        in_meta_helper,
                                        in_obj_meta_helper
                                    )
                        else:
                            error_message = "[ERROR] {0} key does not exist for {1} function doc".format(
                                save_prefix_key,
                                function_document['setting_name'])
                            notify_error(in_notification_sqs_helper, error_message)
    else:
        error_message = "[ERROR] {0} meta does not have the {1} key, ignore if multi object".format(
            obj_meta, function_key
        )
        print(error_message)
        if in_notify_no_functions:
            notify_error(in_notification_sqs_helper, error_message)

    return save_prefixes


def get_bucket_from_save_document(in_document) -> str:
    """
    Get the S3 Bucket name from the save path in a save df function document
    :param in_document: save_df function document
    :return: S3 bucket name
    """
    return in_document["function_input"]["save_path"].split("/")[2]


def force_multi_obj_meta(in_obj_key, in_bucket, in_meta_helper, in_obj_meta_helper):
    """
    Update/create the object meta ddb with the sort events for each of the data files.
    """

    ddb_response = in_obj_meta_helper.GetDocumentOnce(in_obj_key, "main")

    if ddb_response == {}:
        print("creating meta for {}".format(in_obj_key))
        in_meta_helper.create_multi_obj_meta(
            in_obj_key,
            in_bucket,
            in_obj_key,
            in_optional={
                "deleted": False,
                "processed": False,
            }
        )


def create_unprocessed_log(in_data_set, in_log_table_ddb):
    """
    Create a unprocessed log in the log table, used to create a process file for multi object ETL
    :param in_data_set: name of the data set eg bpac/inline/
    :param in_log_table_ddb: log table dynamo db resource
    :return: the dynamo db response to the put operation
    """
    response = in_log_table_ddb.put_item(Item=dict(log_key="unprocessed_multi_object", log_type=in_data_set))
    return response


def process_unknown_log(in_log_key, in_log_type, in_sqs_helper):
    """
    Logic for processing a log with an unknown outcome
    :param in_log_key: key of object
    :param in_log_type: the outcome type of the log
    :param in_sqs_helper: avernus sqs helper object
    :return: None
    """
    error_message = "{obj} had an unknown log type of {type}".format(obj=in_log_key, type=in_log_type)
    notify_error(in_sqs_helper, error_message)


def notify_error(in_sqs_helper, in_error_message):
    """
    Print and send SQS of error
    :param in_sqs_helper: avernus SQS helper object
    :param in_error_message: the error message
    :return: None
    """
    print(in_error_message)
    in_sqs_helper.sqs_put([in_error_message])


def list_remove_dupes(in_list) -> list:
    """
    Remove all duplicates from a list
    """
    return list(dict.fromkeys(in_list))


def lambda_handler(event, context) -> dict:
    """
    Process the SQS log queue
    :param event: AWS lambda event
    :param context: AWS lambda context
    :return: {"log_check_again": True/False}
    """
    # init
    log_sqs_url = event['config']['log_object_process_sqs_url']
    notification_sqs_url = event['config']['sqs_url']
    log_table_name = event['config']['ddb_name_log']
    meta_table_name = event['config']['ddb_name_meta']
    setting_table_name = event['config']['ddb_name_setting']
    exec_arn = event['exec_arn']['exec_arn']
    region = event['config']['region']

    # set batch size
    if "log_check_message_batch_size" in event["config"]:
        try:
            message_batch_size = int(event["config"]["log_check_message_batch_size"])
            if message_batch_size < 1:
                print("[INFO] Set message batch size less than 1, setting to 1")
                message_batch_size = 1
        except Exception as e:
            message_batch_size = 500
            print("[ERROR] Error setting batch size, setting to default 1000, error was {}".format(str(e)))
    else:
        message_batch_size = 500

    # get log keys from sqs
    log_sqs_helper = avernus_sqs_helper.sqs_helper(log_sqs_url)

    log_messages = log_sqs_helper.get_message_batch_from_queue(message_batch_size, in_deleteMessages=True)

    print("[INFO] Message dump")
    print(log_messages)

    # get whether they where success or fail
    log_dict = split_log_messages(log_messages)
    print("[INFO] Number of messages to process {}".format(str(len(log_dict))))
    print("[INFO] Split log types dump")
    print(log_dict)

    # process each log message
    process_log_dict(
        in_log_dict=log_dict,
        in_log_table_name=log_table_name,
        in_meta_table_name=meta_table_name,
        in_setting_table_name=setting_table_name,
        in_notification_url=notification_sqs_url,
        in_exec_arn=exec_arn,
        in_region=region
    )

    if log_sqs_helper.check_objects_in_sqs:
        return {"log_check_again": True}
    else:
        return {"log_check_again": False}
