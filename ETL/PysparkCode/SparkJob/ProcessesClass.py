import datetime
import traceback

from SparkJob.Helpers import AWS_Helper as aws
from SparkJob.Helpers import LogsClass as lc
from SparkJob.Helpers import UniversalFunctions as uf

from SparkJob.Helpers import SpecialErrors as spex
from SparkJob.SparkFunctions import _AllSparkFunctions as AS

""" 
EEEE     TTTTTT     L    
E          TT       L    
EEE        TT       L    
E          TT       L    
EEEE       TT       LLLL 
"""


class ExtractTransformLoad:
    """
    \nBase class for Processing objects based on their Meta Data and functions lists.
    \nThe base class requires the following arguments:
    \ninput:
        spark: Initiated Spark
        processEnvironment: Environment name (option are LOCAL, EMR, GLUE) (string)
    """

    def __init__(self, processKey, spark, processEnvironment, EnvironmentalTable=None, metadataTable=None, **args):

        self.SetupProcessMeta(processKey)

        self.spark = spark
        self.SparkAssistant = AS.SparkUnifiedFunctions(
            spark, processEnvironment)

        if EnvironmentalTable is not None:
            self.SetupEnvironmentalTable(EnvironmentalTable)

        if metadataTable is not None:
            self.SetupMetadataTable(metadataTable)

        self.archivedDataframes = []
        self.FailedFunctions = []

    def SetupEnvironmentalTable(self, EnvironmentalTable):
        uf.CheckVariable(EnvironmentalTable,
                         "EnvironmentalTable", aws.DynamoDB_Helper)
        self.EnvironmentTable = EnvironmentalTable

    def SetupMetadataTable(self, metadataTable):
        uf.CheckVariable(metadataTable, "metadataTable", aws.DynamoDB_Helper)
        self.metadataTable = metadataTable

    def SetupProcessMeta(self, processKey):
        uf.CheckVariable(processKey, "processKey", str)
        self.processID = processKey

    def ValidateObjectMeta(self, objMeta):
        """
        Validates the Object Meta
        """

        if not(isinstance(objMeta, dict)):
            raise spex.WrongType('objMeta', 'Dict', str(type(objMeta)))

        requiredKeys = ["dataframe_details", "prefix", "origin_bucket"]

        missingKeys = [rK for rK in requiredKeys if rK not in objMeta]
        if len(missingKeys) > 0:
            raise spex.MissingDictionaryKey("objMeta", missingKeys)

    def ValidateFunctionsInput(self, functionInput):
        """
        Validates function input
        """
        formatKeys = []

        if isinstance(functionInput, dict):
            for value in functionInput:
                functionInput[value] = self.ValidateFunctionsInput(
                    functionInput[value])

        # if isinstance(functionInput, list):
        #     functionInput = [self.ValidateFunctionsInput(lV) for lV in functionInput]

        if isinstance(functionInput, str):
            formatKeys = uf.FindKeysInFormattedString(functionInput)

        if len(formatKeys) > 0:
            values = {}
            for k in formatKeys:
                values[k] = getattr(self, k)
            return functionInput.format(**values)
        else:
            return functionInput

    def ValidateFunctionDictionary(self, inputDict):
        """
        Iterates through the function dictionary and validates all keys
        """
        validatedInputDictinary = {}
        for functionInput in inputDict:
            validatedInputDictinary[functionInput] = self.ValidateFunctionsInput(
                inputDict[functionInput]
            )
        return validatedInputDictinary

    def ProcessFunction_FullProcess(self, functionName, functionInput, updateDF=True):
        """
        Processes a single function --> Assumes the function is in the environmentalTable and has the key 'function_name' which has been set up on in the SparkAssistantFunction
        \nExpects to always get df and logs from the function if it works, and will push up the error if it is uncaught within the function.
        \n:return Spark DataFrame, either post processing or as give, depending on the updateDF flag (True= Changed, False=Given)
        """
        self.currentUpdateDF = updateDF
        functionDictionary = self.EnvironmentTable.GetDocumentOnce(
            functionName)

        # If document not found
        if functionDictionary is None:
            raise spex.NotFoundDocument(
                self.EnvironmentTable.tableName, functionName)

        if 'multi_object_process_applied_data_sets' in functionDictionary:
            for appliedDataSets in functionDictionary['multi_object_process_applied_data_sets']:
                self.CreateObject(objKey=appliedDataSets)

        # If it contains a function_list, it is a DAG and is iterated throughout
        if "function_list" in functionDictionary:
            return self.IterateThroughDAG(functionDictionary=functionDictionary)

        # update_df means that the dataframe is returned, therefore the next DAG/Function will start with it.
        # This needs to be determined on the DAG for most processes, unless applying a single independent function.
        if "update_df" in functionDictionary:
            self.updateDF = functionDictionary["update_df"]

        if "function_input" in functionDictionary:
            if functionInput is None:
                functionInput = self.ValidateFunctionDictionary(
                    functionDictionary["function_input"])
            else:
                raise spex.RequirementError(
                    "if a function dictionary contains `function_input`, you cannot manually overwrite it", "FindSparkFunctionAndApply")

        # If it contains function_input, it is a function that applies to the dataframe OR context
        # This is defined in the document
        if "function_input" in functionDictionary:
            self.FindSparkFunctionAndApply(
                functionDictionary=functionDictionary, updateDF=updateDF, functionInput=functionInput)

        return self.temp_df

    def IterateThroughDAG(self, functionDictionary):
        """
            If the document contains a list of functions, 
            those will be iterated through in order as a Direct Acyclical Graph and each function applied with it's settings.
            Default is to make the tempDF the original after the DAG is done, unless mentioned otherwise in the functionDictionary
        """
        keepDF = False
        if "keep_df" in functionDictionary:
            keepDF = functionDictionary['keep_df']

        if keepDF is False:
            self.archivedDataframes.append(self.temp_df)

        functionList = functionDictionary["function_list"]
        for functionName in functionList:
            try:
                self.ProcessFunction(functionName, df=self.temp_df)
            except Exception as e:
                if keepDF is True:
                    dagName = self.EnvironmentTable.GetDocumentIndex(
                        functionDictionary)
                    raise spex.TrackedFunctionError(dagName, e)
                else:
                    self.temp_df = self.archivedDataframes.pop()
                    raise e

        if keepDF is False:
            self.temp_df = self.archivedDataframes.pop()

        return

    def FindSparkFunctionAndApply(self, functionDictionary, updateDF, functionInput):
        """
        Applys a function and either saves the result in the spark context (updateDF=False) or updates the temp_df within the class (updateDF=True).
        If the functionInput is added to a document that already has inputs, it will throw an error since this should not occur within this architecture.

        - functionDictionary: Function document from the settings table (Dictionary)
        - updateDF: Determines whether to update the temp_df variable within the class or not (Boolean)
        - functionInput: Manually inserted inputs, only for functions in testing or that have no default/built-in inputs (Dictionary or None)
        """

        # Process Function
        functionAsVar = uf.FindFunctionInClass(
            functionDictionary["function_name"], self.SparkAssistant
        )

        if updateDF is True:
            self.temp_df = functionAsVar(self.temp_df, **functionInput)

        if updateDF is False:
            functionAsVar(self.temp_df, **functionInput)

        return

    def ProcessFunction(self, functionName, df=None, functionInput=None):
        """
        Base for the function process, creates validations, fills needed variables and catches can go here if necessary.
        :return The function's process, using the given DF.
        """
        self.temp_df = df

        if functionInput is not None:
            if len(functionInput) == 0:
                functionInput = None

        # For testing
        print(functionName)

        try:
            self.ProcessFunction_FullProcess(functionName, functionInput)
        except Exception as e:
            if self.currentUpdateDF is True:
                raise spex.TrackedFunctionError(functionName, e)
            else:
                raise e

        return self.temp_df

    def CreateObject(self, objKey=None, objMeta=None):
        if objMeta is None:
            objMeta = uf.GetObjMetadata(self.metadataTable, objKey)

        df = self.SparkAssistant.CreateDataFrame_BasedOnType(
            objMeta['dataframe_details'])
        return df

    def FailedFunction(self, logClass, functionName, error):
        """
        For single object process only.
        """
        logClass.AddToLog_List(
            "function_error",
            "Function {} failed with error {}".format(functionName, error),
        )
        self.FailedFunctions.append(functionName)

        # For testing
        print("AVERNUS ERROR!")
        traceback.print_exc()

    def ErrorChecking(self):
        if len(self.FailedFunctions) > 0:
            failedFunctionList = ",".join(self.FailedFunctions)
            errorMessage = "The following functions threw an error in the process: {}".format(
                failedFunctionList
            )
            raise Exception(errorMessage)

    def ClassDictionary(self):
        """
        Returns the variables as a dictionary
        """
        return self.__dict__


class SingleObj_Process(ExtractTransformLoad):
    def __init__(
        self,
        spark,
        metadataTable,
        logTable,
        EnvironmentTable,
        processKey,
        processEnvironment,
        inputValue,
        **args
    ):
        """
        SingleObj should be mostly functional transformations at this stage

        Make the save a part of the functions process, not the program itself as it shouldn't assume.
        """

        super().__init__(processKey, spark, processEnvironment,
                         EnvironmentalTable=EnvironmentTable, metadataTable=metadataTable)

        self.objName = inputValue

        objMeta = uf.GetObjMetadata(self.metadataTable, self.objName)
        self.ValidateObjectMeta(objMeta)
        self.objMeta = objMeta

        self.objOriginBucket = objMeta["origin_bucket"]
        self.objOriginPrefix = objMeta["prefix"]

        self.objMeta_logs = lc.LogHelper()
        self.objMeta_logs.PrepareForDynamo(
            logTable, self.objName, self.processID)

        print("------------")
        print("Started for {}".format(self.objName))

    def IterateThroughFunctions_ObjMeta(self):
        """
        Goes through the functions key in the object meta and sends them to the basic process
        """

        if "functions" not in self.objMeta or len(self.objMeta["functions"]) == 0:
            raise Exception(
                "'functions' key not set up for the object metadata.")

        for functionName in self.objMeta["functions"]:
            try:
                self.df = self.ProcessFunction(functionName, df=self.df)

            except spex.TrackedFunctionError as tfe:
                self.FailedFunction(self.objMeta_logs, functionName, tfe)
                raise tfe

            except Exception as e:
                self.FailedFunction(self.objMeta_logs, functionName, e)
                continue

    def AvernusProcess(self, **kwargs):
        """
        The script for the Avernus process, this is the basic that should be applying to all.
        """
        createdDF = False

        try:
            self.df = self.CreateObject(objMeta=self.objMeta)
            createdDF = True
        except Exception as e:
            self.FailedFunction(self.objMeta_logs,
                                "InitialDataFrameCreation", e)

        if createdDF is True:
            try:
                self.IterateThroughFunctions_ObjMeta()
            except spex.TrackedFunctionError as tfe:
                self.objMeta_logs.AddValue("process_break_error", tfe.message)

        self.objMeta_logs.AddValue(
            "spark_helper_logs", self.SparkAssistant.logs.ReturnLog()
        )

        self.objMeta_logs.SaveLog_DefinedTable()

        # For Testing
        self.objMeta_logs.PrintLog()

        self.ErrorChecking()

    def ReturnLogs(self):
        return self.objMeta_logs


class MultipleObjs_Process(ExtractTransformLoad):
    def __init__(
        self,
        spark,
        metadataTable,
        logTable,
        EnvironmentTable,
        processKey,
        processEnvironment,
        objsMeta,
        **args
    ):
        """
        Multiple OBJ should be mostly sequential transformations, at the current stage
        To do: 
            - Add Dataframes to context after creation
            - Add all at once, 
            - Dependencies should be on the objMeta to objMeta (Create set of all Paths)
            - The process dictionaries should contain the full DAG, so that they can remove repetitions without removing other DAGs Dependencies or having to repeat the data state.
        """
        super().__init__(processKey, spark, processEnvironment,
                         EnvironmentalTable=EnvironmentTable, metadataTable=metadataTable)

        self.objsMeta = objsMeta

        self.SetupMultiObjMetaLogs(logTable)

        self.functionsDictionary = {}

    def SetupMultiObjMetaLogs(self, logTable):
        self.multi_objMeta_logs = {}
        for objMeta in self.objsMeta:
            keyName = objMeta["obj_key"]
            self.multi_objMeta_logs[keyName] = lc.LogHelper()
            self.multi_objMeta_logs[keyName].PrepareForDynamo(
                logTable, keyName, self.processID
            )

    def AddToMultiObjMetaLogs(self, key, value, objList):
        for o in objList:
            self.multi_objMeta_logs[o].AddToLog_List(key, value)

    def SaveMultiObjMetaLogs(self):
        for o in self.multi_objMeta_logs:
            self.multi_objMeta_logs[o].SaveLog_DefinedTable()

    def ApplyFunctions(self):
        for functionName in self.functionsDictionary:
            try:
                self.ProcessFunction(functionName)
                self.AddToMultiObjMetaLogs(
                    "applied_function",
                    functionName,
                    self.functionsDictionary[functionName]["applied_objects"],
                )
            except Exception as e:
                errorKey = "{}_error".format(functionName)
                errorMessage = "The function threw the error {}".format(e)
                self.AddToMultiObjMetaLogs(
                    errorKey,
                    errorMessage,
                    self.functionsDictionary[functionName]["applied_objects"],
                )
                self.FailedFunctions.append(functionName)

                # For testing
                print("AVERNUS ERROR!")
                traceback.print_exc()

    def UpdateFunctionsDictionary(self, objName, functionList):
        for functionName in functionList:
            if functionName not in self.functionsDictionary:
                self.functionsDictionary[functionName] = {
                    "applied_objects": []}
            self.functionsDictionary[functionName]["applied_objects"].append(
                objName)

    def StartObjectMeta(self, objMeta):

        self.SparkAssistant.add_obj_to_context(objMeta["dataframe_details"])

        if "functions" in objMeta:
            self.UpdateFunctionsDictionary(
                objMeta["obj_key"], objMeta["functions"])
        return

    def AvernusProcess(self, **kwargs):
        """
        The script for the Avernus process, this is the basic that should be applying to all.
        """
        try:
            for objMeta in self.objsMeta:
                self.StartObjectMeta(objMeta)
            self.ApplyFunctions()
            self.SaveMultiObjMetaLogs()
        except Exception as e:
            self.SaveMultiObjMetaLogs()
            raise e

        self.ErrorChecking()


class Function_Specific_Process(ExtractTransformLoad):
    """
    Function centric; it received the function and a list of objects to apply that function to, instead of the other way around.
    """

    def __init__(
        self,
        spark,
        metadataTable,
        logTable,
        EnvironmentTable,
        processKey,
        processEnvironment,
        inputValue,
        **args
    ):

        super().__init__(processKey, spark, processEnvironment,
                         EnvironmentalTable=EnvironmentTable, metadataTable=metadataTable)

        self.functionName = inputValue

        self.functionalCall_logs = lc.LogHelper()
        self.functionalCall_logs.PrepareForDynamo(
            logTable, self.functionName, self.processID)

        # For Testing
        print("------------")
        print("Started for {}".format(self.functionName))

    # def AvernusProcess_LEGACY(self, **kwargs):
    #     """
    #     [DO NOT USE]
    #     The main function; runs the passed function with the given inputs
    #     """

    #     # Set up the input dictionary
    #     nonInputKeys = ["function_name"]
    #     manual_inputDictionary = {k: self.functionDetails[k] for k in set(
    #         list(self.functionDetails.keys())) - set(nonInputKeys)}

    #     try:
    #         self.ProcessFunction(
    #             self.functionName, functionInput=manual_inputDictionary)
    #     except Exception as e:
    #         self.functionalCall_logs.AddValue("process_error", e)

    #     self.functionalCall_logs.AddValue(
    #         "spark_helper_logs", self.SparkAssistant.logs.ReturnLog()
    #     )
    #     self.functionalCall_logs.SaveLog_DefinedTable()
    #     self.ErrorChecking()

    #     # For Testing
    #     self.functionalCall_logs.PrintLog()

    def AvernusProcess(self, **kwargs):
        """
        The main function; runs the passed function with the given inputs
        """

        try:
            self.ProcessFunction(self.functionName)
        except Exception as e:
            self.FailedFunction(self.functionalCall_logs, self.functionName, e)

        self.functionalCall_logs.AddValue(
            "spark_helper_logs", self.SparkAssistant.logs.ReturnLog()
        )
        self.functionalCall_logs.SaveLog_DefinedTable()
        self.ErrorChecking()

        # For Testing
        self.functionalCall_logs.PrintLog()