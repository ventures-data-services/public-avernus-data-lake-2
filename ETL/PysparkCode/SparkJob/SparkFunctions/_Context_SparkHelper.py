from SparkJob.SparkFunctions import Raccoon as Baby
from SparkJob.SparkFunctions import SparkAssistant as s
from SparkJob.Helpers import UniversalFunctions as uf


class ContextFunctions(s.Assistant):
    def add_dataframe_to_context(self, df, contextName="df"):
        """
        Adds a dataframe to the context so that SQL queries can be applied to it.
        """
        quick_raccoon = Baby.Raccoon(df)
        quick_raccoon.AddDataframeToContext(contextName)
        return

    def sql_query(self, df, sql_query):
        """
        Runs the query against the q context and returns result as DataFrame
        """
        return self.spark.sql(sql_query)

    def apply_sql_to_single_obj(self, sql, df=None):
        """
        Applies a SQL Query to a single object, returns it's dataframe
        """
        return self.sql_query(sql)

    def join_context_tables(self, logic, df=None):
        """
        Joins tables in the context based on a sql logic
        """
        return self.sql_query(logic)

    def append_to_table(self, df, table_name, clean_name=False):
        """
        Unionise a df to a table, update the table with this info
        """
        if clean_name is True:
            table_name = uf.CleanString(table_name)

        table_df = self.return_table_from_context(table_name)
        unionised_table_df = table_df.union(df)
        self.sqlContext.registerDataFrameAsTable(unionised_table_df, table_name)
        return

    def return_table_from_context(self, dataframe_name):
        """
        Returns a dataframe from the Spark context
        """
        query = "SELECT * FROM {}".format(dataframe_name)
        return self.sql_query(query)
