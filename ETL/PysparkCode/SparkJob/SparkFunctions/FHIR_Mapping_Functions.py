import pyspark.sql.functions as F

def nested_column_build_type(in_entry, in_key):
    '''
    Used by create_nested_column
    Recursively build the new type based on the arg entry
    '''
    if in_entry["type"] == "array":
        return nested_column_null_empty_array(F.array(*nested_column_build_array(in_entry["value"])).alias(in_key), in_key).alias(in_key)

    elif in_entry["type"] == "struct":
        return F.struct(*nested_column_build_struct(in_entry["value"])).alias(in_key)

    elif in_entry["type"] == "lit":
        return F.lit(in_entry["value"]).alias(in_key)

    elif in_entry["type"] == "col":
        if "blank_if_null" in in_entry:
            if in_entry["blank_if_null"] == True:
                return F.when(F.col(in_entry["value"]).isNull(), F.lit("blank").alias(in_key)).otherwise(
                    F.col(in_entry["value"]).alias(in_key)).alias(in_key)
        return F.col(in_entry["value"]).alias(in_key)

def nested_column_build_array(in_value):
    '''
    Used by create_nested_column
    build a list of values for an array
    '''
    val_list = []
    for value in in_value:
        val_list.append(nested_column_build_type(value, "none"))

    return val_list

def nested_column_build_struct(in_value):
    '''
    Used by create_nested_column
    build a list of values for a struct
    '''
    val_list = []
    for value in in_value:
        val_list.append(nested_column_build_type(in_value[value], value))

    return val_list

def nested_column_null_empty_array(in_array, in_alias):
    '''
    Used by create_nested_column
    Null an array if it is empty or contains only nulls
    '''
    return F.when((F.size(in_array) == 0), F.lit(None).alias(in_alias)).otherwise(in_array.alias(in_alias))