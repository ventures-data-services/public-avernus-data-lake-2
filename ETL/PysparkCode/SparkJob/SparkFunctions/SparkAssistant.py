import pyspark
from pyspark.sql import SQLContext

from SparkJob.Helpers import LogsClass as L
from SparkJob.Helpers import UniversalFunctions as uf


class Assistant:
    def __init__(self, spark, environment):
        uf.CheckVariable(spark, "spark", pyspark.sql.session.SparkSession)

        self.spark = spark
        self.sqlContext = SQLContext(spark.sparkContext)

        self.logs = L.LogHelper(addTimeStamp=False)
        self.environment = environment

        self.logs.AddValue("environment", environment)

    def ReturnLogs(self):
        """
        Return the class logs.
        """
        return self.logs.ReturnLog()

    def ReStartLogs(self):
        """
        Re-start the logs, so it can be empty.
        """
        self.logs = L.LogHelper()
        return
