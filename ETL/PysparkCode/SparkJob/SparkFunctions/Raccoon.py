"""
Helper class that minimises the repeatition of code when modifyinig the Spark DataFrame.
"""

import pyspark.sql.functions as F
from pyspark.sql import DataFrame

from SparkJob.Helpers import UniversalFunctions as uf

"""
                        ,,,
                     .'    `/\_/\
                   .'       <@I@>
        <((((((((((  )____(  \./
                   \( \(   \(\(
 racoon             `-"`-"  " "
 """


class Raccoon:
    """
    Racoon is named after the sweetest animal to ever exist. Raccoons are lovenly nicknamed 'Trash Panda', and since this class exists to help treat the Spark DF like a Pandas DF, for ease of reading and debugging, it felt fitting.
    """

    def __init__(self, df):
        self.df = df
        uf.InstanceInforcer(df, DataFrame)

    def AddDataframeToContext(self, contextName="df"):
        if contextName == "":
            raise Exception(
                "No context name received when applying AddDataframeToContext"
            )

        self.df.createOrReplaceTempView(contextName)

    def GetNewName(self, newName, oldName):
        """
        Creates new column name in case one is not given

        newName -> New column name (string) 
        oldName -> Old column name (string)
        """
        if newName == "":
            return oldName
        else:
            return newName

    def cleanColumn(self, colName):
        """
        Remove unwanted characters, and replace those with an underscore.

        colName -> Column name to be cleaned (string)
        Returns newName -> Cleaned column name
        """
        cleanColumnName = uf.CleanString(colName)
        cleanColumnName = "_".join(cleanColumnName.split())
        if cleanColumnName[-1] == "_":
            cleanColumnName = cleanColumnName[:-1]
        return cleanColumnName

    def cleanDfColumns(self):
        """
        Iterates through the columns in the DF and cleans them from any unwanted characters and lowers them.
        """
        # Create list with all DataFrame Columnss
        ColList = self.df.schema.names

        # Iterate through list of columns
        for ColName in ColList:
            newName = self.cleanColumn(ColName)
            self.ChangeColName(ColName, newName)

    def DropColumns(self, columnsList):
        """
        Drops all the columns that exist in the DF that are in the columnsList

        columnsList --> List of strings with names of columns to be removed [is case-sensitive] (list of strings)
        """
        self.df = self.df.drop(*columnsList)

    def DropColumnsContains(self, contains_string):
        """
        Drops all the columns that exist in the DF that contain a string in the contains string

        contains_string --> string that if contained in a column name, will have the column removed [is case-sensitive] (string)
        """
        col_names = self.df.schema.names
        for name in col_names:
            if contains_string not in name:
                col_names.remove(name)

        self.df = self.df.drop(*col_names)

    def GetNullColumns(self):
        """
        Returns list of column names that are completely null (No value inside)
        """
        currentCount = self.df.count()
        nullColsList = [
            c for c in self.df.columns if self.IsColumnNull(c, currentCount) is True
        ]
        return nullColsList

    def IsColumnNull(self, c, currentCount=None):
        """
        Checks whether the column is empty

        c --> column name (string)
        currentCount --> Size of the DF (int)
        """
        if currentCount is None:
            currentCount = self.df.count()
        if self.GetColSize(c) == currentCount:
            return True
        else:
            return False

    def GetColumns(self):
        return self.df.columns

    def GetColSize(self, columnName):
        """
        Retuns size of column [As in, number of rows that contain any value]

        columnName --> The name of the column [Shocking!] (string)
        """
        return self.df.filter(F.col(columnName).isNotNull()).count()

    def ChangeColName(self, oldColName, newColName):
        """
        Changes a DFs column name

        oldColName --> Old column name (string)
        newColName --> New column name (string)
        """
        self.df = self.df.withColumnRenamed(oldColName, newColName)

    def GetSchema(self):
        """
        Returns the DFs Schema
        """
        return self.df.dtypes

    def GetUniqueColumns(self, columns):
        """
        Transform DF into a DF of only distinct values from the list of columns 

        columns --> list of column names in the DF (list of strings)
        """
        self.df = self.df.select(columns).distinct()

    def NewColumnOnCondition_SQL(self, newColName, expression):
        """
        Create a new column based on a SQL expression (Such as a CASE statement)

        newColName --> column name (string)
        expression --> SQL statement, for example "WHEN 1 THEN 1 END" would make this new column all 1(s). 
        Please note, in order for it to have access to the DF by name, the dataframe must have been added to the Spark context first. 
        """
        self.df = self.df.withColumn(newColName, F.expr(expression))
        return

    def ColUniqueValues(self, columnName):
        """
        Return a list of unique values found in a column (CAREFUL >> VERY SLOW PROCESS)

        columnName --> Name of column to be searched (string)
        Returns distinct list of values found
        """

        if not (isinstance(columnName, list)):
            columnName = [columnName]

        df_uniqueSources = self.df.select(columnName).distinct()

        df_uniqueSources_size = df_uniqueSources.count()

        list_uniqueSources = df_uniqueSources.collect()

        UniqueColValues = {}

        for colNum in range(len(columnName)):
            UniqueColValues[columnName[colNum]] = [
                list_uniqueSources[row][colNum] for row in range(df_uniqueSources_size)
            ]

        return UniqueColValues

    def ColumnFilter(self, columnName, columnValue, filterType="eq"):
        """
        Filter DF based on a column, value and type of filtering.
        You should be able to determine what the options are based on the code below.

        columnName --> name of column to be filtered by (string)
        columnValue --> Value which filtering is done based on (string, int, list)
        filterType --> Which data to keep based on filtering process [options are below] (string)
        """

        if filterType == "eq":
            self.df = self.df.filter(self.df[columnName] == columnValue)

        if filterType == "noteq":
            self.df = self.df.filter(self.df[columnName] != columnValue)

        if filterType == "lte":
            self.df = self.df.filter(self.df[columnName] <= columnValue)

        if filterType == "lt":
            self.df = self.df.filter(self.df[columnName] < columnValue)

        if filterType == "gte":
            self.df = self.df.filter(self.df[columnName] >= columnValue)

        if filterType == "gt":
            self.df = self.df.filter(self.df[columnName] > columnValue)

        if filterType == "in":
            self.df = self.df.filter(self.df[columnName].isin(columnValue))

        if filterType == "notin":
            self.df = self.df.filter(~self.df[columnName].isin(columnValue))

    def TimestampColumnFilter(self, columnName, columnValue, filterType="eq"):
        """
        Filter column based on timestamp this is diferent to ColumnFilter because it requires some extra settings in the code.

        columnName --> Name of column to be filtered by (string)
        columnValue --> Date as string to be compared (string)
        filterType --> Which data to keep based on filtering process [options are below] (string)
        """

        if filterType == "eq":
            self.df = self.df.filter(
                F.col(columnName)
                == F.unix_timestamp(F.lit(columnValue)).cast("timestamp")
            )

        if filterType == "gt":
            self.df = self.df.filter(
                F.col(columnName)
                > F.unix_timestamp(F.lit(columnValue)).cast("timestamp")
            )

        if filterType == "gte":
            self.df = self.df.filter(
                F.col(columnName)
                >= F.unix_timestamp(F.lit(columnValue)).cast("timestamp")
            )

        if filterType == "lt":
            self.df = self.df.filter(
                F.col(columnName)
                < F.unix_timestamp(F.lit(columnValue)).cast("timestamp")
            )

        if filterType == "lte":
            self.df = self.df.filter(
                F.col(columnName)
                <= F.unix_timestamp(F.lit(columnValue)).cast("timestamp")
            )

        return

    def NewColumnOnCondition(self, newColName, condition, valueTrue, valueFalse):
        """
        Boolean Function that creates a new column from the input values based on whether a condition is true or false to each row.

        newColName --> Column name where results will be put (string) 
        condition --> Condition which will be checked for each row  (PySpark Arg) [Example F.lower(F.col(colName)) == "null" ]
        valueTrue --> If true, what value should be in this column (any type that can be in a PySpark DF)
        valueFalse -- > If false, what value should be in this column (any type that can be in a PySpark DF)
        """
        self.df = self.df.withColumn(
            newColName, F.when(condition, valueTrue).otherwise(valueFalse)
        )
        return

    def ApplyFunctionToColumn(self, columnName, sparkFunction):
        """
        Apply PySpark Function to a column, for specific function that are used so rarely they don't require pre-settings.

        columnName --> Name of column which will have function applied to (string)
        sparkFunction --> Spark function which can be applied to a column (Check online or PySpark's API to find which are available[Any function for DataFrame])
        """
        self.df = self.df.withColumn(columnName, sparkFunction(columnName))

    def NewColumn(self, newColName, newColValues):
        """
        Create new column with same valeu throughout.

        newColName --> Name of column which will have value. (string)
        newColValues --> Value which will be repeated throughtout. Make sure to make it spark.function.Lit(value) when calling this function, if it is a simple string or int. (Spark Functions Lit type) 
        """
        self.df = self.df.withColumn(newColName, newColValues)

    def FillBlanks(self, columnName, Value):
        """
        For all blanks in column name, fill with same value.

        columName --> Name of column (string)
        Value --> F.lit value to be inserted in the place of Blanks/Nulls (Spark Functions Lit type)
        """
        self.df = self.df.fillna({columnName: Value})

    def DeleteNones(self, columnName):
        """
        Remove all rows were column is None

        columName --> Name of column (string)        
        """
        self.df = self.df.where(F.col(columnName).isNotNull())

    def WhereNones(self, columnName):
        """
        Keep only rows where a value in column is None

        columName --> Name of column (string)
        """
        self.df = self.df.where(F.col(columnName).isNull())

    def JoinDF(self, r_df, leftList, rightList, joinType="left"):
        """
        Join this Dataframe to another.

        r_df --> The Spark Dataframe which will be joined against. (Spark DataFrame)
        leftList --> List of columns from the left DF to be joined by (list) [MUST be in order]
        rightList --> List of columns from the right DF to be joined by (list) [MUST be in order]
        joinType --> Which type of join this will be [To know which options are available, check spark doc] (string)
        """
        joinList = [self.df[lc] == r_df[rc]
                    for (lc, rc) in zip(leftList, rightList)]
        self.df = self.df.join(r_df, joinList, how=joinType)

    def ConvertCol(self, columnName, columnType, newName="", dateFormat=""):
        """
        Convert a column to a specific data type.

        columName --> Name of column (string)
        columnType --> Type it will be converted to.(string or Spark Types)
        newName --> Use if you would like to save this as a new column. (string)
        """

        if newName == "":
            newName = columnName

        if dateFormat == "":
            self.df = self.df.withColumn(
                newName, F.col(columnName).cast(columnType))
            return

        if columnType == "timestamp":
            return self.ConvertColToDatetime(columnName, newName, dateFormat)

        if columnType == "date":
            return self.ConvertColToDate(columnName, newName, dateFormat)

    def ConvertColToDatetime(self, columnName, newName="", dateFormat=""):
        """
        Convert a column to a a datetime data type.

        columName --> Name of column (string)
        columnType --> Type it will be converted to.(string or Spark Types)
        newName --> Use if you would like to save this as a new column. (string)
        dateFormat --> Format which the datetime is in the column (e.g 2019-01-01 is %y-%m-%d) (string)
        """

        newColumnName = self.GetNewName(newName, columnName)

        if dateFormat == "":
            self.df = self.df.withColumn(
                newColumnName, F.col(columnName).cast("timestamp")
            )
        else:
            self.df = self.df.withColumn(
                newColumnName,
                F.unix_timestamp(F.col(columnName),
                                 dateFormat).cast("timestamp"),
            )

    def ConvertColToDate(self, columnName, newName="", dateFormat=""):
        """
        Convert a column to a a date data type. NOTE: Column must be a DATETIME type already

        columName --> Name of column (string)
        newName --> Use if you would like to save this as a new column. (string)
        """
        newColumnName = self.GetNewName(newName, columnName)

        if dateFormat == "":
            self.df = self.df.withColumn(
                newColumnName, F.col(columnName).cast("date"))
        else:
            self.df = self.df.withColumn(newColumnName, F.unix_timestamp(
                F.col(columnName), dateFormat).cast("timestamp").cast("date"))

    def NewColumn_Default(self, columnName, value):
        """
        Create new column on DF, with name and value that are defined
        """
        self.NewColumn(columnName, F.lit(value))

    def HashColumn_SHA2(self, columnName, newName=""):
        """
        Hash a column with SHA encryption (256)
        colName --> Column name to be hashed
        """

        if newName == "":
            newName = columnName

        self.df = self.df.withColumn(newName, F.sha2(F.col(columnName), 256))

    def ColumnRounding(self, columnName, roundTo, newName=""):
        """
        Round an float column down to x [roundTo] values.
        columnName --> The column's name.
        roundTo --> How many decimals to round to.
        newName --> If you want to save with a new name, add it here,
        """
        if newName == "":
            newName = columnName

        self.df = self.df.withColumn(
            newName, F.round(F.col(columnName), int(roundTo)))

    def ColumnDivision(self, columnName, value, newName=""):
        """
        For Int/Float columns, divides whatever value is in column.
        """
        newColumnName = self.GetNewName(newName, columnName)
        self.df = self.df.withColumn(newColumnName, F.col(columnName) / value)

    def Agg_AllColumnsExceptOne(self, columnName, agg_method, newName=""):
        """
        Function aggregates all other columns except for one, and aggregate method on top - for example, to find the minimum of a value while keeping all other columns the same.
        """

        newColumnName = self.GetNewName(newName, columnName)

        # Get required aggregate function from Spark Function
        method_to_call = getattr(F, agg_method)

        aggExpressions = method_to_call(F.col(columnName))

        # Remove the column to be ignored from the list of columns
        columnList = [col for col in self.df.schema.names if col != columnName]

        # Groupby + Aggregation of the data
        self.df = self.df.groupBy(*columnList).agg(
            (aggExpressions).alias(newColumnName)
        )

    def AddTimeToCol(
        self,
        columnName,
        newName="",
        days=0,
        hours=0,
        minutes=0,
        secs=0,
        additionType="positive",
    ):
        """
        For DateTime columns -> It adds days/times to said column
        columnName -> Name of column being used [string]
        newName -> Result columnName [string]
        days/ hours/minutes/secs -> You can figure this out, come on (how many to add to the datetime col ) [int]
        additionType -> Options are 'positive' or 'negative' [string]
        """
        timeAdded = 0
        timeDict = {"days": 60 * 60 * 24,
                    "hours": 60 * 60, "minutes": 60, "secs": 1}

        if days != 0:
            timeAdded = days * timeDict["days"]

        if hours != 0:
            timeAdded = hours * timeDict["hours"]

        if minutes != 0:
            timeAdded = minutes * timeDict["minutes"]

        if secs != 0:
            timeAdded = secs * timeDict["secs"]

        newColumnName = self.GetNewName(newName, columnName)

        if additionType.lower() == "positive":
            self.df = self.df.withColumn(
                newColumnName,
                (F.unix_timestamp(columnName) + timeAdded).cast("timestamp"),
            )

        if additionType.lower() == "negative":
            self.df = self.df.withColumn(
                newColumnName,
                (F.unix_timestamp(columnName) - timeAdded).cast("timestamp"),
            )

    def DropDuplicates(self, specificColumns=None, ignoredColumns=None):
        """
        Deletes duplicates from dataframe
        specificColumns -> Use this if you want to only delete duplicates from rows that repeat on a column
        ignoreColumns -> Columns to ignore on duplication [NOT YET DONE]
        """
        if specificColumns is None and ignoredColumns is None:
            self.df = self.df.dropDuplicates()

        if specificColumns is not None:
            self.df = self.df.dropDuplicates(specificColumns)

    def GetDfSize(self):
        """
        Returns INT with number of columns in DF
        """
        return self.df.count()

    def PersistDF(self):
        """
        Keeps DF in memory, so that the path can be overwritten.
        """
        self.df = self.df.persist()

    def ShowDF(self, numRows=10):
        return self.df.show(numRows, truncate=False)

    def GetDF(self):
        """
        Returns PySpark DF
        """
        return self.df

    def SaveDF(self, saveLocation, numOfReparitions=0, saveFormat="parquet", saveMode="overwrite", partitionBy=[], coalesceBy=1):
        """
        Saves the DF as parquet files
        saveLocation -> Path as string
        numOfReparitions -> Number of files saved
        saveFormat -> Type of saving, as mentioned in Spark Documentation
        """

        saveInputs = {"format": saveFormat,
                      "path": saveLocation, "mode": saveMode}

        # Repartitions selects the number of files saved for the full dataframe
        # If there are partitions, the bare minimum is one per partition
        if isinstance(numOfReparitions, int) and numOfReparitions > 0:
            self.df = self.df.repartition(numOfReparitions)
            coalesceBy = numOfReparitions

        # Saves one file per partition by default, this can be changed by changing the coalesceBy variable
        if len(partitionBy) > 0:
            writeObject = self.df.coalesce(coalesceBy).write
            writeObject = writeObject.partitionBy(partitionBy)
        else:
            writeObject = self.df.write

        return writeObject.save(**saveInputs)