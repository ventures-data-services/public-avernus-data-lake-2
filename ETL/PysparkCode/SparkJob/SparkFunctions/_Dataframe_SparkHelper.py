import pyspark.sql.functions as F

from SparkJob.Helpers import UniversalFunctions as fu
from SparkJob.Helpers import SpecialErrors as spex
from SparkJob.SparkFunctions import Raccoon as Baby
from SparkJob.SparkFunctions import SparkAssistant as s
from SparkJob.SparkFunctions import _ObjToSpark as o2s

from SparkJob.SparkFunctions import FHIR_Mapping_Functions as fhir_functions


class DataFrameFunctions(s.Assistant):
    def __init__(self, *args):
        super().__init__(*args)
        self.object_opener = o2s.ObjConversion_Functions(*args)

    def required_columns(self, df, required_columns_list):
        """
        Checks wether the column names are acceptable and all the necessary columns are in it 
        """
        # Set up variables
        raccoon_df = Baby.Raccoon(df)
        rawfile_dfSchema = set(raccoon_df.GetColumns())

        # Check wether the required columns are in the schema
        invalidValues = list(set(required_columns_list) - rawfile_dfSchema)

        # Throw error if not
        if len(invalidValues) > 0:
            raise spex.MissingColumns("required_columns", invalidValues)

        df = raccoon_df.GetDF()
        return df

    def convert_columns(self, df, columns_dictionaries, percentage_accepted_errors=50.0, broken_rows_column=True):
        """
        Checks the data types in the columns and inforces the stablished DType in the allowedSchema
        \ndf: Spark DataFrame to be checked
        \ncolumn_dictionaties: list of dictionaries that contain the required details {columnName:name, columnType:type}
        \npercentage_accepted_errors: How many rows can be broken and still be accepted by the process (Default is 50%)
        \nbroken_rows_column: Boolean that determines Whether to keep the extra string column with all the broken values as stings.
        """
        raccoon_df = Baby.Raccoon(df)
        rawfile_dfSchema = raccoon_df.GetSchema()

        # Key/Index the list of column Dictionaries
        formattedColumnsDictionaries = {
            c['columnName']: c for c in columns_dictionaries}

        # Iterate through the columns for a more data type check
        for colValues in rawfile_dfSchema:

            colName = colValues[0]

            # Check if it needs to be compared
            if colName not in formattedColumnsDictionaries:
                continue

            columnDictionary = formattedColumnsDictionaries[colName]

            expectedFormat = columnDictionary["columnType"]
            currentFormat = colValues[1]

            # Remove all the null string values (which are common on Pandas Dataframes)
            raccoon_df.NewColumnOnCondition(
                newColName=colName,
                condition=F.lower(F.col(colName)) == "null",
                valueTrue=None,
                valueFalse=F.col(colName),
            )

            if currentFormat != expectedFormat:

                # Create backup column and make it a string so it is always the same format, to validate values that were not accepted.
                brokenColumnForValidation = '{}_broken_rows'.format(colName)
                raccoon_df.NewColumnOnCondition(
                    newColName=brokenColumnForValidation,
                    condition=F.lower(F.col(colName)) == "null",
                    valueTrue=None,
                    valueFalse=F.col(colName),
                )
                raccoon_df.ConvertCol(brokenColumnForValidation, 'string')

                beforeSize = raccoon_df.GetColSize(colName)

                # Make the conversion
                raccoon_df.ConvertCol(**columnDictionary)

                # Validate Conversion
                afterSize = raccoon_df.GetColSize(colName)
                if beforeSize != afterSize:

                    changePercentage = abs((afterSize / beforeSize - 1) * 100)

                    print("Initially the column {} had {} rows, but after converting to {} it has {} or {}%".format(
                        colName, beforeSize, expectedFormat, afterSize, changePercentage))

                    # Change after testing to a regular number
                    if changePercentage > percentage_accepted_errors:
                        message = "Large error when changing column {} from datatype {} into {}. It lost {} rows".format(
                            colName,
                            currentFormat,
                            expectedFormat,
                            abs(beforeSize - afterSize),
                        )
                        self.logs.AddToLog_List("CheckColumnDType", message)
                        raise Exception(message)

                    if changePercentage <= percentage_accepted_errors:
                        message = "Small error when changing column {} from data type {} into {}. It lost {} rows and created a new column called {} with the broken files.".format(
                            colName,
                            currentFormat,
                            expectedFormat,
                            abs(beforeSize - afterSize),
                            brokenColumnForValidation
                        )
                        self.logs.AddToLog_List("CheckColumnDType", message)

                        if broken_rows_column is False:
                            raccoon_df.DropColumns([brokenColumnForValidation])

                else:
                    raccoon_df.DropColumns([brokenColumnForValidation])
                    self.logs.AddToLog_List(
                        "CheckColumnDType",
                        "In column {} the format was `{}` which is different to the wanted type `{}` but converted without any issues".format(
                            colName, currentFormat, expectedFormat
                        ),
                    )

        return raccoon_df.GetDF()

    def df_subset_static(self, df, list_logic=[]):
        """
        Filters dataframes based on static values in columns 
        """
        raccoon_df = Baby.Raccoon(df)
        for subsetLogic in list_logic:
            raccoon_df.ColumnFilter(**subsetLogic)
        return raccoon_df.GetDF()

    def delete_columns(self, df, columns_list=[], contains_string=None):
        """
        Deletes the columns in the given df, and returns the the new  df.
        """
        raccoon_df = Baby.Raccoon(df)

        if len(columns_list) != 0:
            raccoon_df.DropColumns(columns_list)

        if contains_string != None:
            raccoon_df.DropColumnsContains(contains_string)

        df = raccoon_df.GetDF()
        return df

    def keep_only_columns(self, df, columnNames, checkColumnsExist=True):
        """
        Keeps only the columns in the given list, removes all others. 
        Throws error if the column list from the dataframe is different to the size of the given columnName variable. 

        df: spark dataframe
        columnNames: List of columns to keep in the dataframe (list of strings)
        """
        columns = df.columns

        # Change variable name for clarification for now
        keepOnlyColumns = columnNames

        keepColumns = [c for c in columns if c in columnNames]
        deleteColumns = [c for c in columns if c not in columnNames]

        # If column from keepOnlyColumns is not found on DF, throw error
        if len(keepColumns) != len(keepOnlyColumns) and checkColumnsExist:
            missingColumns = list(set(columnNames) - set(keepColumns))
            raise spex.MissingColumns("keep_only_columns", missingColumns)

        return self.delete_columns(df, deleteColumns)

    def regex_remove_characters(self, df, column_name, custom_regex=""):
        """
        Replaces characters in string column with empty strings, defaults to remove all special characters
        :param df: input dataframe
        :param column_name: the column to regex replace
        :param custom_regex: an optional regex, defaults to alphanumerical characters
        :return: df
        """
        if custom_regex == "":
            return df.withColumn(column_name, F.regexp_replace(F.col(column_name), "/[^0-9A-Za-z]/", ""))
        else:
            return df.withColumn(column_name, F.regexp_replace(F.col(column_name), custom_regex, ""))

    def hashed_columns(self, df, columns_list):
        """
        Hashes a list of columns, currently only does it to SHA2 encryption
        """
        raccoon_df = Baby.Raccoon(df)
        for c in columns_list:
            raccoon_df.HashColumn_SHA2(c)
        return raccoon_df.GetDF()

    def rounded_columns(self, df, rounding_dictionaries):
        """
        Rounds the column list to a certain number of decimals
        """
        raccoon_df = Baby.Raccoon(df)
        for columnDict in rounding_dictionaries:
            raccoon_df.ColumnRounding(**columnDict)
        return raccoon_df.GetDF()

    def rename_columns(self, df, rename_dicionaries):
        """
        Renames the columns in the dataframe.
        """
        raccoon_df = Baby.Raccoon(df)
        for columnDict in rename_dicionaries:
            raccoon_df.ChangeColName(**columnDict)
        return raccoon_df.GetDF()

    def add_constant_column(self, df, column_name, value):
        """
        Adds the same value to all rows.
        """
        raccoon_df = Baby.Raccoon(df)
        raccoon_df.NewColumn_Default(column_name, value)
        return raccoon_df.GetDF()

    def simple_join_dataframes(
            self, df, right_df, left_columns, right_columns, join_type="left"
    ):
        """
        Simplified join, for when there are no same-named columns in both dataframes.
        """
        r_df = self.object_opener.validate_joined_df(right_df)
        raccoon_l_df = Baby.Raccoon(df)
        raccoon_l_df.JoinDF(r_df, left_columns, right_columns, join_type)
        return raccoon_l_df.GetDF()

    def agg_all_columns_except_one(
            self, df, column_name, agg_method, new_column_name=""
    ):
        """
        Aggregate a dataframe by all the columns, and aggregate only one, without having to know the schema.
        df - DataFrame (Spark Dataframe)
        column_name - Name of the column to be aggregated (string)
        agg_method - Aggregation method, as per the available ones on Apache Spark (string)  
        new_column_name - What to name this column, if no input it will use the same column name (string)
        """
        raccoon_df = Baby.Raccoon(df)
        raccoon_df.Agg_AllColumnsExceptOne(
            column_name, agg_method, new_column_name)
        return raccoon_df.GetDF()

    def join_dataframes(
            self, df, right_df, left_columns, right_columns, join_type="left"
    ):
        """
        Opens the right dataframe
        Join two dataframes, based on the columns in the given lists
        This is the safe method, in case the user doesn't know if there are similar named columns
        """

        # Make them into Raccoons
        l_df = Baby.Raccoon(df)
        r_df = Baby.Raccoon(self.object_opener.validate_joined_df(right_df))

        # Validate the input
        if not isinstance(left_columns, list):
            left_columns = [left_columns]

        if not isinstance(right_columns, list):
            right_columns = [right_columns]

        # Make sure there aren't repeating columns, so that they can be accounted for.
        l_columns = l_df.GetColumns()
        r_columns = r_df.GetColumns()
        matching_columns = [c for c in l_columns if c in r_columns]

        if len(matching_columns) > 0:
            new_right_columns = []
            for r_c in r_columns:
                current_r_c = r_c

                if r_c in matching_columns:
                    current_r_c = "right_{}".format(r_c)
                    r_df.ChangeColName(r_c, current_r_c)

                if r_c in right_columns:
                    new_right_columns.append(current_r_c)

            right_columns = new_right_columns

        l_df.JoinDF(r_df.GetDF(), left_columns, right_columns, join_type)

        return l_df.GetDF()

    def create_id_from_data(self, df, id_column_name="row_id", ignore_columns=[]):
        """
        Creates a new column fillexd with a hash from all the other columns - Slightly slow but can be improved.
        \ndf: DataFrame Must have clean column names] (Spark Dataframe)
        \nid_column_name: The name of the new column (string, lower_camel_case)
        \nignore_columns: List of columns to be ignored pre-hashing, in case the dataframe contains columns that are metadata and not a part of the data (list of strings[columns]) 
        """
        raccoon_df = Baby.Raccoon(df)
        columns = sorted(df.columns)

        if len(ignore_columns) > 0:
            columns = [c for c in columns if c not in ignore_columns]

        raccoon_df.NewColumn(id_column_name, F.sha2(
            F.concat_ws("||", *columns), 256))
        return raccoon_df.GetDF()

    def create_nested_column(self, df, col_name, nested_structure):
        """
        Dynamic create a nested column in spark made from arrays, structs, cols, and lits.

        Inputs:
        df : the spark data frame to add the new column to
        in_name : the name of the new column
        "col_name" : "new_col",
        "nested_structure" : {
            "type" : "struct" | "array" | "col" | "lit",
            # struct value: dict of dicts
            # array value: list of dicts
            # col/lit value: dict
            "value" : {"col_name : "{"type" : "", "value" : ""}} | [{"type" : "", "value" : ""}] | {"type" : "", "value" : "", "blank_if_null" : ""}
        }
        col dicts can also have the blank_if_null key, which puts the lit string blank if the value of the col is null, NOT required

        Output:
        if successful: dataframe with new nested column
        if fails: raises exception
        """
        try:
            name = col_name
            structure = nested_structure
            add_list = []

            if structure["type"] == "struct":
                for entry in structure["value"]:
                    add_list.append(fhir_functions.nested_column_build_type(
                        structure["value"][entry], entry))

                return df.withColumn(name, F.struct(*add_list))

            elif structure["type"] == "array":
                for entry in structure["value"]:
                    add_list.append(
                        fhir_functions.nested_column_build_type(entry, "none"))

                return df.withColumn(name, fhir_functions.nested_column_null_empty_array(F.array(*add_list), name))

            else:
                print("Error in function `create_nested_column`: unknown type {}".format(
                    structure["type"]))
                raise Exception(
                    "ERROR unknown type {}".format(structure["type"]))
        except Exception as e:
            message = "Error in function `create_nested_column`: while creating {col} column threw error {exc}".format(
                col=col_name, exc=str(e))
            raise Exception(message)

    def create_hashed_concat_col(self, df, col_list, col_name):
        """
        Creates a column that is a hash of one or more concatenated columns

        Inputs:
        "col_list" : ["col1", "col2", "col3"],
        "col_name" : "col_name"

        Outputs:
        returns updated dataframe
        """

        if len(col_list) == 0:
            raise spex.MissingInputs("create_hashed_concat_col")

        col_list = [F.col(c) for c in col_list]

        raccoon_df = Baby.Raccoon(df)

        raccoon_df.NewColumn("concat", F.concat(*col_list))
        raccoon_df.HashColumn_SHA2("concat", newName=col_name)

        return raccoon_df.GetDF()

    def prefix_column(self, df, col_name, prefix, input_col):
        """
        Adds prefic to the given column
        """
        return df.withColumn(col_name, F.concat(F.lit(prefix), F.col(input_col)))

    def force_column_exist(self, df, force_column_list, force_value=None, force_type="string"):
        """
        Adds col_name to df if it doesn't exists

        Inputs:
        df : the data frame to check
        force_column_list : list of column names to check/add to df
        force_value=None : the list to add if new column
        force_type="string" : the type to cast the new column

        Outputs:
        if col_name exists returns unmodified df
        if not return df with col_name of lit force_value cast as force_type
        """
        df_cols = df.schema.names

        for col_name in force_column_list:
            if col_name not in df_cols:
                df = df.withColumn(col_name, F.lit(
                    force_value).cast(force_type))

        return df

    def transpose_columns(self, df, key_column_name, value_column_name, columns_to_be_put_together):
        """
        Blantly stolen from https://stackoverflow.com/questions/54094365/pyspark-transpose-dataframe
        Transposes a list of columns into two column -> One with the value, the other with the column identifier.
        Make sure all the columns have the same data type before running this.

        df: Spark Dataframe
        key_column_name: name of the column that will contain the identifier of which column those values came from (STRING)
        value_column_name: Name of the column that will contain the values from those columnsa (STRING)
        columns_to_be_put_together: list of column names that will be made into two columns (LIST OF STRINGS)
        """

        # Get the transposing
        transposing_columns_list = [
            c for c in df.columns if c not in columns_to_be_put_together]

        # Filter dtypes and split into column names and type description since Spark SQL supports only homogeneous columns
        cols, dtypes = zip(
            *((c, t) for (c, t) in df.dtypes if c in columns_to_be_put_together))
        if len(set(dtypes)) != 1:
            raise Exception("All columns have to be of the same type")

        # Create and explode an array of (column_name, column_value) structs
        kvs = F.explode(F.array([F.struct(F.lit(c).alias(key_column_name), F.col(
            c).alias(value_column_name)) for c in cols])).alias("kvs")

        # All in one line cause it reminds me of Scala - just read from the left to the right to know whats up.
        return df.select(transposing_columns_list + [kvs]).select(
            transposing_columns_list + ["kvs.{}".format(key_column_name), "kvs.{}".format(value_column_name)])

    def create_conditional_column(self, df, col_name, expression_list):
        """
        col_name = the new column name to be created
        expression_list = ["case when gender = 'M' then 'Male'", "when gender = 'F' then 'Female'", "else 'Unknown' end"]
        """

        if len(expression_list) == 0:
            print(col_name, expression_list)
            raise spex.MissingInputs("create_conditional_column")

        expression = " " + " ".join(expression_list)

        validate_ending_expresion = expression.split(' ')[-1]
        if validate_ending_expresion != "end":
            expression = expression + " end"

        raccoon_df = Baby.Raccoon(df)
        raccoon_df.NewColumnOnCondition_SQL(col_name, expression)

        return raccoon_df.GetDF()

    def delta_in_source_not_target(self, df, target_dataframeConfigs):
        """
        Gets rows that are in df but aren't in the target df
        """
        try:
            # get target data source as a data frame
            target_df = self.object_opener.validate_joined_df(target_dataframeConfigs)
            # get delta of source and target
            new_df = df.unionAll(target_df).subtract(target_df)
        except spex.NotFound:
            new_df = df
        except Exception as e:
            raise e

        return new_df

    def order_by(self, df, col_name, ascending=True):
        """
        Sort a df by a col
        """
        raccoon_df = Baby.Raccoon(df)
        raccoon_df.OrderOnColumn(col_name, ascending)

        return raccoon_df.GetDF()

    def reference_col(self, df, ref_col_name, new_col_name, list_referencable_columns=None):
        """
        Creates a new column that's value is a column reference from the value of another column. CANNOT reference
        the reference column provided. Example:

        A | B | C
        1 | 2 | A
        3 | 4 | B
            |
            | for ref column C, new column ref
            v
        A | B | C | ref
        1 | 2 | A | 1
        3 | 4 | B | 4


        :param list_referencable_columns: list of which columns can referenced, must all be the same data type (string)
        :param df: the input dataframe
        :param ref_col_name: the name of the column to use as a reference
        :param new_col_name: new column to store result of the reference
        :return:
        """
        if list_referencable_columns is None:
            data_cols = [x for x in df.columns if x not in {ref_col_name}]
        else:
            data_cols = list_referencable_columns

        name_to_value = F.create_map(*chain.from_iterable(
            (F.lit(c), F.col(c)) for c in data_cols
        ))

        return df.withColumn(new_col_name, name_to_value[F.col(ref_col_name)])

    def debug_display_df(self, df, show_data=False, row_count=20):
        """
        Display the current dataframe, useful for debugging
        :param df: current dataframe
        :param show_data: whether to show rows of data, defaults to just display schema
        :return:
        """
        df.printSchema()
        if show_data:
            df.show(row_count, truncate=False)

        return df

    def keep_only_columns_TESTING(self, df, columnNames, checkMissingColumns=True):
        """
        Keeps only the columns in the given list, removes all others.
        Throws error if the column list from the dataframe is different to the size of the given columnName variable.

        df: spark dataframe
        columnNames: List of columns to keep in the dataframe (list of strings)
        """
        columns = df.columns

        # Change variable name for clarification for now
        keepOnlyColumns = columnNames

        keepColumns = [c for c in columns if c in columnNames]
        deleteColumns = [c for c in columns if c not in columnNames]

        # If column from keepOnlyColumns is not found on DF, throw error
        if len(keepColumns) != len(keepOnlyColumns) and checkMissingColumns:
            missingColumns = list(set(columnNames) - set(keepColumns))
            print("[ERROR] Missing columns are {}".format(missingColumns))
            df.printSchema()
            raise spex.MissingColumns("keep_only_columns", missingColumns)

        return self.delete_columns(df, deleteColumns)