from SparkJob.SparkFunctions import _ObjToSpark as o2s
from SparkJob.SparkFunctions import _Dataframe_SparkHelper as dff
from SparkJob.SparkFunctions import _Context_SparkHelper as scf
from SparkJob.SparkFunctions import SparkAssistant as s

# pointOfIntersection is the parent level which contains all the unified classes come form, this is the lowest parent in common
pointOfIntersection = s.Assistant

# All the classes that the sparkFunctionGruoping will inherit
SparkUnifierImports = [
    scf.ContextFunctions,
    dff.DataFrameFunctions,
    o2s.ObjConversion_Functions,
]


def ValidateClasses(classesToCheck, pointOfIntersection):
    """
    Ensure that all functions in the classesToCheck are unique, so that there is no ambiguity when calling them in a multi-inheritance class.
    pointOfIntersection -> Class that is the parent to all classesToCheck.
    classesToCheck - list of class locations, that are to be checked for unique functions
    """
    poi_functions = [f for f in dir(pointOfIntersection) if f[:2] != "__"]

    functionsList = []
    repeatingList = []
    importsDictionary = {}
    repeatingFunctions = {}

    for member in classesToCheck:

        memberFunctions = [
            f for f in dir(member) if f[:2] != "__" and f not in poi_functions
        ]

        importsDictionary[member.__name__] = memberFunctions

        repeatingList.extend([mf for mf in memberFunctions if mf in functionsList])
        functionsList.extend(memberFunctions)

    if len(repeatingList) > 0:

        for f in repeatingList:
            repeatingFunctions[f] = [
                k for k in importsDictionary.keys() if f in importsDictionary[k]
            ]

        errorMessage = "Cannot import the SparkFunction classes since there are repeating function(s) in two or more classes, they are: \n {} ".format(
            repeatingFunctions
        )
        raise Exception(errorMessage)

    else:
        return


ValidateClasses(SparkUnifierImports, pointOfIntersection)


class SparkUnifiedFunctions(*SparkUnifierImports):
    """
    This class contains all the functions by all the children of the SparkAssistant Class.
    """
    pass
