import re

import pyspark
import pyspark.sql.functions as F
from pyspark.sql import DataFrame
import pyspark.sql.types as T
from pyspark.sql.utils import AnalysisException

from SparkJob.Helpers import SpecialErrors as spex
from SparkJob.Helpers import LogsClass as L
from SparkJob.Helpers import UniversalFunctions as uf
from SparkJob.SparkFunctions import Raccoon as Baby
from SparkJob.SparkFunctions import SparkAssistant as s
from SparkJob.SparkFunctions import _Context_SparkHelper as scf
from SparkJob.SparkFunctions import _Dataframe_SparkHelper as dff


class ObjConversion_Functions(s.Assistant):
    """
    Class contains functions to help validate and move objects into Spark (Either context or dataframe)
    CamelCase for internal functions only, lower_snake_case for document triggered functions
    """

    def CheckObjPath(self, checkingObject):
        """
        Changes the filename to ensure it's in the right format to open accounting for the environment
        """
        pathIdentifier = []

        if self.environment == "GLUE":
            checkingObject = checkingObject.replace("s3://", "s3a://")
            checkingObject = checkingObject.replace("s3n://", "s3a://")
            pathIdentifier = ["s3a://"]

        if self.environment == "EMR":
            checkingObject = checkingObject.replace("s3n://", "s3://")
            checkingObject = checkingObject.replace("s3a://", "s3://")
            pathIdentifier = ["s3://"]

        if self.environment == "LOCAL":
            checkingObject = checkingObject.replace("s3://", "s3n://")
            checkingObject = checkingObject.replace("s3a://", "s3n://")

            pathIdentifier = ["C:/", "/home/", "C:\\", "s3n://"]

        acceptedFiles = [
            acceptedPath
            for acceptedPath in pathIdentifier
            if acceptedPath in checkingObject
        ]

        if len(acceptedFiles) != 1:
            raise Exception(
                "The following object {} was asked to be made into DataFrames without the correct path formatting, please ensure that it will only be created if the path matches the requirements of the environment `{}`".format(
                    checkingObject, self.environment
                )
            )
        return checkingObject

    def ValidateDataFrameConfig(self, dataframeConfigs):
        """
        Function ensures that the dataframeConfigs is matching the needed keys and values.
        dataframeConfigs - Dictionary with the details to open the file (dictionary)
        """

        # General Standards
        if "spark_name" not in dataframeConfigs:
            raise Exception(
                "Could not open file as it was missing the spark_name key")

        # Deliminated Standards ---vv
        if dataframeConfigs["obj_type"] == "delimited":

            # Create key for ease of process
            if "delimiter_details" not in dataframeConfigs:
                raise Exception(
                    '`delimiter_details` are necessary in the object metadata if the object is the delimited type.')

            # Throw error if lines are needed to be skipped cause I was lazy and haven't found a good way of doing this yet
            if "lines_skipped" in dataframeConfigs["delimiter_details"]:
                if dataframeConfigs["delimiter_details"]["lines_skipped"] > 0:
                    raise Exception(
                        "_ObjToSpark is not yet set up to account for skipping lines on csv, please do that before sending to job."
                    )

        dataframeConfigs["spark_name"] = self.CheckObjPath(
            dataframeConfigs["spark_name"]
        )

        dataframeConfigs["validated"] = True

        return dataframeConfigs

    def CreateDataFrame_BasedOnType_Process(self, validConfigs):
        """
        Determines how to open the dataframe based on the details in the configuration dictionary.
        """

        if "validated" not in validConfigs:
            raise Exception(
                "Configuration was not validated, please do so before sending to this function"
            )

        newDF = None

        # Obj Type, delimiter and cleaning Procedure should all be mentioned in the objMeta, and already pre-defined
        if validConfigs["obj_type"] == "delimited":
            newDF = self.Open_DelimitedObj(
                validConfigs["spark_name"], **validConfigs["delimiter_details"]
            )

        if validConfigs["obj_type"] == "parquet":
            newDF = self.open_parquet_obj(validConfigs["spark_name"])

        if "context_name" in validConfigs:
            self.add_obj_to_context(objDetails=validConfigs, temp_df=newDF)

        return newDF

    def CreateDataFrame_BasedOnType(self, dataframeConfigs):
        """
        Opens the dataframe based on the dataframeConfigs. An example is:

            "dataframe_details": {
            "spark_name": "/home/user/avernus_files/phh99999_2020.txt",
            "obj_type": "delimited",
            "comparison_obj":False,
            "delimiter_details": {
                "delimiter": "|",
                "clean_headers": True,
                "encoding":"utf-8"
            }

        First validate then open in a Try/Catch to minimise the error message (py4java is awful at errors)
        """

        validConfigs = self.ValidateDataFrameConfig(dataframeConfigs)

        try:
            return self.CreateDataFrame_BasedOnType_Process(validConfigs)

        except AnalysisException as AE:

            # Checks if it was not found or another error
            errorMessage = str(AE.args).lower()
            if "path does not exist" in errorMessage or "unable to infer schema for parquet" in errorMessage:
                print(f'Failed to open the dataframe {validConfigs}')
                raise spex.NotFound(validConfigs["spark_name"])
            else:
                # Other random error, raise as is to be safe
                raise Exception(AE)

        except Exception as e:
            raise e

    def Open_DelimitedObj(self, filename, delimiter="|", encoding="utf-8", clean_headers=True, **kwargs) -> DataFrame:
        """
        Opens Deliminated file and creates PySpark DataFrame.
        PySpark determines the schema automatically.

        Input:
        filename -> the file name as a string
        separator -> the character that separates cells in the *SV

        Return:
        Spark Dataframe
        """
        read_DataFrame = (
            self.spark.read.option("header", "true")
            .option("inferSchema", "true")
            .option("multiLine", "true")
            .option("header", True)
            .option("delimiter", delimiter)
            .option("quote", '"')
            .option("encoding", encoding)
            .option("columnNameOfCorruptRecord", "Broken_Row")
            .option("escape", '"')
            .csv(filename)
        )

        if clean_headers is True:
            temp_df = Baby.Raccoon(read_DataFrame)
            temp_df.cleanDfColumns()
            read_DataFrame = temp_df.GetDF()

        return read_DataFrame

    def get_table_from_context(self, table_name):
        """
        Returns a table as Spark Dataframe from the context. Assumed the value to be in the context, throws error if it is not.
        table_name: context name of the table or view (string)
        """
        return self.sqlContext.sql("SELECT * FROM {}".format(table_name))

    def open_comparison_obj(self, objDetails):
        """
        -- DO NOT USE OUTSIDE OF TESTING --
        Currently no functions are depedent on it.
        """

        try:
            # Remove the ending prefix
            # Should be changed by the sorter
            if objDetails["table_name"][-1] == "/":
                objDetails["table_name"] = objDetails["table_name"][:-1]

            objDetails["table_name"] = uf.CleanString(objDetails["table_name"])
            return self.get_table_from_context(objDetails["table_name"])
        except Exception as e:
            print(
                "Could not find table `{}`, threw error `{}`".format(
                    objDetails["table_name"], e
                )
            )

        try:
            comparison_df = self.CreateDataFrame_BasedOnType(objDetails)
        except spex.NotFound:
            field = [
                T.StructField("row_id", T.StringType(), True),
                T.StructField("source_key", T.StringType(), True),
            ]
            schema = T.StructType(field)
            comparison_df = self.spark.createDataFrame(data=[], schema=schema)

        self.sqlContext.registerDataFrameAsTable(
            comparison_df, objDetails["table_name"]
        )

        return comparison_df

    def validate_joined_df(self, d):
        """
        Checks if it's a dataframe or whether it needs to be made into one based on the dictionary.
        """

        if isinstance(d, dict):
            return self.CreateDataFrame_BasedOnType(d)

        if isinstance(d, DataFrame):
            return d

        # Finally, throw error if the function got an invalid input
        else:
            raise Exception(
                "Invalid input in the validate_joined_df, received the input {} which is of type {} that is not accounted for".format(
                    d, type(d)
                )
            )

    def open_parquet_obj(self, objPath):
        """
        Opens Parquet object as Spark DataFrame
        objPath: Object's path (string)
        """
        parquet_DataFrame = self.spark.read.option(
            "mergeSchema", "true").parquet(objPath)
        return parquet_DataFrame

    def add_obj_to_context(self, objDetails=None, temp_df=None, contextName=None):
        """
        Using an obj meta data document, adds it to the Spark Context for multi DF SQL queries.
        """

        # Determine context name
        if contextName is None and "context_name" not in objDetails:
            contextName = "df"
        if contextName is None and "context_name" in objDetails:
            contextName = objDetails["context_name"]

        try:
            self.get_table_from_context(contextName)
            return
        except Exception:
            print(
                'Could not find the table in the context, will try to add the dataframe manually.')

        if temp_df is None and objDetails is None:
            raise Exception(
                '`add_obj_to_context` Requires either the temp_df variable or the objDetails to be given')

        if temp_df is None:
            temp_df = self.CreateDataFrame_BasedOnType(objDetails)

        # Raccoon-ify the dataframe
        quick_raccoon = Baby.Raccoon(temp_df)
        quick_raccoon.AddDataframeToContext(contextName)

    def save_df(self, df, save_path, save_mode, save_format,  repartitions=0, partition_by=[]):
        """
        Saves the dataframe into the given Path.

        df: Spark Dataframe (Spark DataFrame)
        save_path: Spark friendly path to save the file (string)
        save_format: Spark Option for type of saving  (string)
        repartitions: Number of files to be saved as (int)
        partition_by: If you would like to partition by a column or columns, have the column name(s) in a list. (list of strings)
        """
        racoon_df = Baby.Raccoon(df)

        valid_save_path = self.CheckObjPath(save_path)

        # For Testing - Might be useful overall.
        saving_dictionary = {
            "path": valid_save_path,
            "format": save_format,
        }
        self.logs.AddToLog_List("save_outcomes", saving_dictionary)
        ###############################################################

        racoon_df.SaveDF(save_path, numOfReparitions=repartitions,
                         saveFormat=save_format, saveMode=save_mode, partitionBy=partition_by)

        return racoon_df.GetDF()
