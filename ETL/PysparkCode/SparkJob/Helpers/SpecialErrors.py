"""
============================================================
======        ==       ===       =====    ====       =======
======  ========  ====  ==  ====  ===  ==  ===  ====  ======
======  ========  ====  ==  ====  ==  ====  ==  ====  ======
======  ========  ===   ==  ===   ==  ====  ==  ===   ======
======      ====      ====      ====  ====  ==      ========
======  ========  ====  ==  ====  ==  ====  ==  ====  ======
======  ========  ====  ==  ====  ==  ====  ==  ====  ======
======  ========  ====  ==  ====  ===  ==  ===  ====  ======
======        ==  ====  ==  ====  ====    ====  ====  ======
============================================================
"""


class AvernusErrors(Exception):
    """
    Master class for any issues found on the dataframe.
    """

    def __init__(self):
        """
        Initiator, requires the causation of error and what was specified.
        cause --> Value which caused any issues (Any type as long as it is printable)
        expected --> Value which was expected instead (Any type as long as it is printable)
        """
        self.message = "Default, please change with your own damn class."

    def __str__(self):
        """
        Returns the message which is expected to be created.
        """
        return self.message


class MissingDictionaryKey(AvernusErrors):
    """
    Error caused by a dictionary failing validation by a missing key.
    """

    def __init__(self, dictionaryName, missingKeys):
        super().__init__()

        if type(missingKeys) is str:
            missingKeys = [missingKeys]

        self.message = "The dictionary `{}` did not have the required key(s): {} ".format(
            dictionaryName, ",".join(missingKeys)
        )


class NotFoundDocument(AvernusErrors):
    def __init__(self, dynamoTableName, documentKey):
        super().__init__()
        self.message = "The table {} did not have the searched document with the index key {}".format(
            dynamoTableName, documentKey
        )


class NotFound(AvernusErrors):
    def __init__(self, path):
        """
        Error caused by columns not matching either naming or schema conventions.

        Path: The path used to get the file. (string)
        """
        super().__init__()
        self.message = "The path received `{}` could not be found.".format(
            path)


class MissingColumns(AvernusErrors):
    def __init__(self, functionName, missingColumns):
        super().__init__()
        self.message = "Function {} failed due to not being able to find the following columns in the dataframe: {}".format(
            functionName, ",".join(missingColumns)
        )


class TrackedFunctionError(AvernusErrors):
    def __init__(self, functionName, errorName):
        super().__init__()
        self.message = "Needed DAG/Separate Function `{}` failed due to `{}`".format(
            functionName, errorName
        )


class MissingInputs(AvernusErrors):
    def __init__(self, functionName):
        super().__init__()
        self.message = "There are missing necessary inputs for the function `{}`, please validate the function document. ".format(
            functionName)


class ProcessError(AvernusErrors):
    def __init__(self, requirementName, functionName):
        super().__init__()
        self.message = "The following process requirement `{}` was not accounted for, process will stop on function `{}`".format(
            requirementName, functionName
        )


class WrongType(AvernusErrors):
    def __init__(self, variableName, expectedTypeName, currentTypeName):
        super().__init__()
        self.message = "The variable `{}` was not the expected type `{}` and was found to be `{}`".format(
            variableName, expectedTypeName, currentTypeName
        )
