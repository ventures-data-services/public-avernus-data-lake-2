import datetime


class LogHelper:
    """
    This class was made to help create and upkeep the logs.
    No needed inputs, make addTimeStamp False if you do not want to addTimeStamp.
    """

    def __init__(self, addTimeStamp=True):
        self.logs = {}

        if addTimeStamp:
            self.addTimeStamp()

    def addTimeStamp(self):
        """
        Adds current timestamp to log.
        """
        # Calculate UTC Now Timestamp
        d = datetime.datetime.utcnow()
        epoch = datetime.datetime(1970, 1, 1)
        time_epoch = (d - epoch).total_seconds()
        self.logs["timestamp"] = round(time_epoch)

    def AddToLog_Dict(self, logKey, item):
        """
        Adds item to dictionary in the logs, after checking whether dict needs to be initiated.
        """
        if logKey not in self.logs:
            self.logs[logKey] = {}

        self.logs[logKey].update(item)

    def AddToLog_List(self, key, item):
        """
        Adds item to list in the logs, after checking whether list needs to be initiated.
        """
        if key not in self.logs:
            self.logs[key] = []
        self.logs[key].append(item)

    def AddValue(self, k, v):
        """
        Adds value to log straight up -- use this if you are confident it's going to be a list or whatever
        """
        self.logs[k] = v

    def PrepareForDynamo(self, dynamoClass, indexValue, sortValue=None):
        """
        Expects the dynamoClass to have their keys setup.
        indexValue -> Value that will be put in the index key
        sortValue -> Value that will be put in the sort key
        """

        if dynamoClass.indexKey is None:
            raise Exception(
                "dynamoClass has not been setup correctly and is missing their Keys."
            )

        self.logs[dynamoClass.indexKey] = indexValue

        if dynamoClass.sortKey is not None:
            if sortValue is None:
                raise Exception(
                    "This table has a sort key, please add when preparing for dynamo!"
                )
            else:
                self.logs[dynamoClass.sortKey] = sortValue

        self.logTableClass = dynamoClass

    def SaveLog_DefinedTable(self):
        """
        If logTableClass has already been defined, use this function to use it to save.
        """
        self.SaveLog(self.logTableClass)

    def SaveLog(self, logTableClass):
        """
        Saves the self.log variable into the DynamoDB
        """
        logTableClass.AddDictionary(self.logs)

    def PrintLog(self):
        """
        Prints the logs
        """
        print(self.logs)

    def ReturnLog(self):
        """
        Returns the logs
        """
        return self.logs
