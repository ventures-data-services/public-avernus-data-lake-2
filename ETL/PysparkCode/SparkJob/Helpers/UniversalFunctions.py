"""
Pedro Mroninski - 2020/04/11 - Pinnacle Ventures

Universal Functions that apply to multiple files.

"""

from string import Formatter
import re


def FindFunctionInClass(methodName, classObj, required=True):
    """
    Returns the inmemory location of an classes' function based on it's name
    class_obj is to be the class itself, must be already initiated (class object)
    methodName is the name of the function (string) 
    """

    method = None

    try:
        method = getattr(classObj, methodName)
    except AttributeError:
        if required:
            raise NotImplementedError(
                "Class `{}` does not implement `{}`".format(
                    type(classObj).__name__, methodName
                )
            )
    return method


def CleanString(string_value, replacement="_"):
    return re.sub("[^A-Za-z0-9]+", replacement, string_value).lower()


def FindKeysInFormattedString(chekingString):
    """
    Find the keys in a string if it is to be formatted
    """
    return [i[1] for i in Formatter().parse(chekingString) if i[1] is not None]


def InstanceInforcer(var, instanceType):
    """
    Ensures that the variable is the correct type.
    """
    if not isinstance(var, instanceType):
        realtype = type(var)
        raise Exception(
            "The variable is not the correct type! Excepecting {} but got {}".format(
                instanceType, realtype
            )
        )
    return var

def CheckVariable(var, varName, expectedType):
    """
    Compares the variable with the expected type, throws error if they do not match.
    """
    if type(var) != expectedType:
        raise Exception(
            "Expected Variable of type {} for variable {}".format(expectedType, varName)
        )

def EnsureList(value):
    """
    Ensures that the given variable is a list, if it's not, it will be converted into one (as long as it's convertable to a list)
    """
    if not(isinstance(value, list)):
        value = [value]
    return value


def GetObjMetadata(metadataTable, objKey, necessary=True):
    """
    Gets the object's document from the metadata dynamo table, or from the settings if the object is a function call.
    objKey - The key of the document (string)
    necessary - Decides wether to stop the process based on the existance of this document [default is True] (boolean)
    """
    metaDict = metadataTable.GetDocument(objKey, "main")

    if metaDict is None:
        concernedMessage = "Could not find the document for the requested index `{}` with the request table `{}`.".format(
            objKey, metadataTable.tableName
        )
        if necessary is True:
            raise Exception(concernedMessage)

    return metaDict