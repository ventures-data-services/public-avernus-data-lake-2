# Extra files
import traceback

from SparkJob import ProcessesClass as pc
from SparkJob.Helpers import AWS_Helper as aws
from SparkJob.Helpers import LogsClass as lc
from SparkJob.Helpers import UniversalFunctions as uf


class SparkMaestro:
    """
    This is where the magic happens.
    Class made to gather all the required information for the jobs 
        and apply the appropiate process style.
    """

    def __init__(
        self,
        spark,
        environmentalVariablesTable,
        processesMapping,
        processEnvironment,
        processKey,
        requiredDocuments,
        inputType,
        primaryInput,
        secondaryInput,
        **args
    ):
        self.logs = lc.LogHelper()

        self.processesMapping = processesMapping
        self.processEnvironment = processEnvironment

        self.spark = spark

        self.inputType = inputType
        self.primaryInput = primaryInput
        self.secondaryInput = secondaryInput

        self.enviromentalTable = aws.DynamoDB_Helper(
            environmentalVariablesTable)

        self.IterateThroughRequiredDocuments(requiredDocuments)

        self.processKey = processKey

        return

    def IterateThroughRequiredDocuments(self, required_documents):
        """
        Iterates through the required documents and triggers they functions
        required_document - object where the index is the document's key and the value is the function's memory location 
        Example:
            [    {"document_index": "dynamo_tables_info", "document_function": j.Glue_Setup.SetUp_Tables}] (List of Dictionary)
        """
        for required_doc in required_documents:
            required_doc["document_function"](
                self, required_doc["document_index"])

    def UseEnvironmentalSettings(self, datalakeConfigurationIndexKey):
        """
        Get the automatically created document to establish the environmental settings.
        """
        dalakeConfigs = self.enviromentalTable.ValidatedGetDocument(
            datalakeConfigurationIndexKey)

        self.SetUp_Tables(**dalakeConfigs)
        self.SetUp_Queues(**dalakeConfigs)

    def SetUp_Tables(self, log_ddb_table, obj_meta_ddb_table, **args):
        """
        Initiates the logs and metadata tables by connecting to the DynamoDB.
        tableinfo_key - The document key for the document with the tables information for this environment.
        """
        self.logsTable = aws.DynamoDB_Helper(log_ddb_table)
        self.metadataTable = aws.DynamoDB_Helper(obj_meta_ddb_table)

    def SetUp_Queues(self, log_object_sqs_url, **args):
        """
        Set up the SQS queue for tracking the processed objects and their outcome
        """
        self.processQueue = aws.SQS_Helper(log_object_sqs_url)

    def ProcessMessageAndAddToQueue(self, valueIndex, processResult):
        """
        Process the given massages and send them to the process queue
        """
        if isinstance(valueIndex, list):
            for v in valueIndex:
                self.ProcessMessageAndAddToQueue(v, processResult)

        if isinstance(valueIndex, str):
            trackingMessage = "{} {} {}".format(
                valueIndex, self.processKey, processResult)
            self.processQueue.AddToQueue(trackingMessage)

    def GetProcessObject(self):
        """
        Get the inputted process document from the log Table
        """
        self.processDictionary = self.logsTable.GetDocument(
            self.primaryInput, self.secondaryInput)

    def PerformProcess(self, jobClass, variables):
        """
        Initiates the class on the file - Will always run the function AvernusProcess as it will always expect that to exist.
        jobClass - Memory location of the wantedClass [E.g pc.MultipleObjs_Process]
        variables - All the variables required to initiate the JobClass [The keys must be the same name as the variables in the class] (dictionary)
        """
        print
        processStart = jobClass(**variables)
        processStart.AvernusProcess()
        return

    def MultiObjectIteration(self, jobClass, jobInput, variableDictionary={}, dynamoTableType='metadataTable'):
        """
        Sends all objects from a list into the class - This is for when all the objects are expected to be set up at once.
        jobClass - Memory location of the wantedClass [E.g pc.MultipleObjs_Process]
        jobInput - List of file/object keys (list of strings)
        variableDictinary - Contains other default variables to be passed into the class (dictionary)
        """
        metaList = []

        if not(isinstance(jobInput, list)) and not(isinstance(jobInput[0], str)):
            errorMessage = """
            Failed on MultiObjectIteration for the class `{}` due to the jobInput not being a list of strings, 
            check the given inputs from the process request {} and mapping of functions on main.py
            """
            raise TypeError(errorMessage.format(
                jobClass, self.processDictionary))

        for f in jobInput:
            metaList.append(uf.GetObjMetadata(f, necessary=True))
        variableDictionary["objsMeta"] = metaList

        try:
            self.PerformProcess(jobClass, variableDictionary)
            self.ProcessMessageAndAddToQueue(jobInput, "success")
        except Exception as e:
            self.ProcessMessageAndAddToQueue(jobInput, "fail")

            # For Testing
            print("Group {} failed with `{}`\n".format(jobInput, e))
            traceback.print_exc()

    def SingleObjectIteration_Process(self, jobClass, i, variableDictionary={}, **args):
        try:

            variableDictionary["inputValue"] = i

            self.PerformProcess(jobClass, variableDictionary)
            self.ProcessMessageAndAddToQueue(i, "success")
        except Exception as e:
            self.ProcessMessageAndAddToQueue(i, "fail")
            print("Object {} failed due to {}".format(i, e))
            traceback.print_exc()

    def SingleObjectIteration(self, jobClass, jobInput, variableDictionary={}, **args):
        """
        Sends a single object from a list into the class
        jobClass - Memory location of the wantedClass [E.g pc.MultipleObjs_Process]
        jobInput - List of file/object keys (list of strings)
        variableDictinary - Contains other default variables to be passed into the class (dictionary)
        """

        if isinstance(jobInput, list):
            for i in jobInput:
                self.SingleObjectIteration_Process(
                    jobClass, i, variableDictionary, **args)

        if isinstance(jobInput, aws.SQS_Helper):
            i = jobInput.GetMessageAndDelete()
            while i is not None:
                self.SingleObjectIteration_Process(
                    jobClass, i, variableDictionary, **args)
                i = jobInput.GetMessageAndDelete()

    def SetupVariableDictionary(self):
        """
        All jobs require this initial set up. -- if it changes, adjust here.
        """
        self.variableDictionary = {
            "spark": self.spark,
            "metadataTable": self.metadataTable,
            "logTable": self.logsTable,
            "EnvironmentTable": self.enviromentalTable,
            "processKey": self.processKey,
            "processEnvironment": self.processEnvironment,
        }

    def ArgumentProcess(self, jobType, jobInput):
        """
            Puts all the the inputs in a logical set of dictionaries,
            send to their specific types of iteration based on jobType, 
            and then save the logs.
        """

        self.ProcessFunctions = self.processesMapping[jobType]

        self.SetupVariableDictionary()

        # Job set up and iteration variables
        processInput = {
            'jobInput': jobInput,
            'jobClass': self.ProcessFunctions["etl_classes"],
            'variableDictionary': self.variableDictionary
        }

        if "object_table" in self.ProcessFunctions:
            processInput['dynamoTableType'] = self.ProcessFunctions["object_table"]

        # Get the specific function + process as defined in processDictionary
        IterationFunction = self.ProcessFunctions["iteration_type"]
        IterationFunction(self, **processInput)

    def ManualProcess(self):
        jobType = self.primaryInput
        jobInput = self.secondaryInput.split(',')
        return self.ArgumentProcess(jobType, jobInput)

    def DocumentProcess(self):
        self.GetProcessObject()
        return self.ArgumentProcess(
            self.processDictionary["etl_jobtype"], self.processDictionary["job_input"])

    def QueueProcess(self):
        """
        Process based on an SQS Queue
        """
        sns = aws.SQS_Helper(self.primaryInput)
        self.ArgumentProcess('single_object_process', sns)
        return

    def Main(self):
        """
        The generalised avernus process, to iterate and apply functions to the files. 
        Gets the process document, and uses it to set up and start the required job with the given files.
        Then saves logs
        """

        if self.inputType == 'MANUAL':
            self.ManualProcess()

        if self.inputType == 'DOCUMENT':
            self.DocumentProcess()

        if self.inputType == 'QUEUE':
            self.QueueProcess()
