# General Assumptions

The following information makes the following assumptions:

- This code will be used on AWS EMR (Elastic MapReduce) version 6.1.0 or greater with Spark, Hadoop and running on the default python3 environment.

- The necessary DynamoDB tables have been created (Settings, Logs, Object Metadata). It also assumed you are somewhat familiar with their usage and have already inserted some data into them.

- It is also beneficial if you have an AWS SQS (Simple Queue Service) and is comfortable with its usage.

- Also beneficial to Know why Direct Acyclical Graphs are fun (Called DAGs from now onwards)

# Introduction

The ETL for the Avernus datalake does have more dependencies than generally would be seen in a data lake. Specifically, the usage of DynamoDB as opposed to Hadoop variables for shared information amongst the different instances and initialisations. This also enables for minimal user input to the EMR Step. The reasoning for this deliberate design choice comes in twofold:

1. Minimising repetition and ensuring consistency of the environment when running multiple clusters with different processes. This helps the lake scale up as necessary without much change to the administrator.

2. Document Databases are lazy data structures but can be incredibly fast and thorough internal code insurance, have created a stricter schema that can enable clean and human-readable data governance.

DynamoDB was chosen simply to minimise worker overhead, as other document databases would use man-hours to maintain/update and the cost/benefit did not match our requirements.

The three tables are used in the following way:

- Settings: Contains the environmental variables, such as the names of the other tables. It also contains all the function inputs and DAG structures.
- Object Metadata: Contains the required inputs to initialise the data on Spark, such as data type, delimiters and location.
- Logs: This is where the process input can come from, and where all the process is outputted to.

### High-Level Explanation of Process

The EMR Step is submitted and using the input it will open all the required objects by getting the object metadata documents, and will iterate through the DAG. For each function in the DAG, it will get the inputs from the settings table and perform the required process for each file. Once it has completed all the process requested, it will submit the logs to the logs table, and print them into the stdout.

# Code Explanation

The code is in a loose OOP design and should be readable by itself.

## Available Main Functions

The following functions are currently available to be called manually or should be referenced in the settings document:

- single_object_process: Processes each given object separately, this is to perform processes to a single object without moving it into the SparkContext and without interfering with fail-safes which make sure it always moved into the next object.
- multi_object_process: Processes all the given objects at once, by moving them into the context initially and then processes all the functions. Processes all the functions referenced in the object(s) metadata, and always reverts to their original structure after each DAG.
- function_specific_process: Iterates through the functions, expecting the functions documents to contain the index key for the object metadata.

## New Spark Functions

If you are looking to add more callable functions on the settings table; the easiest approach is to add the functions to one of the following files:

- SparkJob/SparkFunctions/\_Context_SparkHelper.py: Any functions that apply to the spark context. For example, if you were to write a validator for unifying two tables that are in the Spark Context.

- SparkJob/SparkFunctions/\_Dataframe_SparkHelper.py: Any functions that apply to a dataframe. This requires the dataframe to be passed. For example, if you were to write a function that aggregates a dataframe in a specific way.

- SparkJob/SparkFunctions/\_ObjToSpark.py: Any functions that apply to moving static objects into the Spark program or vice-versa. For example, if you were to write a function that opens the dataframe and uses a specific column to determine it's context name.

- SparkJob/SparkFunctions/Raccoon.py: These are for repeating functions applying to the dataframe; These are functions called by the other files to minimise code repetition.

### FHIR

One of the best examples of purpose-built processes was FHIR. In order to do it, there were functions created to map the current data into an FHIR structure, which is highly complex and contains many layers of depth. There were written only ~6 functions and ~40 settings documents to do the mapping. This utilised the shared settings, minimal user input and minimal code repetition while ensuring readable DAGs.

# IMPORTANT

#######################

## All object metadata should have the sort key of "main" in the document that contains the information for moving it into Spark.

## The deploy-mode needs to be client as it is depedent on AWS' SDK (BOTO3)

#######################

# Usage example

The EMR Step is submitted with the path to the code, and the following arguments:

#### PROCESS_ENVIRONMENT

Should be EMR or Local, Local is only used for testing.

#### ENVIRONMENTAL_VARIABLES_TABLE

The Settings table which you would like to use, this is in case you have multiple settings tables with different environments and functions.

#### PROCESS_INDEX_KEY

The index key for the log document that will be uploaded into the Log DynamoDB

#### INPUT_TYPE

The options are as follows:

- Document, which is when you have a document in the Log table with the inputs you would like to process
- Manual, which is when you pass the process inputs in the step-submission, but ensure they are comma-separated.
- Queue, which is when you submit a queue URL or name, that will contain the objects necessary

#### PRIMARY_INPUT

For each type, this section will be different:

- Document: This contains the index of the document you would like to use.
- Queue: The name or URL of the SQS queue.
- Manual: The name of the function which you would like to use, they are below:
- - single_object_process
- - multi_object_process
- - function_specific_process

#### SECONDARY_INPUT

The second input should only be used for "manual" or "document" inputs:

- Manual: The actual input to the function given.
- Document: The sorting key for the document which contains the inputs.

# Example

```sh
    spark-submit \
    --deploy-mode client \
    --py-files s3://code_bucket/ETL/SparkJob.zip \ # Extra python files
    s3://code_bucket/ETL/main.py \ # Path to the main file
    EMR \ # Environment
    dev_settings_ddb \ # Settings DynamoDB table
    queue_request_x \ # The index key for the log being processed
    QUEUE \ # Input type
    https://sqs.ap-southeast-2.amazonaws.com/60000000/sqs-queue-name # Input
```
