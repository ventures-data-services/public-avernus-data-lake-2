#!/bin/bash -xe

# Non-standard and non-Amazon Machine Image Python modules:

sudo python3 -m pip install --upgrade pip==19.1.1
sudo python3 -m pip install python-dateutil==2.8.0 boto3==1.9.183
