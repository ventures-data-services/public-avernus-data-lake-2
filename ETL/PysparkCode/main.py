# /usr/bin/python3

# import argparse
import os
import sys
import time

import boto3
from pyspark import SparkConf, SparkContext, SQLContext
# from pyspark.sql import SparkSession

from SparkJob import JobOrchestration as j
from SparkJob import ProcessesClass as pc

start_time = time.time()
print("Path for python currenty running:\n{}".format(sys.executable))

"""
#       _______..______      ___      .______       __  ___                _______ .___  ___. .______      
#      /       ||   _  \    /   \     |   _  \     |  |/  /               |   ____||   \/   | |   _  \     
#     |   (----`|  |_)  |  /  ^  \    |  |_)  |    |  '  /      ______    |  |__   |  \  /  | |  |_)  |    
#      \   \    |   ___/  /  /_\  \   |      /     |    <      |______|   |   __|  |  |\/|  | |      /     
#  .----)   |   |  |     /  _____  \  |  |\  \----.|  .  \                |  |____ |  |  |  | |  |\  \----.
#  |_______/    | _|    /__/     \__\ | _| `._____||__|\__\               |_______||__|  |__| | _| `._____|
#                                                                                                          
"""

# Required Variables for the process

#
possible_process_methods = {
    "single_object_process": {
        "etl_classes": pc.SingleObj_Process,
        "iteration_type": j.SparkMaestro.SingleObjectIteration,
    },
    "multi_object_process": {
        "etl_classes": pc.MultipleObjs_Process,
        "iteration_type": j.SparkMaestro.MultiObjectIteration,
    },
    "function_specific_process": {
        "etl_classes": pc.Function_Specific_Process,
        "iteration_type": j.SparkMaestro.SingleObjectIteration,
    },
}

# Environmental/Settings necessary from the environmental table, and what to do with them/which functions to process with those results
required_documents = [
    {"document_index": "data_lake_config",
        "document_function": j.SparkMaestro.UseEnvironmentalSettings},
]


def StartRun(
    PROCESS_ENVIRONMENT,
    ENVIRONMENTAL_VARIABLES_TABLE,
    PROCESS_INDEX_KEY,
    INPUT_TYPE,
    PRIMARY_INPUT,
    SECONDARY_INPUT=None,
    SPARK=None,
    **args
):
    """
    Starts the job with the given inputs.
    """

    if len(args) > 0:
        print('Process Alert! Received the following extra argumens that have not been set up: {}'.format(args))

    appName = "{}_{}".format(PROCESS_INDEX_KEY, round(time.time()))

    if INPUT_TYPE == 'DOCUMENT':
        appName = "{}_{}".format(PRIMARY_INPUT, SECONDARY_INPUT)

    if SPARK is None:

        # Start Spark Session
        conf = SparkConf().setAppName(appName)\
            .set("spark.sql.sources.partitionOverwriteMode", "dynamic")\
            .set("spark.driver.maxResultSize", "0")\
            .set("spark.port.maxRetries", "257")
        
        # Explaining the reason for these configs:
        #   .set("spark.sql.sources.partitionOverwriteMode", "dynamic") --> so that it overwrites partitions only, not full dataframe
        #   .set("spark.driver.maxResultSize", "0") --> remove limits from memory usage, but could lead to accidentally killing the machine
        #   .set("spark.port.maxRetries", "257") --> sets up a higher range of port selection, based on how many applications will run concurrently in one run


        sc = SparkContext(conf=conf)
        sqlContext = SQLContext(sc)
        SPARK = sqlContext.sparkSession


    if SPARK is None and PROCESS_ENVIRONMENT == 'LOCAL':

        # For testing, this adds in the DeltaLake packages
        SPARK.conf.set("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0")
        SPARK.conf.set("spark.sql.extensions",
                       "io.delta.sql.DeltaSparkSessionExtension")
        SPARK.conf.set("spark.sql.catalog.spark_catalog",
                       "org.apache.spark.sql.delta.catalog.DeltaCatalog")


        os.environ[
            "PYSPARK_SUBMIT_ARGS"
        ] = "--packages=org.apache.hadoop:hadoop-aws:2.7.3 pyspark-shell"

        session = boto3.Session()
        credentials = session.get_credentials()
        credentials = credentials.get_frozen_credentials()

        sc = SPARK.sparkContext
        hadoop_conf = sc._jsc.hadoopConfiguration()
        hadoop_conf.set(
            "fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem"
        )
        hadoop_conf.set("fs.s3n.awsAccessKeyId", credentials.access_key)
        hadoop_conf.set("fs.s3n.awsSecretAccessKey", credentials.secret_key)

    # Start the job
    sM = j.SparkMaestro(
        inputType=INPUT_TYPE,
        processKey=PROCESS_INDEX_KEY,
        spark=SPARK,
        primaryInput=PRIMARY_INPUT,
        secondaryInput=SECONDARY_INPUT,
        environmentalVariablesTable=ENVIRONMENTAL_VARIABLES_TABLE,
        processesMapping=possible_process_methods,
        requiredDocuments=required_documents,
        processEnvironment=PROCESS_ENVIRONMENT,
    )
    sM.Main()
    print("\n\nTime it took in seconds: {}".format(
        abs(time.time() - start_time)))
    return


if __name__ == "__main__":

    # # Set up for locally run
    # parser = argparse.ArgumentParser(
    #     description="This is the Avernus SparkJob; to start you need to give the requested inputs and hope for the best! :-D."
    # )

    # parser.add_argument(
    #     "PROCESS_ENVIRONMENT",
    #     help="Which environment is this process being run in; Options are GLUE, LOCAL or EMR and the default is EMR",
    #     choices=["EMR", "LOCAL"],
    #     default="EMR",
    # )

    # parser.add_argument(
    #     "ENVIRONMENTAL_VARIABLES_TABLE",
    #     help="The name of the Environmental Variables Dynamo Table, which will be used to get the shared variables.",
    # )

    # parser.add_argument(
    #     "PROCESS_INDEX_KEY",
    #     help="The index key for the partition.",
    #     default='Manual'
    # )

    # parser.add_argument(
    #     "INPUT_TYPE",
    #     help="This determines how to treat the given inputs.",
    #     choices=['DOCUMENT', 'MANUAL', 'QUEUE']
    # )

    # parser.add_argument(
    #     "PRIMARY_INPUT",
    #     help="""
    #     This is the main input.
    #     If passing a document, this is the Index Key.
    #     If passing a queue, this is the queue url.
    #     If doing a manual run, this determines the process_method; must be one of the following {}.
    #     """.format(','.join(list(possible_process_methods.keys()))),
    # )

    # parser.add_argument(
    #     "SECONDARY_INPUT",
    #     help="""
    #     This is the add-on to the first input.
    #     If passing a document, this is the Sort Key.
    #     If passing a queue, there is no need to add anything.
    #     If doing a manual run, this is the input to the process_method determined previously.
    #     """,
    #     nargs='?'
    # )

    # args = GetArguments({}, mainArguments)

    mainArguments = ["PROCESS_ENVIRONMENT", "ENVIRONMENTAL_VARIABLES_TABLE",
                     "PROCESS_INDEX_KEY", "INPUT_TYPE", "PRIMARY_INPUT", "SECONDARY_INPUT"]
    if len(sys.argv) < 6:
        raise Exception("Missing Arguments, requires at least the first 5 arguments which are to be converted into the following variables + additional options if added: {}".format(','.join(mainArguments)))

    # Setup
    args = {
        'PROCESS_ENVIRONMENT': sys.argv[1],
        'ENVIRONMENTAL_VARIABLES_TABLE': sys.argv[2],
        'PROCESS_INDEX_KEY': sys.argv[3],
        'INPUT_TYPE': sys.argv[4],
        'PRIMARY_INPUT': sys.argv[5]
    }

    if len(sys.argv) > 6:
        args['SECONDARY_INPUT'] = sys.argv[6]

    StartRun(**args)
