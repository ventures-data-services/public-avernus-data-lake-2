find .. | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
zip -r SparkJob.zip SparkJob/
aws s3 cp SparkJob.zip s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_scripts/development/SparkJob.zip
aws s3 cp main.py s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_scripts/development/main.py
aws s3 cp emr_setup.sh s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_scripts/development/emr_setup.sh
aws s3 cp configurations.json s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_scripts/development/configurations.json
