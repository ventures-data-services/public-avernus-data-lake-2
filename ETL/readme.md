# ETL

The following sections are in the EMR:

1) ManualCrawler -> The raw code for running the manual crawler, which runs on EMR.
2) PySparkCode -> The raw code for the batch processed Extract-Transform-Load job, which runs on EMR.
