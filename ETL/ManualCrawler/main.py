# /usr/bin/python3
import argparse

from crawler_helpers.directory_orchestration import \
    BucketOrchestrationAndIteration

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="This is the SQL Pickup using Apache Spark, because running servers is too hard :-(."
    )

    parser.add_argument(
        "-db",
        help="The name of the database which the tables are to be saved under.",
        required=True
    )

    parser.add_argument(
        "-bucket_name",
        help="The bucket which is to be crawled",
        required=True
    )

    parser.add_argument(
        "-prefixes",
        help="The prefixes that are to be crawled, comma-separated.",
        required=False,
        default=''
    )

    parser.add_argument(
        "-table_names",
        help="The specific name to be given for that crawled prefix. If none are given, it uses the prefix/paths to form a name. Should be comma-separated and same length as prefixes, ",
        required=False
    )

    parser.add_argument(
        "-logs_bucket_name",
        help="The name of the bucket if you would like to save the logs in an S3 directory as opposed to printing it out.",
        required=False
    )

    args = (vars(parser.parse_args()))
    crawler_base = BucketOrchestrationAndIteration(**args)
    crawler_base.main_crawler_process()
