find .. | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
zip -r crawler_helpers.zip crawler_helpers/
aws s3 cp crawler_helpers.zip s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_crawler/crawler_helpers.zip
aws s3 cp main.py s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_crawler/main.py
aws s3 cp configuration.json s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_crawler/configuration.json
aws s3 cp bootstrap_script.sh s3://dev-ventures-lambdacode-s3/avernus_two_resources/emr_crawler/bootstrap_script.sh