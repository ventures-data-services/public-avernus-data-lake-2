import time
from botocore.exceptions import NoCredentialsError

from pyspark import SparkConf, SparkContext, SQLContext

from crawler_helpers.aws_s3_class import S3Helper
from crawler_helpers.aws_dynamo_class import DynamoDB_Helper
from crawler_helpers.dataframe_mapping_to_glue import Metadata_Extractor
from crawler_helpers.log_class import LogHelper


class BucketOrchestrationAndIteration:

    def __init__(self, db, bucket_name, prefixes="", table_names=None, logs_bucket_name=None):

        self.directories = []

        self.db_name = db

        self.bucket_name = bucket_name
        self.prefix_list = prefixes.split(',')

        self.s3_class = S3Helper(self.bucket_name)

        self.validate_logs_bucket(logs_bucket_name)

        self.check_spark()
        self.list_all_objects()
        self.parse_directories()
        self.get_prefixes()

        # Not yet working with the passed table names
        self.validate_table_names(table_names)

    def validate_logs_bucket(self, logs_bucket_name):

        self.manual_logs = False

        if logs_bucket_name is not None:
            self.manual_logs = True
            self.s3_class_logs = S3Helper(logs_bucket_name)

    def initiate_and_prepare_logs(self):
        self.logs = LogHelper()
        self.logs.AddValue('db_name', self.db)
        self.logs.AddValue('table_name', self.table_name)
        self.logs.AddValue('bucket_name', self.bucket_name)
        self.logs.AddValue('prefixes', self.prefix_list)

    def check_spark(self):

        if 'spark' in locals():
            return

        appName = f"Crawling_Bucket_{self.bucket_name}".lower()

        conf = SparkConf().setAppName(appName).set(
            "spark.sql.files.ignoreCorruptFiles", "true")

        self.sc = SparkContext(conf=conf)
        self.sqlContext = SQLContext(self.sc)
        self.spark = self.sqlContext.sparkSession

    def list_all_objects(self):
        """
        Create a list with all the objects on S3 in the given prefix
        """
        self.directories = []
        for prefix in self.prefix_list:
            self.directories.extend(
                self.s3_class.list_all_objects(prefix))

    def parse_directories(self):
        self.directories = [
            f"s3://{self.bucket_name}/{f}" for f in self.directories if '=' in f and '.' in f]

    def get_prefixes(self):
        listPrefixes = []

        for rO in self.directories:
            splitF = rO.split('/')[1:]
            name = f"s3:/{splitF[0]}"
            for sF in splitF[1:]:
                if '=' in sF:
                    break
                else:
                    name = '{}/{}'.format(name, sF)

            listPrefixes.append(name)

        self.prefixes = sorted(list(set(listPrefixes)))

    def validate_table_names(self, table_names):
        """
        Does not work just yet
        """

        if table_names is None:
            self.table_dictionary = {p: None for p in self.prefixes}
            return

        table_names = table_names.split(',')

        if len(table_names) != len(self.prefixes):
            raise Exception(
                f'Could not initiate crawler, as it received table_names but they are not the same count as prefixes. Tables: {table_names} | Prefixes: {self.prefix_list}')

        self.table_dictionary = {
            table_names[x]: self.prefixes[x] for x in range(len(table_names))}

        return

    def process_logs(self, table_name, json_object):

        print()
        print(json_object)

        if self.manual_logs is True:
            objkey = f"crawler_logs_{round(time.time()*10)}/{table_name}.json"
            self.s3_class_logs.upload_json(json_object, objkey)

    def process_prefix(self, prefix, file_list):

        extractor = Metadata_Extractor(
            self.spark, prefix, file_list, self.db_name, self.table_dictionary[prefix])

        extractor.create_main_glue_table()
        extractor.add_partitions()

        if len(extractor.failed_list) > 0:
            table_name = prefix.replace(f's3://{self.bucket_name}/', '')
            broken_dictionary = {table_name: extractor.failed_list}
            self.process_logs(table_name, broken_dictionary)

        return

    def main_crawler_process(self):
        # Create index/value dictionary with table_name/applicable_files
        table_mapping = {}
        for prefix in self.prefixes:
            table_mapping[prefix] = [
                f for f in self.directories if prefix in f]

        # Send to crawler
        for prefix, file_list in table_mapping.items():
            try:
                self.process_prefix(prefix, file_list)
            except Exception as e:
                print(f'Path {prefix} failed due to {e}.')
