import json

import boto3


class S3Helper:
    """
    Class has pre-defined function to help perform processes on a S3 bucket.

    bucketName - name of the bucket one will be performing tasks on [REQUIRED] (string)
    profile - AWS CLI profile that has access to that bucket (string)
    region - Region where the S3 Bucket is located (string)
    """

    def __init__(
        self, bucketName, profile="default", region="ap-southeast-2",
    ):
        self.start_resource(profile, region)
        self.bucketName = bucketName

    def start_resource(self, profile, region):
        if profile == 'default':
            self.s3 = boto3.resource("s3", region_name=region)
        else:
            session = boto3.session.Session(
                profile_name=profile, region_name=region)
            self.s3 = session.resource("s3", region_name=region)

    def check_extension(self, extension, filename):
        """
        Checks whether the extension is on the filename, and if not, adds it in.
        extension - file extension to be checked (string)
        filename - file to be checked for it's extension (string)
        """
        if extension not in filename.lower():
            filename = "{}{}".format(filename, extension)
        return filename

    def check_json_name(self, filename):
        """
        Checks if the wanted filename is in the correct JSON structure
        filename - name of the file to be checked (string)
        """
        return self.check_extension(".json", filename)

    def upload_json(self, obj, filename):
        """
        Uploads dictionary as a JSON into S3
        obj - Python Dictionary that can be converted to JSON (dict)
        filename - name the object will be saved on S3 as (string)
        """
        objkey = self.check_json_name(filename)

        try:
            b = json.dumps(obj)
        except Exception:
            b = json.dumps(obj, default=str)

        self.s3.Object(self.bucketName, objkey).put(Body=b)

    def get_json(self, filename):
        """"
        Gets a JSON object from S3 and returns as a Python dictionary.
        filename - name of the object on S3
        returns python dictionary if object was found
        """
        objkey = self.check_json_name(filename)
        content_object = self.s3.Object(self.bucketName, objkey)
        file_content = content_object.get()["Body"].read().decode("utf-8")
        return json.loads(file_content)

    def list_all_objects(self, prefix=""):
        """
        Create a list with all the objects on S3
        prefix - Limit search scope to be found by it's prefix [NOT NECESSARY] (string)
        """
        requestDict = {"Bucket": self.bucketName, "Prefix": prefix}

        response = self.s3.meta.client.list_objects_v2(
            **requestDict, MaxKeys=1000,)

        if "Contents" not in response:
            return []

        fileList = [x["Key"]
                    for x in response["Contents"] if x["Key"][-1] != "/"]

        while response["IsTruncated"]:

            requestDict["ContinuationToken"] = response["NextContinuationToken"]
            response = self.s3.meta.client.list_objects_v2(
                **requestDict, MaxKeys=1000,)
            fileList.extend(
                [x["Key"] for x in response["Contents"] if x["Key"][-1] != "/"]
            )

        return fileList

    def get_objTags(self, filename):
        """
        Get the tags on a S3 object.

        filename - obj's key (string)
        """
        client = boto3.client("s3", region_name="ap-southeast-2")
        response = client.get_object_tagging(
            Bucket=self.bucketName, Key=filename)
        return response["TagSet"]

    def upload_file(self, localFilename, s3ObjName):
        """
        Upload a local file into S3 [Must be on the pre-defined bucket]

        localFilename - full path to the local file (string)
        s3ObjName - Key for the object on S3 (string)
        """
        self.s3.meta.client.upload_file(
            localFilename, self.bucketName, s3ObjName)

    def open_file(self, fileKey, encoding="utf-8"):
        """
        Open an S3 object and return the data stream post decoding.

        fileKey - S3 Object Key (string)
        Encoding - Object's encoding, default is utf-8 which applies to most. (string)
        """
        s3File = self.s3.meta.client.get_object(
            Bucket=self.bucketName, Key=fileKey)
        s3FileContent = s3File["Body"].read().decode(encoding)
        return s3FileContent

    def add_tags_to_object(self, obj_name, tags):
        """
        Adds tags to an S3 Object [Object must be already uploaded, please remember eventual consistency depending on file size]

        obj_name - The Object's Key (string)
        tags - Required tags to be added to the object (dictionary)
        """
        tag_dict = {"TagSet": []}
        for key, value in tags.items():
            temp_dict = {"Key": "", "Value": ""}
            temp_dict["Key"] = key
            temp_dict["Value"] = value
            tag_dict["TagSet"].append(temp_dict)

        self.s3.meta.client.put_object_tagging(
            Bucket=self.bucketName, Key=obj_name, Tagging=tag_dict
        )
