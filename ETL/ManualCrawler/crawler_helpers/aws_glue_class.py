import boto3
from botocore.exceptions import ClientError


class GlueHelper:
    """
    Class contains some pre-defined functinos to help with working with AWS Glue.
    profile - AWS CLI profile that has access to the AWS Job
    region - Region the AWS Glue job is located in
    """

    def __init__(self, profile="default", region="ap-southeast-2", jobName=None, databaseName=None, tableName=None):
        session = boto3.session.Session(
            profile_name=profile, region_name=region)
        self.glue = session.client("glue", region_name=region)
        self.tableName = tableName
        self.databaseName = databaseName
        self.jobName = jobName

    def SetupJobName(self, jobName):
        """
        Initiate the jobName variable
        jobName - should be the AWS Glue Job name.
        """
        self.jobName = jobName

    def StartJob(self, args):
        """
        Start a job with the given arguments.
        args - Glue Job arguments [Check the Boto 3 documentation to find the possible options] (Dictionary)
        """
        self.glue.start_job_run(**args)

    def SetupJob(self, jobName=None, numWorkers=2, jobArgs=None, previousID=None):
        """
        Sets up and starts a job run based on the jobName and jobArgs *OR* on the previousID.
        jobName - name of the AWS Glue job (string)
        numWorkers - MIN of 2; how many works are requested (int)
        jobArgs - Other Arguments for starting the job (Check job requirements) (Dictionary)
        previousID - ID of a Job you would like to re-run using the exact same arguments [Doesn't require any other variable] (string)
        """

        if jobArgs is None and previousID is None:
            raise Exception(
                "Either the JobArgs need to be given, or the previousID")

        if self.jobName is not None:
            jobName = self.jobName

        if jobArgs is not None:
            self.StartJob(
                {"JobName": jobName, "Arguments": jobArgs, "MaxCapacity": numWorkers}
            )

        if previousID is not None:
            self.StartJob(
                {
                    "JobName": jobName,
                    "JobRunId": previousID,
                    "AllocatedCapacity": numWorkers,
                }
            )

    def ValidateDatabaseName(self, inputVariable, variableName):

        mapping = {'databaseName': self.databaseName,
                   'tableName': self.tableName}

        comparison = inputVariable or mapping[variableName]

        if comparison is None:
            raise Exception(
                f'{variableName} input is missing and it was also not initiated with class.')
        else:
            return comparison

    def CreateCatalogueDatabase(self, databaseName=None):
        databaseNameInput = self.ValidateDatabaseName(
            databaseName, 'databaseName')

        response = self.glue.create_database(
            DatabaseInput={
                'Name': databaseNameInput,
            }
        )
        return response

    def CreateCatalogueTable(self, tableDetails, databaseName=None, createDatabase=False, updateTable=False):
        databaseNameInput = self.ValidateDatabaseName(
            databaseName, 'databaseName')

        try:
            response = self.glue.create_table(
                DatabaseName=databaseNameInput,
                TableInput=tableDetails
            )
            return response

        except ClientError as e:

            if e.response['Error']['Code'] == 'AlreadyExistsException' and updateTable is True:
                print(
                    f"Table {tableDetails['Name']} already exists, but will be updated.")

                return self.UpdateCatalogueTable(tableDetails, databaseName=databaseNameInput)

            if e.response['Error']['Code'] == 'EntityNotFoundException' and \
                f'Database {databaseNameInput} not found' in str(e) and \
                    createDatabase is True:
                print(
                    f"Table {tableDetails['Name']} already exists, but will be updated.")
                self.CreateCatalogueDatabase(databaseName=databaseName)

                return self.CreateCatalogueTable(tableDetails, databaseName=databaseName, createDatabase=False, updateTable=updateTable)
            else:
                raise e

        except Exception as e:
            raise e

    def UpdateCatalogueTable(self, tableDetails, databaseName=None):
        databaseNameInput = self.ValidateDatabaseName(
            databaseName, 'databaseName')

        response = self.glue.update_table(
            DatabaseName=databaseNameInput,
            TableInput=tableDetails
        )
        return response

    def CreatePartitions(self, partitionList, tableName=None, databaseName=None):
        databaseNameInput = self.ValidateDatabaseName(
            databaseName, 'databaseName')
        tableNameInput = self.ValidateDatabaseName(tableName, 'tableName')

        response = self.glue.batch_create_partition(
            DatabaseName=databaseNameInput,
            TableName=tableNameInput,
            PartitionInputList=partitionList
        )
        return response
