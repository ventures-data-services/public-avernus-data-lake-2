import datetime
import json

from crawler_helpers.aws_glue_class import GlueHelper
from crawler_helpers.user_defined_errors import BadSchema


class Metadata_Extractor:

    def __init__(self, spark, dataframe_start, full_file_list, database_name, table_name, crawler_name='manual-spark-crawler'):

        self.partition_list = []
        self.failed_list = []

        self.spark = spark
        self.dataframe_start = dataframe_start
        self.full_file_list = full_file_list

        self.crawler_name = crawler_name
        self.database_name = database_name
        self.table_name = table_name

        # So far, only allows for parquet but this will be updated in the future
        self.file_format = 'parquet'
        self.code_created_database = False
        self.valid_key_partitions = []

        self.define_table_name()
        self.start_glue()

        self.clean_file_list()
        self.define_partitions()
        self.initiate_full_partition_list()

        self.get_main_schema()
        self.create_dataframe_details()
        self.set_glue_table_parameters()
        self.determine_hive_details()

    def define_table_name(self):
        if self.table_name is None or self.table_name == '':
            self.table_name = '_'.join(
                [d for d in self.dataframe_start.split('/')[3:] if '=' or '.' not in d]).lower()

    def start_glue(self):
        self.glue_class = GlueHelper(
            databaseName=self.database_name, tableName=self.table_name)
        return

    def clean_file_list(self):
        self.full_file_list = sorted(
            list(set(['/'.join(f.split('/')[:-1]) for f in self.full_file_list])))

    def define_partitions(self):
        self.partitions_list = []
        for f in self.full_file_list:
            found_partitions = [d.split('=')[0]
                                for d in f.split('/') if '=' in d]
            self.partitions_list = self.partitions_list + found_partitions

        self.partitions_list = sorted(list(set(self.partitions_list)))

    def initiate_full_partition_list(self):
        self.full_partition_list = []
        self.objects_list = []
        for f in self.full_file_list:

            if f[-1] != '/':
                f = f'{f}/'

            basic_dictionary = {
                'path': f,
                'partitions': [d for d in f.split('/') if '=' in d]
            }

            basic_dictionary['partition_name'] = '_'.join(
                [partition.split('=')[-1] for partition in basic_dictionary['partitions']])

            self.full_partition_list.append(basic_dictionary['partition_name'])
            self.objects_list.append(basic_dictionary)

    @staticmethod
    def unique_source_keys(df):
        """
        Return a list of unique values found in the 'source_key' column
        """

        source_column_names = ['source_key']

        for source_column in source_column_names:
            if source_column not in df.columns:
                raise Exception(f'Could not find the column {source_column}')

        source_key_values_df = df.select(source_column_names).distinct()

        df_uniqueSources_size = source_key_values_df.count()

        list_uniqueSources = source_key_values_df.collect()

        unique_source_values = {}

        for col_num in range(len(source_column_names)):
            unique_source_values[source_column_names[col_num]] = [
                list_uniqueSources[row][col_num] for row in range(df_uniqueSources_size)
            ]

        return unique_source_values

    def parse_spark_json_schema(self, s):
        main_schema = [{'Name': f['name'], 'Type':f['type'].lower()}
                       for f in s['fields'] if f['name'] not in self.partitions_list]

        for column in main_schema:
            if column['Type'] == 'integer':
                column['Type'] = 'int'

        return main_schema

    def set_glue_table_parameters(self):
        self.glue_table_parameters = {
            'UPDATED_BY_CRAWLER': self.general_dataframe_details['crawler_name'],
            'classification': self.file_format,
            'compressionType': 'none',
            'objectCount': self.general_dataframe_details['file_count'],
            'typeOfData': 'file'
        }

    def determine_hive_details(self):
        if self.file_format == 'parquet':
            self.hive_details_parquet()

    def get_dataframe_schema(self, p, merge_schema=False, use_source_key_for_valid=False):

        spark_read = self.spark.read

        if merge_schema is True:
            spark_read = spark_read.option("mergeSchema", "true")

        df = spark_read.parquet(p)

        if use_source_key_for_valid is True:
            self.valid_key_partitions = self.unique_source_keys(df)

        schema = json.loads(df.schema.json())
        return self.parse_spark_json_schema(schema)

    def validate_schema(self, s):
        comparable_schema = [f"{c['Name']}_{c['Type']}" for c in s]
        schema_comparison = set(comparable_schema) - \
            set(self.comparable_main_schema)
        return len(schema_comparison) == 0

    def create_dataframe_details(self):

        partitions = [{'Name': p, 'Type': 'string'}
                      for p in self.partitions_list]

        self.general_dataframe_details = {
            'table_name': self.table_name,
            'file_count': str(len(self.full_file_list)),
            'partitions': partitions,
            'schema': self.main_schema,
            'crawler_name': self.crawler_name
        }

    def hive_details_parquet(self):
        self.hive_details = {}
        self.hive_details['InputFormat'] = 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
        self.hive_details['OutputFormat'] = 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'

        self.hive_details['SerdeInfo'] = {
            'SerializationLibrary': 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe',
            'Parameters': {'serialization.format': '1'}
        }

    def create_glue_database(self):
        self.glue_class.CreateCatalogueDatabase()
        self.code_created_database = True
        return self.create_main_glue_table()

    def add_partitions_glue_call(self, partitions_list):
        return self.glue_class.CreatePartitions(partitionList=partitions_list)

    def create_partition_details(self):

        details = {}
        details['Values'] = [self.general_dataframe_details['partition_name']]
        details['StorageDescriptor'] = {
            'Columns': self.general_dataframe_details['schema'],
            'Location': self.general_dataframe_details['full_path'],
            'InputFormat': self.hive_details['InputFormat'],
            'OutputFormat': self.hive_details['OutputFormat'],
            'SerdeInfo': {
                'SerializationLibrary': self.hive_details['SerdeInfo']['SerializationLibrary'],
                'Parameters': self.hive_details['SerdeInfo']['Parameters']
            },
            'StoredAsSubDirectories': False
        }
        details['Parameters'] = self.glue_table_parameters

        return details

    def partition_key_validation(self, partition_name):
        return partition_name in self.valid_key_partitions

    def determine_schema(self, o):
        # Open the object if this isn't part of the validated partition names and get it's schema
        if self.partition_key_validation(o['partition_name']) is False:
            return self.get_dataframe_schema(o['path'])
        # If it is part of the validated partition names, just move it along cause it's fine.
        else:
            return self.main_schema

    def get_main_schema(self):
        formatted_path = f'{self.dataframe_start}/'
        try:
            self.main_schema = self.get_dataframe_schema(
                formatted_path, merge_schema=True)
            self.valid_key_partitions = self.full_partition_list
        except Exception as e:
            if 'failed merging schema' in str(e).lower():
                self.main_schema = self.get_dataframe_schema(
                    formatted_path, merge_schema=False, use_source_key_for_valid=True)
            else:
                raise e

        self.comparable_main_schema = [
            f"{c['Name']}_{c['Type']}" for c in self.main_schema]
        return

    def get_storage_descriptor(self, columns, location):
        default_storage_descriptor = {
            'Columns': columns,
            'Location': location,
            'InputFormat': self.hive_details['InputFormat'],
            'OutputFormat': self.hive_details['OutputFormat'],
            'SerdeInfo': {
                'SerializationLibrary': self.hive_details['SerdeInfo']['SerializationLibrary'],
                'Parameters': self.hive_details['SerdeInfo']['Parameters']
            },
            'StoredAsSubDirectories': False
        }

        return default_storage_descriptor

    def object_partition_mapping(self, o):

        identified_schema = self.determine_schema(o)

        # Testing the multi-schema tables -- Seems valid and will be moving into that format from now onwards
        # if self.validate_schema(identified_schema) is False:
        #     raise BadSchema(f'Schema {details_dictionary["schema"]} does not match the main one.')

        partition_details = {
            'Values': [o['partition_name'], ],
            'StorageDescriptor': self.get_storage_descriptor(columns=identified_schema, location=o['path']),
            'Parameters': self.glue_table_parameters
        }

        return partition_details

    def get_table_details_main_table(self):

        table_details = {
            'Name': self.general_dataframe_details['table_name'],
            'Owner': 'owner',
            'Description': f'Table was manually created for testing at {datetime.datetime.now()}',
            'LastAnalyzedTime': datetime.datetime.now(),
            'StorageDescriptor': self.get_storage_descriptor(columns=self.general_dataframe_details['schema'], location=self.dataframe_start),
            'PartitionKeys': self.general_dataframe_details['partitions'],
            'TableType': 'EXTERNAL_TABLE',
            'Parameters': self.glue_table_parameters,
        }
        return table_details

    def create_main_glue_table(self):
        self.general_dataframe_details['full_path'] = self.dataframe_start
        self.glue_class.CreateCatalogueTable(
            self.get_table_details_main_table(), createDatabase=True, updateTable=True)
        return

    def add_partitions(self):
        """
        Adds the full_fill_list into the tables created, by first validating they have the correct schema and then adding their configurations
        """

        for o in self.objects_list:

            try:
                partition_details = self.object_partition_mapping(o)
                self.partition_list.append(partition_details)
            except Exception as e:
                self.failed_list.append(o)
                print(
                    f"The partition {o['partition_name']} failed due to: {e}")
                continue

            if len(self.partition_list) == 100:
                self.add_partitions_glue_call(self.partition_list)
                self.partition_list = []

        if len(self.partition_list) > 0:
            self.add_partitions_glue_call(self.partition_list)

        return