# Manual Crawler

Since the AWS Glue crawler was creating issues and making it difficult to be selective with what files/paths to crawl through, and the lack of failsafes, we chose to build our own crawler in basic details.

This code can only work on AWS EMR (Elastic Map-Reduce) and AWS S3 (Simple Storage Service) files.
It will save them to AWS Glue's data catalogue with the basic formatting, and the table names will be the prefixes with an `_` instead of a `/`.

It would be recommeded to branch this off and create you own if you would like to change any of the defaults.

# How does it work?

The crawler simply validates that all files have acceptable schemas, and updates the Glue Catalogue with those details.
It tries to find all the varitions of partitions and save them into separate tables to minimise conflicts.

# Example

Below is an input example, for details as to what each option does and what it stands for, please check the file `main.py`.

## The deploy-mode _MUST_ be client as it depends on AWS' SDK (BOTO3)

Command example:

```bash
    spark-submit \
    --deploy-mode client \ # Must be client
    --master yarn \ # Must be YARN on EMR
    --py-files s3://code_bucket/crawler/crawler_helpers.zip \ # Extra python files
    s3://code_bucket/crawler/main.py \  # Main file location
    -db dev_database_name_3 \ # Database name
    -bucket_name dev-datalake-enriched-s3 \ # Bucket to be crawler
    -prefixes source/table1/,source/table2/ \ # Comma separated list of prefixes
```

Step Example:

```
JAR location: command-runner.jar
Main class: None
Arguments: spark-submit --deploy-mode client --master yarn --py-files s3://code_bucket/crawler/crawler_helpers.zip s3://code_bucket/crawler/main.py -db dev_datalake_testing_3 -bucket_name dev-datalake-enriched-s3 -prefixes source/table1/,source/table2/
Action on failure: Continue
```
