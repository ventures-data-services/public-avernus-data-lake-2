# Pinnacle Ventures
# Dylan Byrne
import boto3
# import json
from boto3.dynamodb.conditions import Attr, Key
# from botocore.exceptions import ClientError
import datetime
# import time

"""
A class to help with the object meta ddb, with useful functions

TODO
- better error handler of ddb gets, error might not be entry not found
- add sqs functionality
"""

class meta_helper:

    region = "ap-southeast-2"
    primaryKey = "obj_key"
    sortKey = "type"
    metaTypeKey = "meta_type"
    functionKey = "etl_functions"
    # remove the object extensions when getting and creating ddb entries eg obj.txt --> obj
    remove_object_extensions_check = True

    def __init__(self, in_objMetaDDB: str, in_settingsDDB : str, in_region="ap-southeast-2", in_errorSQS=None):
        """
        Set up ddb objects with table name
        """
        self.objMetaTableName = in_objMetaDDB
        self.settingsTableName = in_settingsDDB

        self.ddbResMeta = boto3.resource('dynamodb', region_name=self.region)
        self.ddbTableMeta = self.ddbResMeta.Table(self.objMetaTableName)

        self.functionDict = {}
        self.multiObjFunctionDict = {}
        self.functionDict = {}

        self.settings_ddb = in_settingsDDB

        # print(self.functionDict)

        self.region = in_region
        # print(self.functionDict)

    def get_timestamp(self):
        """
        Adds current timestamp to log.
        """
        # Calculate UTC Now Timestamp
        d = datetime.datetime.utcnow()
        epoch = datetime.datetime(1970, 1, 1)
        time_epoch = (d - epoch).total_seconds()
        return round(time_epoch)

    def remove_object_extensions(self, in_obj_key):
        """
        Remove an object's extension eg prefix/obj.txt --> prefix/obj
        """
        split_key = in_obj_key.split('.')

        if len(split_key) <= 2:
            return split_key[0]
        else:
            return '.'.join(split_key[:-1])

    
    def get_obj_meta(self, in_objKey: str, in_meta_type = "main", in_remove_extensions=False):
        """
        Check if the object meta key exists in the table

        Returns:
        Empty dict: if no object was found
        object dict: if object was found
        """
        try:
            if in_remove_extensions:
                in_objKey = self.remove_object_extensions(in_objKey)

            item = self.ddbTableMeta.get_item(Key={self.primaryKey: in_objKey, self.sortKey: "main"})['Item']

            return item
        except Exception as e:
            # print("Couldn't find obj meta for {1}\n {0} \n".format(str(e), str({"primary_key" : in_objKey, "type" : "main", "meta_type" : in_meta_type})))

            return {}

    def get_obj_meta_type(self, in_objKey: str, in_meta_type = "main"):
        """
        Check if the object meta key exists in the table

        Returns:
        Empty dict: if no object was found
        object dict: if object was found
        """
        try:
            if self.remove_object_extensions_check == True:
                in_objKey = self.remove_object_extensions(in_objKey)

            item = self.ddbTableMeta.get_item(Key={self.primaryKey: in_objKey, self.sortKey: in_meta_type})['Item']

            return item
        except Exception as e:
            # print("Couldn't find obj meta {1}\n {0} \n".format(str(e), str({"primary_key" : in_objKey, "type" : in_meta_type})))

            return {}

    def create_obj_meta(self, in_obj_key: str, in_prefix, in_data_type, in_origin_bucket, in_functions=[], in_optional={}, in_type="main"):
        """
        Create a new object meta entry. Has required fields and then an optional dict for
        additonal keys/values.

        Inputs
        in_functions : if this is left will attempt to get functions from settings, if not found will be left as an
                       empty list
        """
        if self.functionDict == {}:
            self.functionDict = self.get_data_type_functions(self.settings_ddb)

        if in_functions != []:
            functions = in_functions
        elif in_data_type in self.functionDict:
            functions = self.functionDict[in_data_type]
        else:
            functions = []

        obj_type_info = self.get_obj_type(in_obj_key)

        timestamp = self.get_timestamp()

        meta = {
            self.primaryKey : in_obj_key if self.remove_object_extensions_check == False else self.remove_object_extensions(in_obj_key),
            "prefix" : in_prefix,
            "data_type" : in_data_type,
            "origin_bucket" : in_origin_bucket,
            "functions" : functions,
            "type" : "main",
            "meta_type" : in_type,
            "processed" : False,
            "deleted" : False,
            "dataframe_details": {
                "obj_type" : obj_type_info["obj_type"],
                "spark_name" : "s3://{bucket}/{prefixes}{key}".format(bucket=in_origin_bucket, prefixes=in_prefix, key=in_obj_key)
            },
            "creation_timestamp" : timestamp,
            "last_update_timestamp" : timestamp
        }

        delimiter_details = self.get_delimiter_details(in_obj_key, obj_type_info["obj_type"], obj_type_info["obj_suffix"])
        if delimiter_details != {}:
            meta["dataframe_details"]["delimiter_details"] = delimiter_details

        # print("obj meta: {0}".format(str(meta)))

        for key in in_optional:
            meta[key] = in_optional[key]

        # print(meta)

        try:
            response = self.ddbTableMeta.put_item(Item=meta)
        except Exception as e:
            print("In create obj meta, error {0} for {1}".format(str(e), str(meta)))

    def create_multi_obj_meta(self, in_prefix, in_origin_bucket, in_type, in_functions=[], in_optional={}):
        '''
        Create object meta ddb entry for a multi object type

        Inputs:
        in_prefix : the prefix/data set the obj has
        in_origin_bucket : the S3 bucket where the obj is located
        in_type : the data type of the object
        in_functions : the functions/DAGs that should be mapped to the object, will be found if possible if not
                       provided
        in_optional : a dict of other optional keys to be included in the ddb entry
        '''
        # get multi obj functions here
        # if self.multiObjFunctionDict == {}:  
        #     self.get_multi_obj_dags(self.settingsTableName)

        # datatype = in_prefix.split("/")[0]
        # if in_prefix in self.multiObjFunctionDict:
        #     function = self.multiObjFunctionDict[in_prefix]
        # else:
        #     function = []

        # if in_prefix in self.multiObjDependsDict:
        #     depends = self.multiObjDependsDict[in_prefix]
        # else:
        #     depends = []
        # for dt in self.multiObjFunctionDict:
        #     # print("function key: {0}, obj data type: {1}".format(dt, datatype))
        #     if dt in datatype:
        #         # print("[FOUND] functions: {0}".format(str(self.multiObjFunctionDict[dt])))
        #         function = self.multiObjFunctionDict[dt]
        #         break

        context_name = in_prefix.replace("/", "_")
        if context_name[-1] == "_":
            context_name = context_name[:-1]

        timestamp = self.get_timestamp()
        
        meta = {
            self.primaryKey : in_prefix,
            "type" : "main",
            "origin_bucket" : in_origin_bucket,
            "data_type" : in_type,
            "processed" : False,
            "deleted" : False,
            # "functions" : function,
            # "dependent_data_sets" : depends,
            "dataframe_details": {
                "obj_type" : "parquet",
                "spark_name" : "s3://{bucket}/{prefixes}".format(bucket=in_origin_bucket, prefixes=in_prefix),
                "context_name" : context_name
            },
            "creation_timestamp" : timestamp,
            "last_update_timestamp" : timestamp
        }

        for key in in_optional:
            meta[key] = in_optional[key]

        try:
            response = self.ddbTableMeta.put_item(Item=meta)
        except Exception as e:
            print("In create multi obj meta, error {0} for {1}".format(str(e), str(meta)))     

    def get_delimiter_details(self, in_objKey, in_objType, in_objSuffix, in_prefix=None):
        '''
        Get the delimeter details for an obj. If parquet this is blank, otherwise attempt to get
        it from dynamo or use logic to determin it.
        '''
        if in_objType != "delimited":
            return {}

        key_without_type = in_objKey.split(".")[:-1][0].split("/")[-1]
        # print(key_without_type)
        pickup_details = self.get_obj_meta_type(key_without_type, in_meta_type="pickup")     
        # print(pickup_details)

        if pickup_details == {}:
            # HOTFIX REMOVE THIS
            if in_prefix != None:
                if 'bpac' in in_prefix:
                    return self.get_delimiter_details_logic(in_objType, in_objSuffix, True)

            # print("Couldn't find pickup details for obj {}".format(in_objKey))
            return self.get_delimiter_details_logic(in_objType, in_objSuffix)
        else:
            return self.get_delimiter_details_dynamo(pickup_details)

    def get_delimiter_details_dynamo(self, in_pickUpDetails):
        '''
        Process the delimter details from the dynamo entry
        '''
        try:
            pickup_detials = in_pickUpDetails['format_logs']['csv_settings']

            delimeter_details = {
                "delimiter" : pickup_detials['delimiter'],
                "encoding" : pickup_detials["encoding"]
                # "lines_skipped" : pickup_detials["lines_skipped"]
            }

            return delimeter_details
        except Exception as e:
            print("[ERROR] Can't create delimeter details from pickup details")
            print(str(e))
            return {}

    def get_delimiter_details_logic(self, in_objType, in_objSuffix, in_bpacObj=False):
        '''
        Create the delimeter details based on the obj type/suffix
        '''
        pipe_types = ["txt", "psv"]
        comma_types = ["csv"]

        if in_bpacObj == True:
            delimiter_details = {
                "delimiter": "|",
                "encoding": "iso-8859-1"
            }

            return delimiter_details

        if in_objType == "delimited":

            if in_objSuffix in pipe_types:
                delimiter = "|"
            elif in_objSuffix in comma_types:
                delimiter = ","
            else:
                print("Delimiter object suffix {0} not in supported types".format(in_objSuffix))
                return {}

            delimiter_details = {
                # "clean_headers" : True,
                "delimiter" : delimiter,
                "encoding" : "utf-8"
            }

            return delimiter_details

        return {}        

    def get_obj_type(self, in_objKey):
        '''
        return the obj type/suffix based on it's key
        '''
        accepted_file_types = ["txt", "csv", "psv"]
        keySplit = in_objKey.split(".")

        if len(keySplit) > 1:
            keySplit = keySplit[-1]
            if keySplit == "parquet":
                return {"obj_type" : "parquet", "obj_suffix" : keySplit}
            elif keySplit in accepted_file_types:
                return {"obj_type" : "delimited", "obj_suffix" : keySplit}
            
        return {"obj_type": "unsupported", "obj_suffix" : None}
        

    def update_obj_meta(self, in_obj_key : str, in_newInfo: dict, in_meta_type="main", in_entry={}, in_OverwriteFunctions=False):
        """
        Insert the new info into the obj meta entry. Will remove primary key from new info
        """

        if in_newInfo == {}:
            print("[UOM] No new info to update with")
            return False

        if in_entry == {}:
            if self.remove_object_extensions_check == True:
                in_obj_key = self.remove_object_extensions(in_obj_key)
            # in_entry = self.get_obj_meta(in_obj_key, in_meta_type)
            in_entry = self.get_obj_meta(in_obj_key, "main")

        if in_entry == {}:
            print("[UOM] No entry to update")
            return False
        else:
            if "functions" not in in_newInfo and in_OverwriteFunctions == True:
                in_newInfo.pop("functions", None)
                functions = self.functionDict[in_entry["data_type"]]

                # remove primary key from update
                in_newInfo.pop(self.primaryKey, None)

                new_entry = self.merge(in_entry, in_newInfo)
                new_entry["functions"] = functions
                response = self.ddbTableMeta.put_item(Item=new_entry)
                return response

            # remove primary key from update
            in_newInfo.pop(self.primaryKey, None)

            new_entry = self.merge(in_entry, in_newInfo)

            new_entry["last_update_timestamp"] = self.get_timestamp()

            response = self.ddbTableMeta.put_item(Item=new_entry)

            return response


    # def update_obj_dags(self, in_objKey, in_data_type):
    #     if in_data_type in self.functionDict:
    #         functions = self.functionDict[in_data_type]
    #     else:
    #         functions = []

    #     new_dags = {
    #         'functions' : functions
    #     }

    #     self.update_obj_meta(in_objKey, new_dags)


    def merge(self, a, b, path=None):
        "merges b into a"
        if path is None: path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    self.merge(a[key], b[key], path + [str(key)])
                elif a[key] == b[key]:
                    pass # same leaf value
                else:
                    # raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
                    a[key] = b[key]
            else:
                a[key] = b[key]
        return a

    def get_data_type_functions(self, in_ddbName):
        """
        Get the data type functions info from the settings then store in the object
        """
        functions_key = "data_dictionary"
        primary_settings_key = "setting_name"
        enriched_functions_key = "enrich_functions"
        transformed_functions_key = "transform_functions"

        ddbRes = boto3.resource('dynamodb', region_name=self.region)
        ddbTable = ddbRes.Table(in_ddbName)

        try:
            item = ddbTable.get_item(Key={primary_settings_key: functions_key})['Item']

            functions = {}
            for key in item['value']:
                # functions[key][enriched_functions_key] = item['value'][key][enriched_functions_key]
                # functions[key][transformed_functions_key] = item['value'][key][transformed_functions_key]
                functions[key] = item['value'][key][transformed_functions_key]

            return functions
        except Exception as e:
            print(e)
            return {}

    def get_multi_obj_dags(self, in_ddbName):
        '''
        Obtain the multi obj DAGs from the settings ddb table
        '''
        try:
            ddbRes = boto3.resource('dynamodb', region_name=self.region)
            ddbTable = ddbRes.Table(in_ddbName)

            multi_obj_key_name = 'multi_object_process_applied_data_sets'
            multi_obj_depend_key_name = "multi_object_process_depends_data_sets"
            primary_key = "setting_name" # primary key == DAG name
            
            item = ddbTable.scan(FilterExpression=Attr(multi_obj_key_name).exists())["Items"]
            functions = {}
            dependents = {}

            for entry in item:
                for dt in entry[multi_obj_key_name]:
                    # print(dt)
                    # create functions
                    if dt in functions:
                        functions[dt] = list(set(functions[dt] + [entry[primary_key]]))
                    else:
                        functions[dt] = [entry[primary_key]]
                    
                    # create dependacies <--> bidirectional
                    if multi_obj_depend_key_name in entry:
                        if dt in dependents:
                            dependents[dt] = list(set(dependents[dt] + entry[multi_obj_depend_key_name]))
                        else:
                            dependents[dt] = entry[multi_obj_depend_key_name]
                            
                        for dep_dt in entry[multi_obj_depend_key_name]:
                            if dep_dt == dt:
                                pass
                            elif dep_dt in dependents:
                                dependents[dep_dt] = list(set(dependents[dep_dt].append(dt)))
                            else:
                                dependents[dep_dt] = [dt]

            # print(functions)
            self.multiObjFunctionDict = functions
            self.multiObjDependsDict = dependents
            # print("multi obj functions")
            # print(self.multiObjFunctionDict)
        except Exception as e:
            print(e)
            self.multiObjFunctionDict = {}
            self.multiObjDependsDict = {}