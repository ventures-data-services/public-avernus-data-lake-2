# Pedro Mroninski
import json
from decimal import Decimal


import boto3 as boto3
from boto3.dynamodb.conditions import Attr, Key
from botocore.exceptions import ClientError

import time
from dateutil import parser

def CreateResource(resourceName, profile='default', region='ap-southeast-2'):
    if profile != 'default':
        session = boto3.session.Session(
            profile_name=profile, region_name=region)
        boto3resource = session.resource(resourceName, region_name=region)
    if profile == 'default':
        boto3resource = boto3.resource(resourceName, region_name=region)

    return boto3resource


cachedDocuments = {}

class DynamoDB_Helper:
    """
    Simplifies use of the DynamoDB Tables - Connects to the passed table name, find's it's index and sort keys.
    tablename --> Name of the table which these functions would be to be applied to. (String)
    regionName --> AWS region where the table is located.
    profile --> AWS CLI profile name that has access to the wanted table.
    """

    def __init__(self, tableName, regionName="ap-southeast-2", profile="default"):

        self.tableName = tableName

        self.indexKey = None
        self.sortKey = None

        self.Connect2DynamoDB_Table(profile, regionName)
        self.SetUpKeys()

    def Connect2DynamoDB_Table(self, profile, region):
        """
        Connects the tableName given into a variable.
        """
        # Start boto session
        dynamodb = CreateResource("dynamodb", profile, region)
        self.table = dynamodb.Table(self.tableName)

    def SetUpKeys(self):
        """
        Sets up the dictionary keys based on the table schema
        """
        for k in self.table.key_schema:
            if k["KeyType"] == "HASH":
                self.indexKey = k["AttributeName"]
            if k["KeyType"] == "RANGE":
                self.sortKey = k["AttributeName"]

    def CreateKeysDictionary(self, document):
        """
        Creates the dictionary that is often used by functions, based on the document.
        """
        keysDictionary = {self.indexKey: document[self.indexKey]}

        if self.sortKey is not None:
            keysDictionary[self.sortKey] = document[self.sortKey]

        return keysDictionary

    def CleanTable(self):
        """
        CAREFUL!!
        Deletes all items from a DynamoDB Table.
        keyName --> The name of the table's main index.
        """
        scan = self.GetAllItems()
        with self.table.batch_writer() as batch:
            for each in scan["Items"]:
                batch.delete_item(Key={self.indexKey: each[self.indexKey]})

    def CleanDict(self, d):
        """
        Removes any keys with no values
        d --> Dictionary to be uploaded into dynamo (Dictionary)
        """
        return {k: v for k, v in d.items() if v}

    def PrepareDocForDynamo(self, doc):

        if self.indexKey not in doc:
            raise Exception(
                "Could not find the index key `{}` in the document".format(self.indexKey, doc))

        formattedDict = json.loads(
            json.dumps(doc, default=str), parse_float=Decimal
        )
        return self.CleanDict(formattedDict)

    def AddDictionary(self, doc):
        """
        Save dictionary on dynamodb table.
        dic --> python dictionary you'd like to upload, make sure it has the required main/sort indexes. (Dictionary)
        """
        try:
            cleanFormattedDict = self.PrepareDocForDynamo(doc)
            self.table.put_item(Item=cleanFormattedDict)
            return True
        except Exception as e:
            print(
                "\nADMIN ERROR: Document failed to be saved on DynamoDB\nIt threw the error {}\nThe dict was:\n{}".format(
                    e, doc
                )
            )
            return False

    def AddDocuments(self, docList):
        """
        Save a list of documents into the dynamo DB (uses lazy loading based on number of writers available on table)
        docList: List of Dictionaries to be uploaded into the DynamoDB
        """
        with self.table.batch_writer() as batch:
            for doc in docList:
                try:
                    readyDoc = self.PrepareDocForDynamo(doc)
                    batch.put_item(readyDoc)
                except Exception as e:
                    print('Doc {} failed due to {}'.format(doc, e))

    def ProcessItemRequest(self, response):
        """
        Checks whether the 'item' key is in the response, to remove all the metadata that comes with response.
        \nresponse: DynamoDB Get API response
        \nreturns:
        \nprocessed_reponse: Either values in the Item/Items reponse, or None if no response was found.
        """
        processed_response = None

        if "Item" in response:
            processed_response = response["Item"]

        if "Items" in response:
            processed_response = response["Items"]

        return processed_response

    def ValidateSortValue(self, sortValue):
        """
        Checks if the SortValue is needed
        sortValue - value to be checked if it needs to be used. (must match table's schema)
        """
        if self.sortKey is None and sortValue is not None:
            raise Exception("The function requires a sort value")

    def ValidatedGetDocument(self, documentKey, documentSort=None):
        """
        Attempts to pickup document, and throws error if document doesn't exist.
        documentKey - The key of the document(string)
        documentSort - Only neeed if sort key on table, needed as this function only gets exact matches (string or int depending on table schema)
        """
        newDocument = self.GetDocument(documentKey, documentSort)
        if newDocument is None:
            raise Exception(
                "Could not find the document {} in the table {}".format(
                    documentKey, self.tableName
                )
            )
        else:
            return newDocument

    def GetDocument(self, documentKey, documentSort=None):
        """
        Helps get the document if you can't be bothered making the document in the code
        documentKey - The key of the document(string)
        documentSort - Only neeed if sort key on table, needed as this function only gets exact matches (string or int depending on table schema)
        """
        self.ValidateSortValue(documentSort)
        tempKeyDict = {self.indexKey: documentKey}

        if documentSort is not None:
            tempKeyDict[self.sortKey] = documentSort

        return self.GetItem(tempKeyDict)

    def GetDocumentOnce(self, documentKey, documentSort=None):
        """
        Helps get the document, and keeps it in the memory if this is a document that is not expected to change
        during process. DO NOT use this if the document would be changing through the process.
        documentKey: IndexKey for the document (defined in Dynamo Table)
        documentSort: SortKey for the document, if needed (defined in Dynamo Table)
        """

        self.ValidateSortValue(documentSort)
        cachedKey = documentKey

        if documentSort is not None:
            cachedKey = "{}_{}".format(cachedKey, documentSort)

        if cachedKey not in cachedDocuments:
            newDoc = self.ValidatedGetDocument(documentKey, documentSort)
            cachedDocuments[cachedKey] = newDoc
            return newDoc

        if cachedKey in cachedDocuments:
            return cachedDocuments[cachedKey]

    def GetItem(self, keyDict):
        """
        This requres a keyDict, which should be similar to {'keyName':'keyValue'} where keyName is the main index in the dict and the keyValue is the value you are looking for.
        \nIf there is a sort key, the dictionary should be as follows {'keyName':'keyValue', 'SortkeyName':''SortkeyValue'}
        """
        response = self.table.get_item(Key=keyDict)
        return self.ProcessItemRequest(response)

    def GetAllItems(self):
        """
        Gets all documents from the table.

        returns all the items found (list)
        """
        response = self.table.scan()
        data = self.ProcessItemRequest(response)

        while "LastEvaluatedKey" in response:
            try:
                response = self.table.scan(
                    ExclusiveStartKey=response["LastEvaluatedKey"])
                data.extend(self.ProcessItemRequest(response))
            except ClientError as e:
                print(e)
                time.sleep(20)

        return data

    # Gets the latest file for a key, if there are multiple documents with same key
    def LatestFile(self, keyValue, limit=1, ForwardIndex=False):
        """
        -- For tables with a sort key only --
        Gets the top x document(s) on a descending sorted list for that object. Define how many documents by defining the limit, and ascending if ForwardIndex=True
        keyname --> the name of the index
        keyvalue --> the partition value.
        """
        response = self.table.query(
            KeyConditionExpression=Key(self.indexKey).eq(keyValue),
            Limit=limit,
            ScanIndexForward=ForwardIndex,
        )

        return response

    def RemoveDocuments(self, documentList):
        """
        Removes one or many documents from the database.
        """

        if not(isinstance(documentList, list)):
            documentList = [documentList]

        with self.table.batch_writer() as batch:
            for document in documentList:
                document_search_dict = self.CreateKeysDictionary(document)
                batch.delete_item(Key=document_search_dict)

    def ItemGet_ScanTable(self, comparisonKey, comparisonValue='', comparisonType='eq'):
        """
        Scans the table for documents, using the value and scantype for a single key.
        comparisonKey: The Document Key to make this comparison against [String]
        comparisonValue: Value to be compared against the document's key [String/Int/List]
        compasionType: The Type of comparison (use the ones in the Boto3 Documentation) [String]
        """
        whileStarter = True
        response = {}
        data = []

        try:
            method = getattr(Attr, comparisonType)

        except AttributeError:
            errorName = 'Could not find the comparisonType `{}` in the Attr Class of DynamoDb'.format(
                comparisonType)
            raise errorName

        argumentCount = method.__code__.co_argcount

        while whileStarter is True or "LastEvaluatedKey" in response:

            whileStarter = False

            if argumentCount > 1:
                response = self.table.scan(
                    Select='ALL_ATTRIBUTES',
                    FilterExpression=method(
                        Attr(comparisonKey), comparisonValue),
                    ConsistentRead=True
                )
            else:
                response = self.table.scan(
                    Select='ALL_ATTRIBUTES',
                    FilterExpression=method(Attr(comparisonKey)),
                    ConsistentRead=True
                )

            data.extend(self.ProcessItemRequest(response))

        return data

    def GetDocumentIndex(self, document):
        """
        Get's a document's index key, if it belongs to this Table, if not, it will throw and error.
        document: A document from this table. (Dictionary)
        """

        if self.indexKey is None or self.indexKey not in document:
            raise Exception(
                "Could not get the index key from the document `{}` make sure it belongs to the table `{}` ".format(
                    document, self.tableName
                )
            )

        return document[self.indexKey]