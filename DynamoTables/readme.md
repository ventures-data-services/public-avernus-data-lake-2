DynamoDB is a document database hosted and maintained by AWS.
It must be set up in order to utilise the ETL process, and for the data lake to contain all the required information for all its processes.

# Required Tables

For the Avernus Data Lake, we have designed it to depend on three main tables. They are as follows:

- Settings Table: This contains documents that are shared amongst the multiple services and Elastic MapReduce's instances.
- Object Metadata Table: Provides metadata or any required information about the objects. For more information on what are these objects, please read the section about this table.
- Logs Table: Provides information on any events; be they process requests, object changes or any other event that needs to be tracked. Always contains a timestamp as it is cleared on a biweekly schedule.

# Settings Table

The "Settings" table is extremely important to all the parts of the lake, as it contains the names of other resources. This table only contains an Index Key and does not need too many readers/writers as the documents will be small and it will only receive a few calls on each section.

## Data Lake Config

For the other process to run, it _MUST_ have a document with the `IndexKey` being `data_lake_config` and the following schema:

| Key                         | Meaning                                                                                                                                                                        | Format                                                                |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------- |
| setting_name                | The index key, which means it is what you will get to pickup the document. This is defined whe building the cloud formation and it's going to be in any document on this table | lower_snake_case                                                      |
| log_ddb_table               | The name of the Log table on DynamoDB                                                                                                                                          | lower_snake_case                                                      |
| sqs_url                     | The URL for the queue that will track all the messages to be sent at the end of the state-machine                                                                              | http://url                                                            |
| single_object_sqs_url       | The URL for the queue that will submit jobs to the ETL process, to maintain a state machine process                                                                            | http://url                                                            |
| state_machine_arn           | Amazon Resource Name for the State machine, which runs the daily batch ETL process                                                                                             | arn:aws:states:region-name:account-id:stateMachine:state-machine-name |
| log_bucket                  | The name of the bucket that will keep logs, on S3                                                                                                                              | lower-kebab-case                                                      |
| obj_meta_ddb_table          | The name of the Object metadata table on DynamoDB                                                                                                                              | lower_snake_case                                                      |
| enriched_bucket             | The name of the bucket that will contain the objects in their enriched state, on S3                                                                                            | lower-kebab-case                                                      |
| transformed_bucket          | The name of the bucket that will contain the objects in their transformed state, on S3                                                                                         | lower-kebab-case                                                      |
| log_object_sqs_url          | The URL for the queue that will check the logs for the processes objects                                                                                                       | http://url                                                            |
| ferryman_log_checker_lambda | The name of the lambda function which checks the logs on the log queue                                                                                                         | kebab-Case                                                            |
| admin_sns_topic             | Amazon Resource Name for the Simple Notification Service, which will notify the admins of any errors found on the batched ETL process                                          | arn:aws:sns:region-name:account-id:sns-name                           |
| raw_bucket                  | The name of the bucket that will contain the objects in their raw state, on S3                                                                                                 | lower-kebab-case                                                      |

Example in JSON format:

```json
{
  "setting_name": "data_lake_config",
  "log_ddb_table": "dev_datalake_log_ddb",
  "sqs_url": "https://sqs.ap-southeast-2.amazonaws.com/url",
  "single_object_sqs_url": "https://sqs.ap-southeast-2.amazonaws.com/url",
  "state_machine_arn": "arn:aws:states:ap-southeast-2:0000000:stateMachine:dev-datalake-etl-sfn",
  "log_bucket": "dev-datalake-logs-s3",
  "obj_meta_ddb_table": "dev_datalake_object_meta_ddb",
  "enriched_bucket": "dev-datalake-enriched-s3",
  "transformed_bucket": "dev-datalake-transformed-s3",
  "log_object_sqs_url": "https://sqs.ap-southeast-2.amazonaws.com/000000/dev-datalake-object-log-process-sqs",
  "ferryman_log_checker_lambda": "dev-datalake-two-FerrymanLogCheckerFunction",
  "admin_sns_topic": "arn:aws:sns:ap-southeast-2:00000000:dev-datalake-admin-sns",
  "raw_bucket": "dev-datalake-raw-s3"
}
```

### _For all the examples, the Key setting_name has the same meaning as described above_

## ETL DAGs and Functions

The Extract-Load-Transform (ETL) depend on the "Settings" table. This is because the "Settings" table provides the inputs for the functions and the DAGs for the Objects. This makes for a clearer and queryable data lineage.
The dependencies expect specific inputs.

### DAG Example

Each DAG returns the Dataframe to it's inital state, so you can have multiple outputs from the same object without interfering in their processes.

The following is an example for a document that contains a DAG:

```json
{
  "function_list": ["sql_query_join_tables", ..."save_basic_source_table"],
  "multi_object_process_applied_data_sets": [
    "classification/example/",
    "nhi_reference/example/"
  ],
  "setting_name": "source_table_dag_raw"
}
```

The explanation for it is:

| Key                                    | Meaning                                   | Format                                                                 | Required?                                        |
| -------------------------------------- | ----------------------------------------- | ---------------------------------------------------------------------- | ------------------------------------------------ |
| function_list                          | The name of the Log table on DynamoDB     | list of strings, each value should exist in the ETL code               | Yes, for any DAG                                 |
| multi_object_process_applied_data_sets | All the objects which this DAG applies to | list of strings, each value should be an object on the Object Metadata | No, only for DAGs which have multiple dependency |

### Function Example

Each function is a step on the DAG, and one function follows the other.
The dataframe is kept from one state to the other, and the lists should easily describe all the changes to the dataframe.

Since the dataframe cannot change it's source, that means they always have to save to a new environment and have a saving step.

The following is an example for a document that contains a function's input, specifically a save_df function since it will have to always be on a DAG:

```json
{
  "function_input": {
    "partition_by": ["source_key"],
    "save_format": "parquet",
    "save_mode": "overwrite",
    "save_path": "s3://transformed-bucket-datalake/source/table"
  },
  "function_name": "save_df",
  "object_creation_prefixes": ["source/table/"],
  "setting_name": "save_basic_source_table"
}
```

- `object_creation_prefixes` is a key which only appears in save_df functions, which says which objects have been created. This should be referencing a document that exists or will be created in the object_metadata table.
- `function_name` is the name of the function, exactly as is in the ETL code.

- To help make more sense out of the `function_input` key, below is the code in the ETL for the save_df function:

```python

    def save_df(self, df, save_path, save_mode, save_format,  repartitions=0, partition_by=[]):
        """
        Saves the dataframe into the given Path.

        df: Spark Dataframe (Spark DataFrame)
        save_path: Spark friendly path to save the file (string)
        save_format: Spark Option for type of saving  (string)
        repartitions: Number of files to be saved as (int)
        partition_by: If you would like to partition by a column or columns, have the column name(s) in a list. (list of strings)
        """
        racoon_df = Baby.Raccoon(df)

        valid_save_path = self.CheckObjPath(save_path)

        racoon_df.SaveDF(save_path, numOfReparitions=repartitions,
                         saveFormat=save_format, saveMode=save_mode, partitionBy=partition_by)

        return racoon_df.GetDF()

```

The dataframe (df) is always passed, but the arguments after it can be options in the document. The key is the name of the variable, the value is whatever you would like to pass.
Values with a default don't have to be passed, as per python rules. To make sure the correct keys are given, it is beneficial to have the code easily viseable when designing the DAGs.

# Object Metadata Table

For the purposes of this documentation, an "Object" is any file or prefix which can be opened into a single dataframe on Apache Spark.
The object metadata table contains any metadata about an object.
It can be useful for data lineage or archiving and is recommended to add in information that is not accesable without opening the file (e.g the file's original schema).

For all the documents, they _MUST_ have a document with the `sort key` which is `main` for the ETL to work. The keys which the ETL checks are the below:

| Key               | Meaning                                                                                | Format                                                                             | Required?                                                              |
| ----------------- | -------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| obj_key           | The index key for the object metadata                                                  | Any format, as long as it creates an unique partition for each object              | Yes                                                                    |
| type              | The sort key for this object metadata, identifies which type of document this is       | lower case                                                                         | Yes, must be the value "main"                                          |
| dataframe_details | Contains all the details needed by Spark to open the object into a dataframe           | Dictionary with the details, they will be broken down further in their own section | Yes, for any object                                                    |
| functions         | Contains all the functions or DAGs which need to be applied to this object once opened | list of strings, each value should be a document in the Settings table             | No, only for objects which require functions (single-object processes) |

An example of a document is below:

```json
{
  "dataframe_details": {
    "delimiter_details": {
      "delimiter": "|",
      "encoding": "iso-8859-1"
    },
    "obj_type": "delimited",
    "context_name": "table",
    "spark_name": "s3://dev-avernus-raw-s3/source/table/extract_filter_timestamp.txt"
  },
  "functions": ["source_table_dag_raw"],
  "obj_key": "extract_filter_timestamp",
  "type": "main"
}
```

The `dataframe_details` is an unique setting, as it contains all the necessary information to inform the Spark Dataframe on how to open the objects.

Some of the options and requirements are below:

If the `obj_type` is `parquet`; you only need to have the `spark_name`.
If the `obj_type` is `delimited`; you have to also have any delimiter details and the `spark_name`. The delimiter details can be any of the following:

```python
    # What delimites each column
    delimiter="|",
    # What encoding is used by the files
    encoding="utf-8",
```

If the key `context_name` is in the `dataframe_details`, the ETL process will add this file to the Spark Context with the given name, and you will be able to query the context using that name as it's table name.

# Logs Table

The logs table contains any events which need to be temporarily traced by the Data Lake. For purposes of this documentation, please note that "temporary" means ~14 days.
This separation is important as this is the only table that won't continue to grow over time and is continuously cleaned. Which makes it great for tracking any events between applications with a higher complexity than a single key.

This can be used to submit ETL jobs in more detail, track errors in the applications and check individual file's issues.

There are no inforced documents on this table.

# Helpers

To help with some of these processes, some helpers were created. They are Jupyter Notebooks, and so their information and details should be within the notebooks themselves. They are located under Build_Documents.
To access them, it is recommended using conda to install the Jupyter Notebook/Labs and once started, just plug and play.
