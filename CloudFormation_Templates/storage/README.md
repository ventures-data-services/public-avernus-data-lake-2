# Avernus Storage Cloudformation

## Contents

1. S3 Buckets
2. Dynamo DB Resources
3. SNS/SQS Resources
4. Lambda Functions
5. Glue Resources

## S3 Buckets

The Avernus Storage stack creates several S3 Buckets that (apart from the Log Bucket) represent different stages of data transformation. They are:

* **Raw Data Bucket** - the data in it's unprocessed form, in it's original object format
* **Transformed Data Bucket** - the data in a more refined format, should have the correct data types and be in parquet format
* **Enriched Data Bucket** - the data in an analysis/visualization/sharable ready format
* **Log Bucket** - a bucket for storing logs from several sources in the data lake (Cloudtrail, Lambda etc.)

### S3 Lifecycle Policies

These currently apply only to the **Raw Data Bucket** and **Logs Bucket**. The policy is that if objects are 6 months or older it transitions it's storage class to S3 Glacier. If it is a *versioned* object (eg deleted but back upped), they are transitioned to Glacier after 2 months.

## Dynamo DB Resources

For full information on how Avernus uses Dynamo DB check the README in the *DynamoTables* directory.

The Avernus Dynamo DB creates 3 Dynamo DB tables and scaling resources for these tables. They are:

* **Object Meta Table** - stores meta information about objects in the data lake. This information then can be used for governance and ETL
* **Settings Table** - should be used to store static documents used as variables in the data lake
* **Logs Table** - stores log documents from the lake, such as from ETLs

### Scaling

Each Dynamo table has scaling for read and write units (apart from the Settings Table, which just has read scaling). These are all set to minimum of 1 unit, scaling to a maximum of 30 units, when reaching 70% capacity.

## SNS/SQS Resources

SNS (Simple Notification Service) and SQS (Simple Queue Service) resources are:

* **Admin SNS Topic** - an SNS topic for Admin level messages from the lake, that is technical level messages that the data lake administrator should be aware of
* **SNS Message SQS** - queue of messages that should be concatenated and sent to the Admin SNS Topic
* **Process Object SQS** - queue of unprocessed objects uploaded to the Raw Data bucket. Is added to from a lambda triggered by PUTs on the Raw Bucket.
* **Process Log SQS** - queue of log keys in the Log Dynamo Table to process into messages when appropriate

## Lambda Functions

* **Object Delete Meta Lambda** - triggered on a DELETE event in the Raw Data Bucket. Deletes data in the Transformed Data Bucket that has the object name in it's partition prefix
* **Object Create Meta Lambda** - triggered on a PUT event in the Raw Data Bucket. Creates 'Main' object meta in the Dynamo Table, and adds objects key (without suffix) to the Process Object SQS
* **Object Create Multi Meta Lambda** - triggered on a PUT/DELETE event in the Transformed Data Bucket. Adds the data type prefix to the Log Dynamo Table, to be processed by an ETL

### Note on creating Object Meta

Object Meta delimiter details is based on the object extension (eg txt, csv, parquet) if no pickup object meta document is found. '.txt' files default to pipe | separated and UTF-8 encoded. You may need to change this based on your specifications.

## Glue Resources

### Databases

* **Transformed Database** - Glue database for Transformed Data Bucket
* **Enriched Database** - Glue database for the Enriched Data Bucket

### Crawlers

The provided Glue Crawlers can be unreliable, it is recommended you use the Manual Crawler code in the *ETL* directory to populate the Glue Databases (further information can be found in the README inside the directory). This crawler runs on an EMR Cluster. 

* **Transformed Data Crawler** - Crawls the Transformed Data Bucket, and populates the Transformed Database
* **Enriched Data Crawler** - Crawls the Enriched Data Bucket, and populates the Enriched Database