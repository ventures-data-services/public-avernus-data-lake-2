# Avernus ETL Cloudformation

Additional READMEs can be found in each of the Cloudformation template directories.

## Contents

1. [Step Function State Machine](#Step Function State Machine)
   1. [Description](#Description)
   2. [Diagram](#Diagram)
   3. [Input](#Input)
   4. [Step Breakdown](#Step Breakdown)
   5. [Schedule](#Schedule)

## Step Function State Machine

### Description

The Avernus EMR State Machine manages the ETL process for the data lake. It uses a combination of Lambda functions and EMR tasks to do this.

### Diagram

![alt text](readme_images/stepfunctions_graph.png "State Machine Diagram")

### Input

You can add the *skip_sort* key to the State Machine input as a bool. If set to true the *object_sort* step will skip it's process and return. You may want to use this if you have created your own method for sorting objects.

### Step Breakdown

The Avernus State Machine can be broken down into several parts:

1. Initialization
2. Object Sorting
3. EMR Cluster Set Up
4. Raw Bucket Objects ETL to Transformed Bucket Objects
5. Transformed Bucket Objects ETL to Enriched Bucket Objects
6. Success/Fail End states

<u>Initialization</u>

Set up parameters to be used by the other steps in the state machine. These parameters are stored in the *config* and *exec_arn* keys in the state machine output.

Example parameters are the names of S3 buckets, Dynamo DB table, and the current execution ARN of the State Machine.

This is made up of the states:

- **config_setup**: pass state that adds variables inputted from the Cloudformation template needed by the State Machine
- **set_raw_type**: sets the name of the current stage of the State Machine
- **find_exec_arn**: a Lambda function that find the current execution ARN of the State Machine

<u>Object Sorting</u>

This section is made up of the states:

* **object_sort** - lambda that sorts the objects in the *landing/* prefix 
* **wait_sorting** - wait state

The object_sort state triggers a Lambda function that sorts objects in the *landing/* prefix in the Raw Data Bucket. It does this using the rules contained within the *data_dictionary* and *data_prefix* documents found in the Settings Dynamo DB table. See the Dynamo DB README for more information about the above documents.

The *wait_sorting* state waits a number of minutes, this is because S3 objects have eventual consistency.

<u>EMR Cluster Set Up</u>

This section will not be started if no Raw Objects can be found to be processed (this is checked by the *r2t_finder*, and *r2t_start?* states).

It is made up of the states:

* **create_a_cluster** - creates an EMR cluster
* **merge_results** - merges useful EMR Cluster information to step output
* **enabled_termination_protection** - enables termination protection on the EMR Cluster
* **submit_hadoop_debug_step** - submits a step to the cluster to set up Hadoop debugging

This creates an EMR cluster made up of a single master node, and the *merge_results* state places the appropriate outputs from this in the State Machine output. After this it enables termination protection on the cluster, and sets up hadoop debugging by submitting a task to do so to the cluster.

<u>Raw Bucket Objects ETL to Transformed Bucket Objects</u>

This uses dynamic parallel steps to submit EMR task to process objects that are in the Process Object SQS Queue. Currently it uses the iterate list outputted by the *r2t_finder* step (hardcoded to 5) to generate the number of parallel states created.

For more information on how the EMR code works check the README in the ETL directory.

It is made up of the states:

* **arg_maker** - lambda that formats the arguments correctly to submit an EMR step 
* **submit_etl_step** - submits an EMR step using the arguments from *arg_maker*, this step will pull object keys from the Process Object SQS and perform DAGs on them attached in their object meta 'main' documents. DAGs are stored in the Settings Dynamo Table
* **emr_error_sqs** - only triggered if there is an error in *arg_maker* or *submit_etl_step*. Sends an error message to the SNS Message SQS
* **r2t_log_check** - lambda that checks the Process Log SQS and sends appropriate messages to the SNS Messages SQS

<u>Transformed Bucket Objects ETL to Enriched Bucket Objects</u>

* **t2n_finder** - lambda that pulls prefixes to process from the Logs Dynamo DB Table, that should of been created by the S3 triggered Create Multi Object Meta Lambda function, and create a Dynamo process document and appropriate EMR arguments
* **submit_multi_obj_step** - submit am EMR step using the arguments created by the *t2n_finder* step

<u>Success/Fail End states</u>

* **{fail_}disable_termination_protection** - disables termination protection from the EMR Cluster
* **{fail_}terminate_cluster** - terminates the EMR Cluster
* **{fail/success}_log_check** - check and process logs in the Process Log SQS
* **{fail/success}_notify** - pull messages from the SNS Messages SQS, concatenate them into a single message, and send this to the Admin SNS Topic
* **{fail/success}_end** - end the State Machine execution with the appropriate end type

### Schedule

If enabled in the parameters the Avernus EMR State Machine will run daily at 9:30am NZ time. This can be changed by edition the CRON for the Step Function schedule in the ETL Cloudformation yaml.