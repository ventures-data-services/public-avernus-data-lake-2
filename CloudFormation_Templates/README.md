# Avernus Data Lake CloudFormation

Additional READMEs can be found in each of the CloudFormation template directories.

## Contents

1. [Building/Deploying the CloudFormation Template](#Building/Deploying the CloudFormation Template)
2. [Parameters](#Parameters)
3. [Nested Stacks](#Nested Stacks)

## Building/Deploying the CloudFormation Template

This assumes you have the AWS CLI set up for the account you want to deploy the Avernus Data Lake, if not follow the instructions here https://aws.amazon.com/cli/.

1. Create or choose at least one S3 bucket to upload the Avernus Resources needed for deployment

2. To build the template files use the following command in the *CloudFormation_Templates* directory:

    ```bash
    aws cloudformation package --template-file avernus-data-lake-core.yaml --output-template avernus-packaged.yaml --s3-bucket {your-deployment-s3-bucket}
    ```

    Where your deployment bucket is any other S3 bucket in your account. This command will create several *.template* objects in the root of that bucket that are used with deployment.

3. Run the below command to generate default AWS EMR IAM roles in your deployment account:

	```bash
	aws emr create-default-roles
	```
	
4. Create the *AWSServiceRoleForEMRCleanup* in your account (https://docs.aws.amazon.com/emr/latest/ManagementGuide/using-service-linked-roles.html), which you can do by by going to the IAM console and creating an AWS Service Role for EMR

5. Upload the other necessary resources to an S3 bucket you have permission to:

	* Lambdas/CloudformationLambdas/CloudformationLambdas.zip
	* ETL/PySpark_ETL/SparkJob.zip
	* ETL/PySpark_ETL/main.py
	* ETL/PySpark_ETL/emr_setup.sh

5. Create an EC2 key pair to be used by the EMR Cluster (https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)

6. Either deploy the template in the Cloudformation console using the *avernus-packaged.yaml* (see the **Parameters** section for additional information), or deploy via the AWS CLI as below:

    a. Edit the *avernus-parameters.json* file to use your specific parameters, more information can be found in the [Parameters](#Parameters) section of this document

    b. Then use the following command to deploy the CloudFormation Stack:
      ```bash
      aws cloudformation deploy --template-file avernus-packaged.yaml --stack-name {YOUR-STACK-NAME} --parameters file:avernus-paramters.json
      ```

7. Upload documents to the Settings Dynamo DB table by following the steps below (further details can be found in the Dynamo Tables README):

    a. run the Dynamo Init Lambda, this creates the Data Lake Config document

    b. run the Jupyter Notebooks in the *DynamoTables* directory to upload the documents needed for sorting (data dictionary, and data prefix documents), and for ETL (DAG and function documents)

9. If you believe you will be dealing with a large number of objects being uploading to the Raw Data Bucket at the same time you will also have to send AWS a support ticket to increase the number of concurrent Lambda executions for your account. By default this is set to 1000.

## Parameters

Stack Name is the name used in the stack itself, this is what you use when deploying from the AWS CLI. Readable Name is the name displayed in the AWS CloudFormation Console when deploying.

| **Stack Name**              | **Readable Name**       | **Description**                                              | **Example**              |
| --------------------------- | ----------------------- | ------------------------------------------------------------ | ------------------------ |
| PrefixName                  | Stack Resource Prefixes | A prefix that will be used on all resources created by the template, usually the environment (dev, prod) | dev                      |
| UniqueName                  | Custom Prefix           | Enter a unique team/project name to give resources a unique name (avoids conflicting name issues) | team                     |
| RawBucketLifecycleEnabled   | Raw Bucket Lifecycle    | Enabled Glacier transition on the Raw Bucket                 | Enabled                  |
| LogBucketLifecycleEnabled   | Log Bucket Lifecycle    | Enabled Glacier transition on the Log Bucket                 | Disabled                 |
| CodeBucket                  | Code S3 Bucket          | The name of the S3 bucket where the code is stored           | code-bucket-s3           |
| CodeKey                     | Code Zip Key            | The key of the lambda code zip file                          | prefix/code.zip          |
| StepFunctionScheduleEnabled | Schedule Enabled        | Whether the Step Function is enabled to run on the schedule (9:30am) | ENABLED                  |
| EmrSparkZipPath             | Spark Zip Path          | S3 path to the code zip file for the Spark EMR Jobs          | s3://bckt/prfx/spark.zip |
| EmrSparkMainPath            | Spark Main Python Path  | The S3 path to the main.py file to use with EMR              | s3://bckt/prfx/main.py   |
| EmrBootstrapPath            | Bootstrap Path          | The S3 path the bootstrap script used by EMR                 | s3://bckt/prfx/setup.sh  |
| EmrKeyPair                  | EC2 Key Pair            | The EMR EC2 key name to use for the cluster                  | key-name                 |
| EmrSubnet                   | EC2 Subnet              | The EMR EC2 subnet to use for the cluster                    | subnet-abc123            |
| EmrMasterNodeType           | Master Node EC2 Type    | The EC2 type to use for the EMR master node                  | r5.xlarge                |

## Nested Stacks

The Avernus Data Lake CloudFormation is broken into several CloudFormation templates that are logically divided by purpose.

### Core

The Avernus Core stack's main purpose is to build and manage the other templates used by the Avernus Data lake. It also creates an Initializer Lambda that takes important information from the stacks and places it in a Dynamo DB document in the Settings Table. This lambda function should be manually run after the stack creation.

### Storage

The Avernus Storage stack's main purpose is build out the storage structures used by the Avernus Data Lake. These are the S3 buckets, Dynamo DB tables, Glue resources, SQS Queues, SNS Topics, S3 Triggered lambda functions.

### ETL (Extract Transform Load)

The Avernus ETL stack's main purpose is to build out the resources used during the ETL process. These being lambda functions, Step Function State Machine (which builds EMR Clusters), and Cloudwatch Schedule for the State Machine.
